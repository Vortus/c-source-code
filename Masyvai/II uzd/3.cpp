
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    double average(double arr[], int l){
        double sum = 0;
        for(int i = 0; i < l; i++)
            sum += arr[i];
        return sum / l;
    }

 int main(){

	ifstream fi("in.txt");
    int n, count = 0; fi >> n;
    double arr[n]; for(int i = 0; i < n; i++) fi >> arr[i];
    double av = average(arr, n);

    for(int i = 0; i < n; i++)
        if(arr[i] > av) count++;

    cout << count;
	fi.close();

 return 0; }
