
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    void coutArr(int arr[], int l){
        for(int i = 0; i < l; i++)
            cout << arr[i] << " ";
        cout << endl;
    }

 int main(){

	int aN, bN;
	cout << "A masyvo dydis ir pats mas.: "; cin >> aN;
	int AArr[aN]; for(int i = 0; i < aN; i++) cin >> AArr[i];
    cout << "B masyvo dydis ir pats mas.: "; cin >> bN;
	int BArr[bN]; for(int i = 0; i < bN; i++) cin >> BArr[i];
    int arr[aN + bN];

    for(int i = 0; i < aN + bN; i++){
        if(i < aN) arr[i] = AArr[i];
        else arr[i] = BArr[i - aN];
    }

    coutArr(AArr, aN);
    coutArr(BArr, bN);
    coutArr(arr, aN + bN);

 return 0; }
