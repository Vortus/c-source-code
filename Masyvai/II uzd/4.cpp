
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int check(int number, int from, int to){
        if(number >= from && number <= to) return 1;
        return 0;
    }

 int main(){

	ifstream fi("in.txt");
    int perfect = 0, positive = 0, negative = 0;

	while(!fi.eof()){
        int j; fi >> j;
        perfect += check(j, 9, 10);
        positive += check(j, 4, 10);
        negative += check(j, 0, 3);
	}

    cout << perfect << endl << positive << endl << negative;
	fi.close();

 return 0; }
