
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

	int mCount = 0, wCount = 0, n, mR = 0, wR = 0;

	ifstream fi("in.txt");	fi >> n;

    for(int i = 0; i < n; i++){
        int j; fi >> j;
        if(j > 0){ mCount++; mR += j; }
        else { wCount++; wR += j * -1; }
    }
    cout << "Vaikinu: " << mCount << endl;
    cout << "Merginu: " << wCount << endl;

    if(mR > wR) cout << "Ciuvai laimehjo";
    else if(mR < wR) cout << "Cioles laimejo";
    else cout << "Lygu";

	fi.close();

 return 0; }
