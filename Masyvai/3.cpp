
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <math.h>

using namespace std;

    int arrayAverage(double arr[], int l){
        double sum = 0;
        for(int i = 0; i < l; i++)
            sum += arr[i];
        return sum / l;
    }

    int biggestInArrayIndex(double arr[], int l){
        int maxIndex = 0, maxN = arr[0];

        for(int i = 1; i < l; i++)
            if(maxN < arr[i]){ maxIndex = i; maxN = arr[i]; }

        return maxIndex;
    }

    double* insertAndPush(double arr[], int l, double number, int index){
        double* newArray = new double[l + 1];

        for(int i = 0; i < l + 1; i++){
            if(i < index) newArray[i] = arr[i];
            else if(i == index) newArray[i] = number;
            else newArray[i] = arr[i - 1];
        }

        return newArray;
    }

 int main(){

    double arr[99999];
    int arrIndex = 0;
    cout << "Ivestas masyvas: ";
    double j, average = 0; cin >> j; arr[arrIndex] = j;

    while(cin.get() != '\n'){
        cin >> j;
        arrIndex++;
        arr[arrIndex] = j;
    }

    average = arrayAverage(arr, arrIndex + 1);
    double *newArr = insertAndPush(arr, arrIndex + 1, average, biggestInArrayIndex(arr, arrIndex + 1));

    cout << "Naujas masyvas: ";
    for(int i = 0; i < arrIndex + 2; i++)
        cout << newArr[i] << " ";

    cout << endl << "Seno masyvo vidurkis: " << average;
    return 0; }
