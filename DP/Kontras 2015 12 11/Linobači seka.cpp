#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

//dp subsetproblem, gan neefektyvi nes recursija
bool isSubsetSum(int set[], int n, int sum)
{
    bool subset[sum+1][n+1];
    for (int i = 0; i <= n; i++)
      subset[0][i] = true;
    for (int i = 1; i <= sum; i++)
      subset[i][0] = false;
     for (int i = 1; i <= sum; i++)
     {
       for (int j = 1; j <= n; j++)
       {
         subset[i][j] = subset[i][j-1];
         if (i >= set[j-1])
           subset[i][j] = subset[i][j] || subset[i - set[j-1]][j-1];
       }
     }
     return subset[sum][n];
}


 int main(){

    //////////////Failo nuskaitymas
    int arr[1000];
	ifstream fi("Duomenys.txt");
    int n; fi >> n;
    for(int i = 0; i < n; i++)
        fi >> arr[i];
	fi.close();
    //////////////////////

    ofstream fo("Rezultatai.txt");

    for(int i = 1; i < n; i++){
        bool b = isSubsetSum(arr, i, arr[i]);
        if(!b){ fo << i; return 0; } // jei neian sudaryt sumos
    }

    fo << n;
    fo.close();

 return 0; }
