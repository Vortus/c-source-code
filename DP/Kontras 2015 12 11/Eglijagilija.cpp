#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int gilija[50], eglija[50];
    int gAll, eAll, gM, eM;

    int sum(ifstream& fi, int l, int arr[]){
        int result = 0;
        for(int i = 0; i < l; i++){
            int j; fi >> j; result += j * arr[i];
        }
        return result;
    }

 int main(){

    ifstream fi("U1.txt"); fi >> gAll;
    for(int i = 0; i < gAll; i++) fi >> gilija[i];
    gM = sum(fi, gAll, gilija); fi >> eAll;
    for(int i = 0; i < eAll; i++) fi >> eglija[i];
    eM = sum(fi, eAll, eglija);
    fi.close();

    //Gilijos konvercija
    ofstream fo("U1rez.txt");
    int sum = 0;
    for(int i = 0; i < eAll; i++){
        sum += gM / eglija[i];
        fo << eglija[i] << " " << gM / eglija[i] << endl;
        gM %= eglija[i];
    }
    fo << sum << endl; sum = 0;

    //Eglija konvercija
    for(int i = 0; i < gAll; i++){
        sum += eM / gilija[i];
        fo << gilija[i] << " " << eM / gilija[i] << endl;
        eM %= gilija[i];
    }
    fo << sum << endl; sum = 0;

    fo.close();

 return 0; }
