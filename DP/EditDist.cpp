#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int min(int x, int y, int z){
        return min(min(x,y), z);
    }

    int editDist(string s, string t, int m, int n){
        int dp[m + 1][n + 1];
        for(int i = 0; i <= m; i++){
            for(int j = 0; j <= n; j++){
                if(i == 0) dp[i][j] = j;
                else if(j == 0) dp[i][j] = i;
                else if(s[i - 1] == t[j - 1]) dp[i][j] = dp[i - 1][j - 1];
                else dp[i][j] = 1 + min(
                        dp[i][j - 1],
                        dp[i - 1][j],
                        dp[i - 1][j - 1]
                       );
                       cout << dp[i][j];
            }
            cout << endl;
        }
        return dp[m][n];
    }

 int main(){

     cout << editDist("sunday", "saturday", 6, 8);

 return 0; }
