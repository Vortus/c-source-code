#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

using namespace std;

    int maximum(int arr[], int N, int &index){
        int maxs = 0;
        for(int i = 1; i <= N; i++)
            if(arr[i] != -1 && arr[i] > maxs){ maxs = arr[i]; index = i; }
        return maxs;
    }


 int main(){

    /////FAILO NUSKAITYMAS
    ifstream FI("Antras.txt");
    int N; FI >> N;
    int arr[N + 1];
    for(int i = 1; i <= N; i++) FI >> arr[i];
    FI.close();
    ////////
    int result[N + 1];
    fill(result, result + N + 1, -1);

    int index = 0, sum = 0;
    for(int i = 0; i < 4; i++){
        int maxs = maximum(arr, N, index);
        sum += maxs;
        result[index] = maxs;
        arr[index] = -1;
    }

    cout << "Keturkampio atkarpos: ";
    for(int i = 0; i <= N; i++)
        if(result[i] != -1) cout << result[i] << " ";
    cout << endl << "Perimetras: " << sum;

 return 0; }
