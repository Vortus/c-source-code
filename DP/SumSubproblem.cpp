#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    bool sub(int a[], int l, int sum){
        bool m[l + 1][sum + 1];
        for(int i = 0; i <= l; i++) m[i][0] = true;

        for(int i = 1; i <= l; i++){
            for(int j = 1; j <= sum; j++){
                if(j - a[i - 1] >= 0)
                    m[i][j] = m[i - 1][j] || m[i - 1][j - a[i - 1]];
            }
        }
        return m[l][sum];
    }

 int main(){

    int arr[5] { 2, 3, 4, 9, 15 };
    cout << sub(arr, 5, 19);


 return 0; }
