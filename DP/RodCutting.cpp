#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int calc(int n, int p[]){ // p kainos
        int memo[n + 1];
        for(int i = 0; i <= n; i++) memo[i] = 0;

        for(int i = 1; i <= n; i++){
            int maxs = 0;
            for(int j = 1; j <= i; j++){
                int k = p[j] + memo[i - j];
                if(k > maxs) maxs = k;
            }
            memo[i] = maxs;
        }
        return memo[n];
    }

 int main(){

    int n = 8;
    int p[9] = {0, 1, 5, 8, 9, 10, 17, 17, 20};
    cout << calc(n, p);
 return 0; }
