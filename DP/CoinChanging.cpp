#include <iostream>
#include <iomanip>
#include <fstream>
#define MAXINT 2000000000
using namespace std;

 int main(){

    int SUM = 16, N = 4;
    int nominals[5] = { 1, 5, 12, 25 };
    int opt[SUM + 1]; int coins[SUM + 1];
    fill(opt, opt + SUM + 1, MAXINT);
    opt[0] = 0; coins[0] = 0;

    for(int i = 1; i <= SUM; i++){
        for(int j = 0; j < N; j++){
            if(nominals[j] <= i &&
            opt[i - nominals[j]] + 1 < opt[i]){
                opt[i] = opt[i - nominals[j]] + 1;
                coins[i] = nominals[j];
            }
        }
    }

    cout << opt[SUM] << endl;
    while(SUM > 0){
        cout << coins[SUM] << " ";
        SUM -= coins[SUM];
    }


 return 0; }
