#include <iostream>
#include <iomanip>
#include <fstream>
#define MAXINT 2147483647
using namespace std;

 int main(){

    int SUM = 20;
    int N = 4;
    int nominals[4] = { 1, 5, 2, 3 };
    int buffer[SUM + 1];
    fill(buffer, buffer + SUM + 1, MAXINT); buffer[0] = 0;

    for(int i = 1; i <= SUM; i++){
        for(int j = 0; j < N; j++){
            if(nominals[j] <= i && buffer[i - nominals[j]] + 1 < buffer[i]){
                buffer[i] = buffer[i - nominals[j]] + 1;
            }
        }
    }


    for(int i = 0; i <= SUM; i++)
        cout << buffer[i] << " ";

   // cout << buffer[SUM];

 return 0; }
