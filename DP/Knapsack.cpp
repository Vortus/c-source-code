#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    const int MAXS = 12, MAXN = 1000;

    int buffer[2][MAXN];
    void cpy(){
        for(int i = 0; i < MAXN; i++)
            buffer[0][i] = buffer[1][i];
    }

 int main(){

    int V[6] = {0, 1, 5, 8, 11, 20 };
    int S[6] = {0, 1, 2, 3, 4, 7};

    for(int i = 1; i <= 5; i++){
        for(int j = 1; j <= MAXS; j++){
            if(S[i] <= j){
                buffer[1][j] = max(buffer[0][j], V[i] + buffer[0][j - S[i]]);
            } else buffer[1][j] = buffer[0][j];
        }
        cpy();
        for(int k = 0; k <= MAXS; k++)
            cout << buffer[1][k] << " ";
        cout << endl;
    }


 return 0; }
