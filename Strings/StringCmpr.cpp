#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <wctype.h>

using namespace std;

    int compare(string a, string b, int &l){
        if(a.length() < b.length()){
            string temp = a;
            a = b; b = temp;
        }
        l = a.length();
        int res = 0;
        for(int i = 0; i < b.length(); i++)
            if(a[i] == b[i]) res++;
        return res;
    }

    string readFileModif(const char path[]){
        ifstream fi(path);
        string result;
        while(!fi.eof()){
            string line; getline(fi, line);
            for(int i = 0; i < line.length(); i++){
                char c = line[i];
                if(!iswspace(line[i]) && line[i] != '\n') result += line[i];
            }
        }
        fi.close();
        return result;
    }

 int main(){

    int count = 0, l = 0;
    string a, b;
    a = readFileModif("1.txt");
    b = readFileModif("2.txt");
    count = compare(a, b, l);
    cout << count << " tokiu paciu simboliu is " << l << " simboliu, atitikimas " << fixed << setprecision(2)
        << (double)count / l * 100 << "%" << endl;

 return 0; }
