#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

    string reversed(string s){
        int i = 0, j = s.length() - 1;
        while(i < j){
            swap(s[i], s[j]);
            j--; i++;
        }
        return s;
    }

    string reversedd(string s){
        if(s.length() <= 1)
            return s;
        return reversedd(s.substr(0, s.length() - 1)) + s.at(0);
    }

    void displaySpace(int n){
        for(int i = 0; i < n; i++)
            cout << " ";
    }

    void updown(string s, int i){
        if(s.length() == 1){
            cout << s << endl;
            return;
        }
        cout << s << " "; for(int j = 0; j < i; j++) cout << " ";
        cout << reversed(s) << endl;

        updown(s.substr(0, s.length() - 1), i + 2);

        cout << s << " "; for(int j = 0; j < i; j++) cout << " ";
        cout << reversed(s) << endl;
    }

 int main(){

    string s = "Greetings";
    updown(s, 0);

 return 0; }
