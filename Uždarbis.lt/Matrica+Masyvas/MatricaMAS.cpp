#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

#define INF 243124211
//          A  P
int matrica[4][4] = {
    {1, 2, 4, 5},
    {6, 3, 1, 4},
    {2, 1, 4, 2},
    {1, 1, 0, 9},
};


int masyvas[3] = {1, 2, 1};
int matr_plotis = 4, matr_aukstis = 4, mas_plotis = 3;

int stulpelis();

int main(){

    if(matr_aukstis < mas_plotis){
        cout << "Klaida! Masyvo dydis negali buti didesnis uz matricos auksti!" << endl;
        return 0;
    }

    cout << "Yra stulpelyje " << stulpelis();

 return 0; }

void perkopinti(int *mas){

    for(int i = 0; i < mas_plotis; i++){
        mas[i] = masyvas[i];
    }

}

 /// --------------- Randam stulpeli -------------------

 int stulpelis(){
    int tmp[mas_plotis];

    for(int i = 0; i < matr_plotis; i++){
        int c = 0;
        perkopinti(tmp);

        for(int j = 0; j < matr_aukstis; j++){ // Per visos matricos auksti

            for(int k = 0; k < mas_plotis; k++){
                if(tmp[k] == matrica[j][i]){
                    c++;
                    tmp[k] = INF;
                    break;
                }
            }
        }

        if(c == mas_plotis) return i + 1;
    }

    return -1;

 }

