#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
using namespace std;

#define MAXC 11
#define FILE "U1.txt"

void readFile(int &N, int &KM, int &traveledDistance, int &traveledPlaces, string &lastStop);
void displayResults(int traveledPlaces, int traveledDistance, string lastStop);

int distanceToPoint(int x, int y);

 int main(){

    int N, KM, traveledDistance = 0, traveledPlaces = 0;
    string lastStop;

    readFile(N, KM, traveledDistance, traveledPlaces, lastStop);
    displayResults(traveledPlaces, traveledDistance, lastStop);

 return 0; }

/// ------------- Išvestis(tingiu į failą)  -----------------

void displayResults(int traveledPlaces, int traveledDistance, string lastStop){
    cout << traveledPlaces << " " << traveledDistance << " " << lastStop << endl;
}

/// ------------- Failo nuskaitymas -----------------

void readFile(int &N, int &KM, int &traveledDistance, int &traveledPlaces, string &lastStop){
    int i, x, y; // Kintamieji
    char tmp[MAXC];

    ifstream FI(FILE);
    FI >> N >> KM;

    for(i = 0; i < N; i++){
        FI.ignore();
        FI.get(tmp, MAXC);
        FI >> x >> y;

        if(KM < traveledDistance + distanceToPoint(x, y)) break;
        lastStop = tmp;
        traveledDistance += distanceToPoint(x, y);
        traveledPlaces++;
    }

    FI.close();

}

/// ------------- Atstumas iki taško  -----------------

int distanceToPoint(int x, int y){
    return (abs(x) + abs(y)) * 2;
}
