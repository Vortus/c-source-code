
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <cstdlib>

using namespace std;


void meniu();
void baigti();
void spausdinti_matrica(ostream &, double [][100], int, int);
int formuoja_matrica(istream &, double (&)[100][100], int, int);
double matricos_elementu_vidurkis(double [][100], int, int);
int gauti_eilute(double [][100], int, int, char);
int neigiamiausias_stuleplis(double [][100], int, int);
bool ar_egzistuoja_toks_matricos_elementas(int, int, int, int);
int iveda_matrica_is_failo(string, double (&)[100][100], int &, int &);
bool keisti_matricos_elementa_nauja_reiksme(double (&)[100][100], int, int, int, int, double);
void keistiKvadratais(double (&)[100][100], int, int, int);
void rikiuoti_matrica(double (&)[100][100], int, int, int);
double dauginti_teigiamus_elementus(double [][100], int, int);
void sukurti_matrica_pagal_atrinktus_elementus(double (&)[100][100], double (&)[100], int, int);
bool sukeicia_min_max_stulpelius(double (&)[100][100], int, int);
void debug(int, int);
void suformuoti_didziausiu_matricos_elementu_masyva(double (&)[100][100], int, int, double (&)[100]);
void keisti_neigiamus_elementus_kvadratais(double (&)[100][100], int, int, int);

int main() {
        // kintamuju aprasai
        int x, y, matrica_jau_suformuota = 0;
        double didziausi_elementai[100];
        string kelias, veiksmas;
        string veiksmai[10] = {"3", "4", "5", "6", "7", "8", "9", "10", "11", "13"};
        bool veiksmas_egzistuoja = false;
        double matrica[100][100];
        int maziausiai_elementas_yra_sioje_eiluteje;

        cout << "Laboratorinis darbas nr. 3, Lukas Liesis, GM080102\n\n";
        cout << "Programa atlieka ivairius veiksmus su matrica pagal vartotojo pasirinkima\n";
        cout << "Matrica galima ivesti is failo arba ranka\n";
        cout << "Norima ivedimo varianta pasirinkite is meniu ivedus 1 arba 2\n\n";
        cout << "Matricos failo formatas yra toks:\n";
        cout << "Pirmoje eiluteje nurodyti 2 skaiciai parodo kokio dydzio yra matrica\n";
        cout << "Pirmas skaicius nurodo stulpeliu skaiciu, antras  eiluciu\n";
        cout << "Kiekvienoje kitoje eiluteje yra surasomi matricos elementu skaicia\n\n";

        // meniu spausdinimas
        meniu();
        // pagrindinis programos ciklas
        do {
                veiksmas_egzistuoja = false;
                cout << "\nIveskite norimo atlikti veiksmo meniu numeri: ";
                cin >> veiksmas;
                cout << "\n";
                if (veiksmas == "0") {
                        // rodyti meniu
                        meniu();
                } else if (veiksmas == "12") {
                        // pabaigti programa
                        baigti();
                } else if (veiksmas == "1"){
                        // ivedimas is failo
                        cout << "Kelias iki failo: ";
                        cin >> kelias;
                        matrica_jau_suformuota = iveda_matrica_is_failo(kelias, matrica, x, y);
                        if (matrica_jau_suformuota <= 0) {
                                cout << "Klaida! Blogas kelias iki failo\n";
                        } else {
                                cout << "Matrica ivesta. Ji sudaryta is " << x << " stulpeliu ir " << y << " eiluciu.\n";
                                spausdinti_matrica(cout, matrica, x, y);
                        }
                } else if (veiksmas == "2") {
                        // rankiniu budu ivesti matrica
                        cout << "Iveskite matricos stulpeliu skaiciu:\n";
                        cin >> x;
                        cout << "Iveskite matricos eiluciu skaiciu:\n";
                        cin >> y;
                        if (cin.good()) {
                                if (ar_egzistuoja_toks_matricos_elementas(100, 100, x, y)) { // ar matricos matmenys nevirsija masyvo
                                        cout << "Iveskite matricos reiksmes: \n";
                                        matrica_jau_suformuota = formuoja_matrica(cin, matrica, x, y);
                                        if (matrica_jau_suformuota == -1 || matrica_jau_suformuota == -2) {
                                                cout << "Klaida! Neteisingi matricos duomenys.\n";
                                        } else {
                                                cout << "Matrica ivesta. Ji sudaryta is " << x << " stulpeliu ir " << y << " eiluciu.\n";
                                                spausdinti_matrica(cout, matrica, x, y); // spausdinam matrica
                                        }
                                } else {
                                        cout << "Klaida! Toks eiluciu ar stulpeliu kiekis yra negalimas.\n";
                                        cout << "Daugiausia gali buti 100 eiluciu ir 100 stulepliu.\n";
                                }
                        }
                }  else {
                        // tikrinam ar egzistuoja nurodytas veiksmas
                        // prasukam cikla pro visus veiksmus
                        for(int i = 0; i < sizeof(veiksmai)/sizeof(string); i++) {
                                if (veiksmas == veiksmai[i]) {
                                        veiksmas_egzistuoja = true;
                                        break;
                                }
                        }

                        if (veiksmas_egzistuoja) {
                                if (matrica_jau_suformuota == 1) { // tikrinam ar matrica jau suformuota

                                        if (veiksmas == "3") {  // skaiciuoti matricos elementu aritmetini vidurki
                                                double vidurkis = matricos_elementu_vidurkis(matrica, x, y); // gaunam vidurki
                                                cout << "Matricos aritmetinis vidurkis yra: " << vidurkis << "\n";
                                        } else if (veiksmas == "4") { // pakeisti pasirinkta elementa nauja reiksme
                                                int x1, y1;
                                                double n;
                                                cout << "Iveskite keiciamos eilutes nr: ";
                                                cin >> x1;
                                                cout << "\nIveskite keiciamo stulpelio nr: ";
                                                cin >> y1;
                                                if (cin.good()) {
                                                        cout << "\nIveskite nauja elemento reiksme: ";
                                                        cin >> n;
                                                        if (cin.good()) {
                                                                if (keisti_matricos_elementa_nauja_reiksme(matrica, x, y, x1, y1, n)) { // keicia elementa
                                                                        cout << "\nElemento reiksme pakeista sekmingai \n\n";
                                                                        spausdinti_matrica(cout, matrica, x, y);
                                                                } else {
                                                                        cout << "Klaida! Toks matricos elementas neegistuoja";
                                                                }
                                                        }
                                                }
                                        } else if (veiksmas == "5") {   // Rasti kiekvienos eilutes didziausia elementa
                                                double nauja_matrica[100];
                                                sukurti_matrica_pagal_atrinktus_elementus(matrica, nauja_matrica, x, y); // formuojam nauja matrica
                                                cout << "Didziausi kiekvienos matricos eilutes elementai yra: \n\n";
                                                for(int i = 0; i < y; i++) { // prasuka pro visas eilutes
                                                        cout << i + 1 << " eilutes didziausias elementas yar: " << nauja_matrica[i] << "\n"; // Y eilutes didziausias elementas yra: bla bla
                                                }
                                        } else if (veiksmas == "6") {   // Sudauginti teigiamus elementus is eilutes, kurioje yra didziausias matricos elementas
                                                //debug(x, y);
                                                int eilute = gauti_eilute(matrica, x, y, 'd') + 1; // suranda kuri eilute turi didziausia elementa
                                                cout << "Didziausias elementas yra " << eilute << " eiluteje.\n";
                                                cout << eilute << " eilutes teigiamu elementu sandauga lygi: ";
                                                cout << std::fixed << dauginti_teigiamus_elementus(matrica, eilute - 1, x) <<  "\n";
                                                cout << resetiosflags( ios::fixed ) ;

                                        } else if (veiksmas == "7") {   // suranda stulpeli, kuriame yra didziausia suma neigiamu elementu
                                                int id = neigiamiausias_stuleplis(matrica, x, y);
                                                suformuoti_didziausiu_matricos_elementu_masyva(matrica, y, x, didziausi_elementai);
                                                if (id == -1) {
                                                        cout << "Klaida! Neigiamu elementu matricoje nera\n";
                                                } else {
                                                        cout << "Didziausia neigiamu elementu suma turintis stulpelis yra: " << id + 1 << "\n";
                                                }
                                        } else if (veiksmas == "8") {   // sukeicia daugiausia neigiamu elementu turinti stulpeli su maziausiai turinciu
                                                cout << "Sukeicia daugiausia neigiamu elementu turinti stulpeli su maziausiai turinciu\n";
                                                if (sukeicia_min_max_stulpelius(matrica, x, y)) {// matricos stulpeliu sukeitimas
                                                        cout << "Stulpeliai sukeisti sekmingai\n\n";
                                                        spausdinti_matrica(cout, matrica, x, y);
                                                } else {
                                                        cout << "Klaida! Teigiamu elementu matricoje nera\n";
                                                }
                                        }  else if (veiksmas == "9") {
                                                spausdinti_matrica(cout, matrica, x, y); // spausdinam matrica ekrane
                                        } else if (veiksmas == "10") { // spausdinam matrica i faila
                                                cout << "Iveskite kelia iki failo: ";
                                                cin >> kelias;
                                                ofstream failas(kelias.c_str());
                                                if (!failas.fail()) {
                                                        // irasom matricos dydi
                                                        failas << x << " ";
                                                        failas << y << "\n";
                                                        spausdinti_matrica(failas, matrica, x, y); // irasom matrica
                                                        cout << "\nMatrica sekmingai irasyta.\n";
                                                } else {
                                                        cout << "Klaida! Blogas failas. Jis neegzistuoja arba yra pazeistas\n";
                                                }
                                                failas.close();
                                        } else if (veiksmas == "11") {
                                                int st;
                                                cout << "Iveskite stulpeli, pagal kuri bus rikiuojama matrica didejimo tvarka: ";
                                                cin >> st;
                                                if (cin.good()) {
                                                        if (ar_egzistuoja_toks_matricos_elementas(y, x, 1, st)) {
                                                                rikiuoti_matrica(matrica, x, y, st - 1); // rikiuojam matrica
                                                                cout << "Matrica surusiuota\n\n";
                                                                spausdinti_matrica(cout, matrica, x, y); // spausdinam
                                                        } else {
                                                                cout << "Klaida! Sio stulpelio nera\n";
                                                        }
                                                }
                                        } else if(veiksmas == "13"){
                                                // kvadratais pakeisti po maziausia elementa turincios eilutes
                                                maziausiai_elementas_yra_sioje_eiluteje = gauti_eilute(matrica, y, x, 'm') + 1; // suranda kuri eilute turi didziausia elementa
                                                cout << "Maziausias elementas yra " << maziausiai_elementas_yra_sioje_eiluteje << " eiluteje.\n";
                                                keisti_neigiamus_elementus_kvadratais(matrica, y, x, maziausiai_elementas_yra_sioje_eiluteje);
                                                cout << "Matricos elementai pakeisti.\n";
                                                spausdinti_matrica(cout, matrica, x, y);
                                        }
                                } else {
                                        cout << "Klaida! Pries pasirinkdami si meniu punkta turite ivesti matrica!\n";
                                }
                        } else {
                                cout << "Klaida! Pasirinkote meniu punkta, kurio nera\n";
                        }
                }
                if (cin.fail()) {// jei ivesti neteisingi duomenys, sutvarkom cina ir pranesam apie klaida
                        cin.clear();
                        cin.ignore(256, '\n');
                        cout << "Klaida! Neteisingi parametrai\n";
                }
        } while(true); // ciklas vyksta visa programos veikimo laika

        return 0;
}


// meniu punktai

void meniu() {
        cout << "[0] Meniu\n";                                                                                                                                                                                                                  // done
        cout << "[1] Formuoti matrica is failo\n";                                                                                                                                                                              // done
        cout << "[2] Formuoti matrica su klaviatura\n";                                                                                                                                                                 // done
        cout << "[3] Skaiciuoti matricos elementu aritmetini vidurki\n";                                                                                                                                // done
        cout << "[4] Keisti matricos elementa\n";                                                                                                                                                                               // done
        cout << "[5] Rasti kiekvienos eilutes didziausia elementa\n";                                                                                                                                   // done
        cout << "[6] Sudauginti teigiamus elementus is eilutes, kurioje yra didziausias matricos elementas\n";                                                  // done
        cout << "[7] Surasti didziausia neigiamu elementu suma turinti matricos stulpeli\n";                                                                                    // done
        cout << "[8] Sukeisti vietomis maziausiai ir daugiausiai teigiamu elementu turincius stulpelius vietomis\n";                                    // done
        cout << "[9] Spausdinti matricja\n";                                                                                                                                                                                    // done
        cout << "[10] Irasyti matrica i faila\n";                                                                                                                                                                               // done
        cout << "[11] Rusiuoti matrica pagal pasirinkta stulpeli\n";
        cout << "[12] Baigti darba\n";                                                                                                                                                                                                  // done
        cout << "[13] Visose matricos eilutese, esanciose zemiau tos eilutes, kurioje yra maziausias matricos elementas, neigiamus pakeisti ju kvadratais";
}

void baigti() {
        exit(0);
}


// gauna eilute spausdinimui matricos i konsole arba faila

void spausdinti_matrica(ostream &duomenys, double matrica[][100], int x, int y) {
        duomenys.precision(3); // 3 skaiciu po kableliu tikslumu
        for(int i = 0; i < y; i++) {
                for(int j = 0; j < x; j++) {
                        duomenys.width(5);                                      // duomeny plotis
                        duomenys.fill(' ');                                     // duomenu pradinis uzpildymas
                        duomenys << matrica[i][j] << " ";       // iveda matricos elemento reiksme
                }
                duomenys << "\n"; // pereina i nauja eilute
        }
}

// suformuoja matrica

int formuoja_matrica(istream &duomenys, double (&matrica)[100][100], int x, int y) {
        duomenys.precision(3); // tikslumas - 3 sk. po kablelio
        for(int i = 0; i < y; i++)      {
                for(int j = 0; j < x; j++)              {
                        duomenys >> matrica[i][j];
                        if (duomenys.fail()) // jei klaida - iseina is f-cijos su -1 o ne 1
                                return -1;
                }
        }
        return 1;
}

// iveda matrica is failo

int iveda_matrica_is_failo(string kelias, double (&matrica)[100][100], int &x, int &y) {
        int s = -1;
        ifstream failas(kelias.c_str());
        if (!failas.is_open()) { // atidarom
                return -1;
        }

        failas >> x >> y; // gaunam eiluciu ir stulpeliu skaiciu
        if (cin.good()) {
                if (ar_egzistuoja_toks_matricos_elementas(100, 100, x, y)) {
                        s = formuoja_matrica(failas, matrica, x, y); // formavimas
                }
        }
        failas.close(); // uzdarom
        return s;
}

// matricos elementu vidurkis

void debug(int x, int y){
        cout << "x: " << x << " Y: " << y;
}

double matricos_elementu_vidurkis(double matrica[][100], int y, int x) {
        double suma = 0;
        for(int i = 0; i < y; i++) {
                for(int j = 0; j < x; j++) {
                        suma = suma + matrica[i][j];
                }
        }
        return (double) suma / (y * x);
}

// grazina stuleplio id, kuris buvo neigiamiausias. t.y. sudeda neigiamus elementus ir palygina

int neigiamiausias_stuleplis(double matrica[][100], int x, int y) {
        double suma[100], min;
        int e = 0, id;

    // nulinam
        for(int i = 0; i < 100; i++) {
                suma[i] = 0;
        }
        // skaiciuojam stulpeliu neigiamu elementu suma
        for(int i = 0; i < x; i++) {
                for(int j = 0; j < y; j++) {
                        if (matrica[j][i] < 0) {
                                suma[i] = suma[i] + matrica[j][i];
                                e = 1;
                        }
                }
        }
        if (!e) {
                return -1;
        }
        // randam indeksa
        min = suma[0];
        id = 0;
        for(int i = 0; i < x; i++) {
                if (suma[i] < min) {
                        min = suma[i];
                        id = i;
                }
        }
        return id;
}

// ar matricoje x y egzistuoja elementas x1 y1

bool ar_egzistuoja_toks_matricos_elementas(int x, int y, int x1, int y1) {
        if (0 < x1 && x1 <= x && 0 < y1 && y1 <= y){    // x1 ir y1 turi buti [0...x] arba [0...y]
                return true;
        }
        return false;
}


// grazina eilutes indeksa, kuri eilute turi didziausia arba maziausia elementa

int gauti_eilute(double matrica[][100], int x, int y, char ka_veikt) {
        if (ka_veikt != 'd' && ka_veikt != 'm') {       // ka daryt ar rast maziausia (m) ar didziausia (d)
        //      cout << "ka veitk: " << ka_veikt << "\n\n\n";
                return -1;
        }
        int eilute = 0;
        double n = matrica[0][0];
        for(int i = 0; i < x; i++) {
                for(int j = 0; j < y; j++) {
                        if (ka_veikt == 'd') {
                                if (matrica[i][j] > n) {
                                        n = matrica[i][j];
                                        eilute = i;
                                }
                        } else if (ka_veikt == 'm') {
                                if (matrica[i][j] < n) {
                                        n = matrica[i][j];
                                        eilute = i;
                                }
                        }
                }
        }
        return eilute;
}

// sudaugina tieigiamus elementus pateiktos matricos

double dauginti_teigiamus_elementus(double matrica[][100], int y, int x) {
        double sandauga = 1;
        for(int i = 0; i < x; i++) {
                if (matrica[y][i] > 0) {
                        sandauga = (double)sandauga * (double)matrica[y][i];
                }
        }
        return sandauga;
}


// sukuria nauja matrica pagal atrinktus kiekvienos eilutes didziausius elementus

void sukurti_matrica_pagal_atrinktus_elementus(double (&matrica)[100][100], double (&nauja_matrica)[100], int y, int x) {
        double max;
        for(int i = 0; i < x; i++) {
                max = matrica[i][0];                    // didziausias pirmas elementas
                for(int j = 0; j < y; j++) {    // prasisuka pro visus eilutes elementus
                        if (matrica[i][j] > max) {
                                max = matrica[i][j];
                        }
                }
                nauja_matrica[i] = max;                 //formuoja nauja matrica
        }
}


// keicia matricos elemento reiksme, taip pat patikrina ar tas elementas egzistuoja

bool keisti_matricos_elementa_nauja_reiksme(double (&matrica)[100][100], int x, int y, int x1, int y1, double reiksme) {
        if (ar_egzistuoja_toks_matricos_elementas(x, y, y1, x1)) {
                matrica[x1 - 1][y1 - 1] = reiksme;
                return true;
        }
        return false;
}

//rikiavimas pagal pasirinkta stulpeli didejimo tvarka
void rikiuoti_matrica(double (&matrica)[100][100], int x, int y, int st) {
        int mini = 0;
        double min = matrica[0][0], tmp;
        for(int i = 0; i < y; i++) {
                for(int j = 0; j < y; j++) {
                        if (matrica[j][st] > matrica[i][st])  {
                                for(int k = 0; k < x; k++) {
                                        tmp = matrica[i][k];
                                        matrica[i][k] = matrica[j][k];
                                        matrica[j][k] = tmp;
                                }
                        }
                }
        }
}


// sukeicia vietomis minimalu ir maksimalu stulpelius

bool sukeicia_min_max_stulpelius(double (&matrica)[100][100], int x, int y) {
        int e[100], c = 0, mini, maxi;
        double max, min, tmp;
        //skaiciuojam kiek teigiamu elementu turi kiekvienas stulpelis
        for(int i = 0; i < x; i++) {
                e[i] = 0;
                for(int j = 0; j < y; j++) {
                        if (matrica[j][i] > 0) {
                                e[i]++;
                        }
                }
        }
        // randam stulpeli, kuriame daugiausiai teigiamu elementu
        max = e[0];
        maxi = 0;
        for(int i = 0; i < x; i++) {
                if (e[i] > max) {
                        max = e[i];
                        maxi = i;
                }
        }
        if (max == 0) {         // nera ne vieno teigiamo elemento
                return false;
        }

        min = e[0];
        mini = 0;
        for(int i = 0; i < x; i++) {
                if (e[i] < min) {
                        min = e[i];
                        mini = i;
                }
        }
        // apkeiciam stulpelius vietomis
        for(int i = 0; i < y; i++) {
                tmp = matrica[i][maxi];
                matrica[i][maxi] = matrica[i][mini];
                matrica[i][mini] = tmp;
        }
        return true;
}



void suformuoti_didziausiu_matricos_elementu_masyva(double (&masyvas)[100][100], int y, int x,  double (&naujas_masyvas)[100]){
        int didziausias;
        for(int i = 0; i < y; i++){
                didziausias = 0;
                // randa didziausia sios eilutes
                for(int j = 0; j < y; j++){
                        if(masyvas[i][didziausias] > masyvas[i][j]){
                                didziausias = j;
                        }
                }
                naujas_masyvas[i] = masyvas[i][didziausias];
        }
}

void keisti_neigiamus_elementus_kvadratais(double (&masyvas)[100][100], int y, int x, int nuo_eilutes){
        for(int i = nuo_eilutes; i < y; i++){
                for(int j = 0; j < x; j++){
                        if(masyvas[i][j] < 0){
                                masyvas[i][j] = masyvas[i][j] * masyvas[i][j];
                        }
                }
        }
}
