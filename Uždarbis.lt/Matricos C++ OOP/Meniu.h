#ifndef MENIU_H_INCLUDED
#define MENIU_H_INCLUDED
#include "MatricosValdikliai.h"
#include <iostream>

class Meniu{

    private:
        bool busena; // Būsena (Įjungas/Išjungtas)

    public:
        Meniu(bool busena) : busena(busena){} // Konstruktorius
        ~Meniu(){} // Destructorius

        bool gauti_busena(){ // Būsena (Įjungas/Išjungtas)
            return busena;
        }

        void pakeisti_busena(bool b){ // Pakeičia meniu būseną
            busena = b;
        }

        /// ----------------- Meniu Funkcijos ----------------------

        void skaityti_matrica_failas(Matrica *&matrica, string &kelias, int &matrica_jau_suformuota){ /// Matricos įvedimas iš failo
            system("CLS"); // Pravalom ekraną
            cout << "Kelias iki failo: "; cin >> kelias;
            matrica_jau_suformuota = iveda_matrica_is_failo(kelias, matrica);
            system("CLS"); // Pravalom ekraną

            if (matrica_jau_suformuota <= 0) {
                cout << "Klaida! Blogas kelias iki failo\n\n";
            } else {
                cout << "Matrica ivesta. Ji sudaryta is " << matrica->gauti_ploti() << " stulpeliu ir " << matrica->gauti_auksti() << " eiluciu.\n\n";
                matrica->spausdinti(cout);
            }

        }

        void skaityti_matrica(Matrica *&matrica, int &matrica_jau_suformuota){ /// Matricos įvedimas iš failo
            int plotis, aukstis;
            system("CLS"); // Pravalom ekraną
            cout << "Iveskite matricos stulpeliu skaiciu:\n"; cin >> plotis;
            cout << "Iveskite matricos eiluciu skaiciu:\n"; cin >> aukstis;

            if(cin.good()){
                if(ar_egzistuoja_toks_matricos_elementas(100, 100, aukstis, plotis)){
                    system("CLS"); // Pravalom ekraną

                    cout << "Iveskite matricos reiksmes: \n";
                    matrica = new Matrica(aukstis, plotis);
                    matrica_jau_suformuota = matrica->formuoti(cin);
                    system("CLS"); // Pravalom ekraną

                    if (matrica_jau_suformuota == -1 || matrica_jau_suformuota == -2) {
                        cout << "Klaida! Neteisingi matricos duomenys.\n";
                    } else {
                        cout << "Matrica ivesta. Ji sudaryta is " << plotis << " stulpeliu ir " << aukstis << " eiluciu.\n\n";
                        matrica->spausdinti(cout);
                    }

                } else {
                    cout << "Klaida! Toks eiluciu ar stulpeliu kiekis yra negalimas.\n";
                    cout << "Daugiausia gali buti 100 eiluciu ir 100 stulepliu.\n";
                }
            }

        }

        void keisti_elementa(Matrica *matrica){ /// Matricos elemento keitimas
            int x1, y1;
            double n;
            cout << "Iveskite keiciamos eilutes nr: "; cin >> x1;
            cout << "Iveskite keiciamo stulpelio nr: "; cin >> y1;
            system("CLS");

            if (cin.good()) {
                cout << "Iveskite nauja elemento reiksme: "; cin >> n;
                system("CLS");

                if (cin.good()) {
                    if (ar_egzistuoja_toks_matricos_elementas(matrica->gauti_auksti(), matrica->gauti_ploti(), x1, y1)) { // keicia elementa
                        matrica->priskirti_reiksme(y1 - 1, x1 - 1, n);
                        cout << "Elemento reiksme pakeista sekmingai \n\n";
                        matrica->spausdinti(cout);
                    } else {
                        cout << "Klaida! Toks matricos elementas neegistuoja\n\n";
                    }
                }
            }
        }

        void vykdyti_meniu(int pasirinkimas, string &kelias, Matrica *&matrica, int &matrica_jau_suformuota){ // Vykdom meniu veiksmus
            bool bande_su_matrica = false; // Bandė atlikti veiksmą su matrica

            switch(pasirinkimas){
                case 1: /// Nuskaitom matricą iš failo

                    skaityti_matrica_failas(matrica, kelias, matrica_jau_suformuota); // Nuskaitom matricą iš failo
                    break;

                case 2: /// Nuskaitom matricą iš klaviatūros

                    skaityti_matrica(matrica, matrica_jau_suformuota); // Nuskaitom matricą iš failo
                    break;

                case 3: /// Matricos vidurkis

                    system("CLS");
                    if(matrica_jau_suformuota) cout << "Matricos aritmetinis vidurkis yra: " << matricos_elementu_vidurkis(matrica) << "\n\n";
                    bande_su_matrica = true;
                    break;

                case 4: /// Keisti elementa

                    system("CLS");
                    if(matrica_jau_suformuota)
                        keisti_elementa(matrica);

                    bande_su_matrica = true;
                    break;

                case 5: /// Kiekvienos eilutės didžiausias elementas

                    system("CLS");
                    if(matrica_jau_suformuota){
                    double nauja_matrica[100];
                    sukurti_matrica_pagal_atrinktus_elementus(matrica, nauja_matrica); // formuojam nauja matrica
                    cout << "Didziausi kiekvienos matricos eilutes elementai yra: \n\n";
                    for(int i = 0; i < matrica->gauti_auksti(); i++) // prasuka pro visas eilutes
                        cout << i + 1 << " eilutes didziausias elementas yar: " << nauja_matrica[i] << "\n"; // Y eilutes didziausias elementas yra: bla bla                                                }
                        cout << "\n";
                    }
                    bande_su_matrica = true;
                    break;

                case 6: /// Dauginimas....
                    system("CLS");
                    if(matrica_jau_suformuota){
                        int eilute = gauti_eilute(matrica, 'd') + 1; // suranda kuri eilute turi didziausia elementa
                        cout << "Didziausias elementas yra " << eilute << " eiluteje.\n";
                        cout << eilute << " eilutes teigiamu elementu sandauga lygi: ";
                        cout << fixed << dauginti_teigiamus_elementus(matrica, eilute - 1) <<  "\n\n";
                        cout << resetiosflags( ios::fixed );
                    }
                    bande_su_matrica = true;
                    break;

                case 7: /// Dauginimas....
                    system("CLS");
                    if(matrica_jau_suformuota){
                        int id = neigiamiausias_stuleplis(matrica);
                        if (id == -1) {
                            cout << "Klaida! Neigiamu elementu matricoje nera\n\n";
                        } else {
                            cout << "Didziausia neigiamu elementu suma turintis stulpelis yra: " << id + 1 << "\n\n";
                        }
                    }
                    bande_su_matrica = true;
                    break;

                case 8: /// Sukeitimas
                    system("CLS");
                    if(matrica_jau_suformuota){
                        cout << "Sukeicia daugiausia neigiamu elementu turinti stulpeli su maziausiai turinciu\n";
                        if (sukeicia_min_max_stulpelius(matrica)) {// matricos stulpeliu sukeitimas
                            cout << "Stulpeliai sukeisti sekmingai\n\n";
                            matrica->spausdinti(cout);
                        } else {
                            cout << "Klaida! Teigiamu elementu matricoje nera\n\n";
                        }
                    }

                    bande_su_matrica = true;
                    break;

                case 9: /// Spausdinti matricą

                    system("CLS");
                    if(matrica_jau_suformuota){
                        cout << "Matrica \n\n";
                        matrica->spausdinti(cout);
                    }
                    bande_su_matrica = true;
                    break;

                case 10: /// Įrašom į failą
                    system("CLS");
                    if(matrica_jau_suformuota){
                        cout << "Iveskite kelia iki failo: "; cin >> kelias;
                        ofstream failas(kelias.c_str());
                        if (!failas.fail()) {
                            failas << matrica->gauti_ploti() << " ";
                            failas << matrica->gauti_auksti() << "\n";
                            matrica->spausdinti(failas); // irasom matrica
                            cout << "\nMatrica sekmingai irasyta.\n\n";
                        } else {
                            cout << "Klaida! Blogas failas. Jis neegzistuoja arba yra pazeistas\n\n";
                        }
                        failas.close();
                    }

                    bande_su_matrica = true;
                    break;

                case 11: /// Įrašom į failą
                    system("CLS");
                    if(matrica_jau_suformuota){
                        int st;
                        cout << "Iveskite stulpeli, pagal kuri bus rikiuojama matrica didejimo tvarka: "; cin >> st;
                        if (cin.good()) {
                            if (ar_egzistuoja_toks_matricos_elementas(matrica->gauti_auksti(), matrica->gauti_ploti(), 1, st)) {
                                rikiuoti_matrica(matrica, st - 1); // rikiuojam matrica
                                cout << "Matrica surusiuota\n\n";
                                matrica->spausdinti(cout); // spausdinam
                            } else {
                                cout << "Klaida! Sio stulpelio nera\n\n";
                            }
                        }
                    }

                    bande_su_matrica = true;
                    break;

                case 12: /// Keitimai
                    system("CLS");
                    if(matrica_jau_suformuota){
                        int maziausiai_elementas_yra_sioje_eiluteje = gauti_eilute(matrica, 'm') + 1; // suranda kuri eilute turi didziausia elementa
                        cout << "Maziausias elementas yra " << maziausiai_elementas_yra_sioje_eiluteje << " eiluteje.\n";
                        keisti_neigiamus_elementus_kvadratais(matrica, maziausiai_elementas_yra_sioje_eiluteje - 1);
                        cout << "Matricos elementai pakeisti.\n\n";
                        matrica->spausdinti(cout);
                    }

                    bande_su_matrica = true;
                    break;

                case 13: /// Išjungiam meniu

                    pakeisti_busena(false);
                    break;

                default:

                    system("CLS"); // Pravalom ekraną
                    break;

            }

            if(bande_su_matrica && !matrica_jau_suformuota){
                system("CLS");
                cout << "Klaida! Pries pasirinkdami meniu punkta turite ivesti matrica!\n\n";
            }
        }


};

#endif // MENIU_H_INCLUDED
