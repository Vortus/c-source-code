/*
    Čia laikomi pagrindiniai kintamieji ir įtraukimai.
*/

#ifndef KINTAMIEJI_H_INCLUDED
#define KINTAMIEJI_H_INCLUDED

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <cstdlib>
#include "Matrica.h" // Matricos klasė
#include "Meniu.h" // Meniu valdiklis
using namespace std;

    void spausdinti_meniu() { /// Meniu punktai

        cout << "[1] Formuoti matrica is failo\n";
        cout << "[2] Formuoti matrica su klaviatura\n";
        cout << "[3] Skaiciuoti matricos elementu aritmetini vidurki\n";
        cout << "[4] Keisti matricos elementa\n";
        cout << "[5] Rasti kiekvienos eilutes didziausia elementa\n";
        cout << "[6] Sudauginti teigiamus elementus is eilutes, kurioje yra didziausias matricos elementas\n";
        cout << "[7] Surasti didziausia neigiamu elementu suma turinti matricos stulpeli\n";
        cout << "[8] Sukeisti vietomis maziausiai ir daugiausiai teigiamu elementu turincius stulpelius vietomis\n";
        cout << "[9] Spausdinti matricja\n";
        cout << "[10] Irasyti matrica i faila\n";
        cout << "[11] Rusiuoti matrica pagal pasirinkta stulpeli\n";
        cout << "[12] Visose matricos eilutese, esanciose zemiau tos eilutes, kurioje yra maziausias matricos elementas, neigiamus pakeisti ju kvadratais\n";
        cout << "[13] Baigti darba\n\n";

    }

    void spausdinti_izanga(){ /// Įžanga

        cout << "Laboratorinis darbas nr. 3, Lukas Liesis, GM080102\n\n";
        cout << "Programa atlieka ivairius veiksmus su matrica pagal vartotojo pasirinkima\n";
        cout << "Matrica galima ivesti is failo arba ranka\n";
        cout << "Norima ivedimo varianta pasirinkite is meniu ivedus 1 arba 2\n\n";
        cout << "Matricos failo formatas yra toks:\n";
        cout << "Pirmoje eiluteje nurodyti 2 skaiciai parodo kokio dydzio yra matrica\n";
        cout << "Pirmas skaicius nurodo stulpeliu skaiciu, antras  eiluciu\n";
        cout << "Kiekvienoje kitoje eiluteje yra surasomi matricos elementu skaicia\n\n";

    }

#endif // KINTAMIEJI_H_INCLUDED
