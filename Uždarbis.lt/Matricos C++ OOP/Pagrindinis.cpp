#include "Headeris.h"
using namespace std;

 int main(){

    /// --------------- Kintamieji ----------------

    string kelias; // Kelias iki failo

    int veiksmas = -1; // Meniu veiksmas
    int veiksmai[10] = {3, 4, 5, 6, 7, 8, 9, 10, 11, 13};

    Matrica *matrica; // Matrica
    Meniu meniu = Meniu(true); // Sukuriam meniu ir paleidžiam

    int matrica_jau_suformuota = 0;
    bool veiksmas_egzistuoja = false;

    /// ------------- Įžanga ---------------

    spausdinti_izanga(); // Spausdinam įžangą

    /// --------------- Pagrindinis loopas --------------------

    while(meniu.gauti_busena()){ // Kol meniu įjungtas

        veiksmas_egzistuoja = false;
        spausdinti_meniu(); // Spausdinam meniu

        cout << "Iveskite norimo atlikti veiksmo meniu numeri: "; cin >> veiksmas; // Nuskaitom veiksmą
        meniu.vykdyti_meniu(veiksmas, kelias, matrica, matrica_jau_suformuota); // Vykdom veiksmą

    }

 return 0; }
