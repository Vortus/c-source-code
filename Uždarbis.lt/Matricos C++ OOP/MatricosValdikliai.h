#ifndef MATRICOSVALDIKLIAI_H_INCLUDED
#define MATRICOSVALDIKLIAI_H_INCLUDED
#include "Matrica.h"
#include <fstream>
#include <iostream>

bool ar_egzistuoja_toks_matricos_elementas(int x, int y, int x1, int y1) {
    return 0 < x1 && x1 < x && 0 < y1 && y1 < y;
}

int iveda_matrica_is_failo(string kelias, Matrica *&matrica) {
    int s = -1, aukstis, plotis;

    ifstream failas(kelias.c_str());
    if (!failas.is_open()) { // Atidarom
            return -1;
    }

    failas >> plotis >> aukstis; // Gaunam stulpelių ir eilučių skaičių
    matrica = new Matrica(aukstis, plotis);

    if (cin.good() && ar_egzistuoja_toks_matricos_elementas(100, 100, aukstis, plotis))
        s = matrica->formuoti(failas); // Formavimas

    failas.close(); // Uždarom
    return s;
}

double matricos_elementu_vidurkis(Matrica *matrica) {
    double suma = 0;
    int a = matrica->gauti_auksti(), p = matrica->gauti_ploti();

    for(int i = 0; i < a; i++) {
        for(int j = 0; j < p; j++) {
            suma = suma + matrica->gauti_reiksme(j, i);
        }
    }

    return suma / (a * p);
}

void sukurti_matrica_pagal_atrinktus_elementus(Matrica *matrica, double (&nauja_matrica)[100]) {
        double max;
        for(int i = 0; i < matrica->gauti_auksti(); i++) {
                max = matrica->gauti_reiksme(0, i); // didziausias pirmas elementas
                for(int j = 0; j < matrica->gauti_ploti(); j++) {    // prasisuka pro visus eilutes elementus
                        if (matrica->gauti_reiksme(j, i) > max) {
                                max = (matrica->gauti_reiksme(j, i));
                        }
                }
                nauja_matrica[i] = max;                 //formuoja nauja matrica
        }
}

int gauti_eilute(Matrica *matrica, char ka_veikt) {
    if (ka_veikt != 'd' && ka_veikt != 'm')       // ka daryt ar rast maziausia (m) ar didziausia (d)
        return -1;

    int eilute = 0;
    double n = matrica->gauti_reiksme(0, 0);
    for(int i = 0; i < matrica->gauti_auksti(); i++) {
        for(int j = 0; j < matrica->gauti_ploti(); j++) {
            if (ka_veikt == 'd') {
                if (matrica->gauti_reiksme(j, i) > n) {
                    n = matrica->gauti_reiksme(j, i);
                    eilute = i;
                }
            } else if (ka_veikt == 'm') {
                if (matrica->gauti_reiksme(j, i) < n) {
                    n = matrica->gauti_reiksme(j, i);
                    eilute = i;
                }
            }
        }
    }
    return eilute;
}

double dauginti_teigiamus_elementus(Matrica *matrica, int eil) {
        double sandauga = 1;
        for(int i = 0; i < matrica->gauti_ploti(); i++) {
                if (matrica->gauti_reiksme(i, eil) > 0) {
                        sandauga = (double)sandauga * matrica->gauti_reiksme(i, eil);
                }
        }
        return sandauga;
}

int neigiamiausias_stuleplis(Matrica *matrica) {
        double suma[100] = {0}, min;
        int e = 0, id;

        // skaiciuojam stulpeliu neigiamu elementu suma
        for(int i = 0; i < matrica->gauti_auksti(); i++) {
                for(int j = 0; j < matrica->gauti_ploti(); j++) {
                        if (matrica->gauti_reiksme(j, i) < 0) {
                                suma[i] = suma[i] + matrica->gauti_reiksme(j, i);
                                e = 1;
                        }
                }
        }

        if (!e) {
                return -1;
        }
        // randam indeksa
        min = suma[0];
        id = 0;

        for(int i = 0; i < matrica->gauti_auksti(); i++) {
                if (suma[i] < min) {
                        min = suma[i];
                        id = i;
                }
        }
        return id;
}

bool sukeicia_min_max_stulpelius(Matrica *matrica) {
        int e[100] = {0}, c = 0, mini, maxi;
        double max, min, tmp;
        //skaiciuojam kiek teigiamu elementu turi kiekvienas stulpelis
        for(int i = 0; i < matrica->gauti_ploti(); i++) {
                for(int j = 0; j < matrica->gauti_auksti(); j++) {
                        if (matrica->gauti_reiksme(i, j) > 0) {
                                e[i]++;
                        }
                }
        }

        // randam stulpeli, kuriame daugiausiai teigiamu elementu
        max = e[0];
        maxi = 0;
        for(int i = 0; i < matrica->gauti_ploti(); i++) {
                if (e[i] > max) {
                        max = e[i];
                        maxi = i;
                }
        }

        if (max == 0) return false; // nera ne vieno teigiamo elemento


        min = e[0];
        mini = 0;
        for(int i = 0; i < matrica->gauti_ploti(); i++) {
                if (e[i] < min) {
                        min = e[i];
                        mini = i;
                }
        }
                // apkeiciam stulpelius vietomis
        for(int i = 0; i < matrica->gauti_auksti(); i++) {
                tmp = matrica->gauti_reiksme(maxi, i);
                matrica->priskirti_reiksme(maxi, i, matrica->gauti_reiksme(mini, i));
                matrica->priskirti_reiksme(mini, i, tmp);
        }

        return true;
}

void rikiuoti_matrica(Matrica *matrica, int st) {
        int mini = 0;
        double min = matrica->gauti_reiksme(0, 0), tmp;
        for(int i = 0; i < matrica->gauti_auksti(); i++) {
                for(int j = 0; j < matrica->gauti_auksti(); j++) {
                        if (matrica->gauti_reiksme(st, j) > matrica->gauti_reiksme(st, i))  {
                                for(int k = 0; k < matrica->gauti_ploti(); k++) {
                                        tmp = matrica->gauti_reiksme(k, i);
                                        matrica->priskirti_reiksme(k, i, matrica->gauti_reiksme(k, j));
                                        matrica->priskirti_reiksme(k, j, tmp);
                                }
                        }
                }
        }
}

void keisti_neigiamus_elementus_kvadratais(Matrica *matrica, int nuo_eilutes){
        for(int i = nuo_eilutes; i < matrica->gauti_auksti(); i++){
                for(int j = 0; j < matrica->gauti_ploti(); j++){
                        if(matrica->gauti_reiksme(j, i) < 0){
                                matrica->priskirti_reiksme(j, i, matrica->gauti_reiksme(j, i) * matrica->gauti_reiksme(j, i));
                        }
                }
        }
}

#endif // MATRICOSVALDIKLIAI_H_INCLUDED
