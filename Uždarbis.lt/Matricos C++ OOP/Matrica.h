#ifndef MATRICA_H_INCLUDED
#define MATRICA_H_INCLUDED
#include <ostream> // Išvestis
#include <istream> // Įvestis
using namespace std;

class Matrica{

    protected:
        int aukstis; // Matricos plotis
        int plotis; // Matricos aukštis
        double *masyvas; // Matricos masyvas


    public:
        Matrica(int aukstis_par, int plotis_par){ /// Konstruktorius su parametrais
            aukstis = aukstis_par;
            plotis = plotis_par;
            masyvas = new double[aukstis_par * plotis_par](); // Atsidarom masyvui atmintį
        }

        double gauti_reiksme(int x, int y){ /// Gaunam reikšmę iš masyvo
            return masyvas[x + y * plotis];
        }

        int gauti_auksti(){
            return aukstis;
        }

        int gauti_ploti(){
            return plotis;
        }

        void priskirti_reiksme(int x, int y, double v){ /// Priskiriam reikšmę masyvo elementui
            masyvas[x + y * plotis] = v;
        }

        ~Matrica(){ // Destruktorius
            delete[] masyvas; // Ištrinam masyvą iš atminties
        }

        void spausdinti(ostream &duomenys) { /// Spausdinam matricą
            duomenys.precision(3); // 3 skaičių po kableliu tikslumu
            for(int i = 0; i < aukstis; i++) {
                for(int j = 0; j < plotis; j++) {
                        duomenys.width(5); // duomenų plotis
                        duomenys.fill(' '); // duomenų pradinis užpildymas
                        duomenys << gauti_reiksme(j, i) << " "; // įvedama reikšmę į streamą
                }
                duomenys << "\n"; // pereina i nauja eilute
            }
            duomenys << "\n";
        }

        int formuoti(istream &duomenys){ /// Formuojam matricą iš įvesties
            duomenys.precision(3); // tikslumas - 3 sk. po kablelio
            for(int i = 0; i < aukstis; i++){
                for(int j = 0; j < plotis; j++){
                    double tmp; duomenys >> tmp; priskirti_reiksme(j, i, tmp); // Įdedam į masyvą
                    if (duomenys.fail()) return -1; // jei klaida - iseina is f-cijos su -1 o ne 1
                }
            }
            return 1; // Viskas OK
        }

};


#endif // MATRICA_H_INCLUDED
