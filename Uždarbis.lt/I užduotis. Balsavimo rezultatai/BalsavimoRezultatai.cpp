#include <iostream>
#include <fstream>
using namespace std;

#define MAXK 11

void Skaitymas(int mA[], int mB[], int mC[], int & kiekbalsu, int & direktorius1, int & direktorius2, int & direktorius3); // Failo nuskaitymas
void Skyriaus_taskai(int mA[], int mB[], int mC[], int skyrius, int &taskaia, int &taskaib, int &taskaic); // Vieno skyriaus taškų radimas ir pridėjimas prie visų taškų
int Geriausias_logotipas(int a, int b, int c); // Randa geriausią logotipą

int main()
{
    int kiekbalsu, didziausias;
    int direktoriusA, direktoriusB, direktoriusC;
    int taskaiA = 0, taskaiB = 0, taskaiC = 0; // Šituos iškart naudosim kaip visus taškus

    int sudetiaL = 0, sudetibL = 0, sudeticL = 0; // Logotipų taškų sumos
    int BalsaiA[MAXK], BalsaiB[MAXK], BalsaiC[MAXK]; // Logotipų taškai

    Skaitymas(BalsaiA, BalsaiB, BalsaiC, kiekbalsu, direktoriusA, direktoriusB, direktoriusC); // Failo nuskaitymas

    for(int i = 0; i < kiekbalsu; i++){ // Skaičiuojam visų balų sumą
        sudetiaL += BalsaiA[i];
        sudetibL += BalsaiB[i];
        sudeticL += BalsaiC[i];
        Skyriaus_taskai(BalsaiA, BalsaiB, BalsaiC, i, taskaiA, taskaiB, taskaiC);
    }

    cout << sudetiaL << " " << sudetibL << " " << sudeticL << endl;
    cout << taskaiA << " " << taskaiB << " " << taskaiC << endl;

    if(taskaiA == taskaiB || taskaiB == taskaiC){ // Jei yra vienodų taškų
        taskaiA += direktoriusA;
        taskaiB += direktoriusB;
        taskaiC += direktoriusC;
    }

    cout << Geriausias_logotipas(taskaiA, taskaiB, taskaiC) << endl;

    return 0;
}

/// -------- Geriausias logotipas -------------

int Geriausias_logotipas(int a, int b, int c){
    if(c > a && c > b) return 3;
    if(b > a && b > c) return 2;
    return 1;
}


/// -------- Vieno skyriaus taškų radimo procedūra -----------

void Skyriaus_taskai(int mA[], int mB[], int mC[], int skyrius, int &taskaia, int &taskaib, int &taskaic){

    /// Tikrinam po 6 taškus
    // 1 0 0
    if(mA[skyrius] > mB[skyrius] && mA[skyrius] > mC[skyrius]){ taskaia += 6; return; }
    // 0 1 0
    if(mB[skyrius] > mA[skyrius] && mB[skyrius] > mC[skyrius]){ taskaib += 6; return; }
    // 0 0 1
    if(mC[skyrius] > mA[skyrius] && mC[skyrius] > mB[skyrius]){ taskaic += 6; return; }

    /// Tikrinam po du taškus
    // 1 1 0
    if(mA[skyrius] == mB[skyrius] && mB[skyrius] < mC[skyrius]){ taskaia += 2; taskaib += 2; return; }
    // 1 0 1
    if(mA[skyrius] == mC[skyrius] && mC[skyrius] < mB[skyrius]){ taskaia += 2; taskaic += 2; return; }
    // 0 1 1
    if(mB[skyrius] == mC[skyrius] && mC[skyrius] < mA[skyrius]){ taskaib += 2; taskaic += 2; return; }

}

/// ------------- Vieno logotipo taškų suma --------------

int Logotipo_Suma(int m[], int kiekbalsu)
{
    int result = 0;
    for (int i = 0; i < kiekbalsu; i++)
        result += m[i];
    return result;
}

/// -------------- Failo nuskaitymas ----------------

void Skaitymas(int mA[], int mB[], int mC[], int & kiekbalsu, int & direktorius1, int & direktorius2, int & direktorius3)
{
    ifstream fs("U1.txt");
    fs >> kiekbalsu;
    for (int i = 0; i < kiekbalsu; i++)
        fs >> mA[i] >> mB[i] >> mC[i];

    fs >> direktorius1;
    fs >> direktorius2;
    fs >> direktorius3;
    fs.close();
}
