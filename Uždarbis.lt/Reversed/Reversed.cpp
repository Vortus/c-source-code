#include <stdio.h>
#include <stdlib.h>
#include <iostream>

int reverseNumber(int n){ /// Funkcija apver�ianti skai�i�
    int result = 0;
    while(n > 0){
        result = result * 10 + n % 10;
        n /= 10;
    }
    return result;
}

int main()
{
    int m, i, m1, m2, n, num, rem, rev = 0, rev1 = 0;
    printf("Iveskite intervala m n: ");
    scanf ("%d%d", &m, &n);

    if(m > n) swap(m, n); // Jei intervalas gaunasi atvirk�ias

    for(i = m; i <= n; i++)
        if(i == reverseNumber(i)) printf("%d\n", i);

return (0);
}
