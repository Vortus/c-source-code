#include <fstream>
using namespace std;

    #define FILE "duomenys.txt" // Duomen� failas
    #define FILEOUT "rezultatai.txt" // I�vesties failas
    const double C = 10; // Konstanta

///----------- Funkcij� prototipai ---------------

    void makeNewSequence(double *sequenceA, double *sequenceB, double *&newSequence, int asize, int bsize, int &nsize); // Naujos sekos sudarymas
    void readInput(double *&sequenceA, double *&sequenceB, int &asize, int &bsize); // Failo nuskaitymas
    void fractateSequence(double *newSequence, int nsize); // Dalinam masyv� i� sumos
    void displayResults(double *newSequence, int nsize);
    double getSum(double *newSequence, int nsize); // Sumos radimas
    void releaseMemory(double *marray); // Atmities i�laisvinimas


///---------- Pagrindin� funkcija -------------

 int main(){

    int aSize, bSize, newSize = 0; // Masyv� dyd�iai
    double *sequenceA, *sequenceB, *newSequence; // Sekos

    readInput(sequenceA, sequenceB, aSize, bSize); // Sek� nuskaitymas
    makeNewSequence(sequenceA, sequenceB, newSequence, aSize, bSize, newSize); // Susidarom nauj� sek�
    fractateSequence(newSequence, newSize); // Padalinam vis� masyv�
    displayResults(newSequence, newSize);

    releaseMemory(sequenceA);
    releaseMemory(sequenceB);
    releaseMemory(newSequence);

 return 0; }

  ///--------------- Rezultat� spausdinimas  ------------------

   void displayResults(double *newSequence, int nsize){
        int i; // Kintamieji
        ofstream FO(FILEOUT);
        for(i = 0; i < nsize; i++)
            FO << newSequence[i] << endl;
        FO.close();
   }

  ///----------------- Padainam i� sumos ------------------

   void fractateSequence(double *newSequence, int nsize){
        int i; double sum = getSum(newSequence, nsize); // Kintamieji
        for(i = 0; i < nsize; i++)
            newSequence[i] /= sum;
   }

 ///----------------- Sumos radimas ------------------

    double getSum(double *newSequence, int nsize){
        int i; double result = 0; // Kintamieji
        for(i = 0; i < nsize; i++)
            result += newSequence[i];
        return result;
    }

 ///--------------- Naujos sekos sudarymas ------------

    void makeNewSequence(double *sequenceA, double *sequenceB, double *&newSequence, int asize, int bsize, int &nsize){
        int i, tmp = 0; // Kintamieji
        // Susirandam masvo dyd�
        for(i = 0; i < asize; i++)
            if(sequenceA[i] < C) nsize++;

        for(i = 0; i < bsize; i++)
            if(sequenceB[i] < C) nsize++;

        newSequence = new double[nsize]; // Atidarom atmint�

        // Sudedam � masyv� reik�mes
        for(i = 0; i < asize; i++)
            if(sequenceA[i] < C) newSequence[tmp++] = sequenceA[i];

        for(i = 0; i < bsize; i++)
            if(sequenceB[i] < C) newSequence[tmp++] = sequenceB[i];
    }

 ///--------------- Atminties i�laisvinimas ------------

    void releaseMemory(double *marray){
        delete[] marray; // I�laisvinam atmint�
    }

 ///--------------- Duomen� nuskaitymas ---------------

    void readInput(double *&sequenceA, double *&sequenceB, int &asize, int &bsize){
        int i; // Kintamieji
        ifstream FI(FILE); // Failo atidarymas

        /// --- Pirmas masyvas
        FI >> asize; // Kiek pirmame masyve element�
        sequenceA = new double[asize]; // Atidarom atminties

        for(i = 0; i < asize; i++) // Nuskaitom masyv�
            FI >> sequenceA[i];

        /// --- Antras masyvas
        FI >> bsize; // Kiek pirmame masyve element�
        sequenceB = new double[bsize]; // Atidarom atminties

        for(i = 0; i < bsize; i++) // Nuskaitom masyv�
            FI >> sequenceB[i];

        FI.close(); // Failo u�darymas
    }

