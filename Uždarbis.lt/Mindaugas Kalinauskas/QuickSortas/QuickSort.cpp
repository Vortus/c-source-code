#include <iostream>
#include <fstream>

using namespace std;

void quickSort(int *arr, int left, int right, bool didejanc);
void displayarr(ostream &failas, int arr[], int masdydis);

 int main(){

    int arr[1000], masdydis;

    cout << "aibes dydis: "; cin >> masdydis;
    cout << "aibes elementai: ";

    for(int i = 0; i < masdydis; i++)
        cin >> arr[i];

    ofstream failas("aibe.txt");
    quickSort(arr, 0, masdydis - 1, true);
    displayarr(failas, arr, masdydis);

    quickSort(arr, 0, masdydis - 1, false);
    displayarr(failas, arr, masdydis);

    failas.close();

 return 0; }

void displayarr(ostream &failas, int *arr, int masdydis){
    int i; // Kintamieji
    for(i = 0; i < masdydis; i++)
        failas << arr[i] << endl;
    failas << endl;
}

void quickSort(int *arr, int left, int right, bool didejanc) {

      int i = left, j = right; // Kintamieji
      int tmp;
      int mid = arr[(left + right) / 2];

      while (i < j) {
            while (didejanc ? (arr[i] < mid) : (arr[i] > mid)) i++;
            while (didejanc ? (arr[j] > mid) : (arr[j] < mid)) j--;

            if (i <= j) {
                swap(arr[i], arr[j]);
                j--;
                i++;
            }
      }

      if (left < j) quickSort(arr, left, j, didejanc);
      if (i < right) quickSort(arr, i, right, didejanc);

}
