using namespace std;

class DekasH{

    private:
        int *dekoel;
        int virselements = 0;

    public:
        DekasH(istream &FI){
            int i;
            cout << "el kiekis: ";
            FI >> virselements;
            dekoel = new int[virselements];

            cout << "elementai: ";
            for(i = 0; i < virselements; i++)
                FI >> dekoel[i];
        }

        bool isEmpty(){
            return virselements == 0;
        }

        void push_front(int number){
            int *tmp = new int[virselements];
            copy(dekoel, dekoel + virselements, tmp);

            delete[] dekoel;

            dekoel = new int[virselements + 1];
            copy(tmp, tmp + virselements, dekoel);
            dekoel[virselements++] = number;
        }

         void push_back(int number){
            int *tmp = new int[virselements];
            copy(dekoel, dekoel + virselements, tmp);

            delete[] dekoel;

            dekoel = new int[virselements + 1];
            copy(tmp, tmp + virselements, dekoel + 1);
            dekoel[0] = number;

            virselements++;
        }

        void pop_front(){
            if(isEmpty()) return;
            int *tmp = new int[virselements];
            copy(dekoel, dekoel + virselements - 1, tmp);

            delete[] dekoel;
            virselements--;

            dekoel = new int[virselements];
            copy(tmp, tmp + virselements, dekoel);
        }

         void pop_back(){
            if(isEmpty()) return;
            int *tmp = new int[virselements - 1];
            copy(dekoel + 1, dekoel + virselements, tmp);

            delete[] dekoel;

            dekoel = new int[--virselements];
            copy(tmp, tmp + virselements, dekoel);
        }

        void display(ostream &FO){
            int i;
            FO << "Didis: " << virselements << endl;
            for(i = 0; i < virselements; i++)
                FO << dekoel[i] << endl;
            FO << endl;
        }


        int front(){
            if(isEmpty()) return -1;
            return dekoel[virselements - 1];
        }

        int back(){
            if(isEmpty()) return -1;
            return dekoel[0];
        }

        ~DekasH(){
            delete[] dekoel;
        }

};
