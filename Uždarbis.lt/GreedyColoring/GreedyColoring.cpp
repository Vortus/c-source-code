ir#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    #define FILE "duomenys.txt"
    #define MAXTABLE 1000

    struct Node {
        int color;
    };

    int table[MAXTABLE][MAXTABLE]; // Lentelë
    Node nodes[MAXTABLE]; // Nodai
    int WIDTH, HEIGHT;

    void readFile(istream &FI); // Nuskaitom duomenis
    void displayTable(ostream &FO); // Iðvedam duomenis
    void GCA(); // Explorinam Su Greedy Coloring Algorithm
    bool isSafe(int color, int startNode); // Ar galima spalva Node

 int main(){

    ifstream FI(FILE);
    readFile(cin);
    FI.close();

    displayTable(cout);

    GCA();

    for(int i = 1; i <= HEIGHT; i++)
        cout << i << " - " << nodes[i].color << endl;

 return 0; }

    bool isSafe(int color, int startNode){
        for(int i = 0; i < WIDTH; i++){ // Einam per jungtis
            if(table[startNode][i] == 0) continue; // Praleidžiam nulius
            if(nodes[table[startNode][i]].color == color) return false; // Jei rado ta pačią spalvą, nesaugu
        }
        return true; // Spalva saugi
    }

    void GCA(){
        for(int i = 0; i < HEIGHT; i++){ // Pradedam nuo pirmos iki pat galo, alfabetiškai
            int color = 1; // Pradedam nuo 1
            while(!isSafe(color, i)) color++; // Kol ras tinkamą spalvą
            nodes[i + 1].color = color; // Priskiriam spalvą
        }
    }

    void readFile(istream &FI){
        FI >> HEIGHT >> WIDTH; // Nuskaitom duomenis lentelës
        for(int y = 0; y < HEIGHT; y++){
            for(int x = 0; x < WIDTH; x++)
                FI >> table[y][x];
        }
    }

    void displayTable(ostream &FO){
        for(int y = 0; y < HEIGHT; y++){
            for(int x = 0; x < WIDTH; x++)
                FO << table[y][x] << " ";
            FO << endl;
        }
        FO << endl;
    }

