#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    #define FILE "tekstas.txt"
    #define FILEOUT "tekstasOUT.txt"
    string editedTextCapitalized; // Pertvarkytas tekstas

/// --------- Prototipai ------------

    void displayText(ostream &FO);
    void readText(istream &FI);

 int main(){
string str;
char dot = '.' ;
int n;

    ifstream FI(FILE);
    //readText(FI);


    while (FI)
{
getline(FI, str, dot);
n=str.find_first_of("abcdefghijklmnopqrstuvwxyz");
str[n]=toupper(str[n]);
cout<<str<<dot;
}

    FI.close();

    //displayText(cout);


 return 0; }

/// ----------- Nuskaitymas iš failo --------------

    void readText(istream &FI){
        char c;
        bool capitalize = true; // Ar uždėti didžiają raidę
        while(FI.get(c)){
            if(capitalize && isalpha(c)){ // Uždedam didžiają raidę
                c = toupper(c);
                capitalize = false;
            }
            editedTextCapitalized += c; // Pridedam charą į sutvarkytą textą
            if(c == '.' || c == '?' || c == '!') capitalize = true; // Reikia didžiosios raidės
        }
    }

/// ----------- Išvedimas į išvestį --------------

    void displayText(ostream &FO, string text){
        FO << text << endl;
    }
