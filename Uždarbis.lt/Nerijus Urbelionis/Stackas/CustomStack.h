#ifndef CUSTOMSTACK_H_INCLUDED
#define CUSTOMSTACK_H_INCLUDED

#include <string.h> /// Memcpy
#define MAXS 1000000 /// Bus papraðèiau nurodyti maximalø stacko dydá, negu dirbti su dinamine atmintimi
using namespace std;

class CustomStack{

    private:
        int *stackElements; // Skaièiø masyvas
        int currentTopElement = 0;

    public:
        CustomStack(istream &FI){ /// Konstruktorius
            int i; // Kintamieji
            FI >> currentTopElement;
            stackElements = new int[currentTopElement];

            for(i = 0; i < currentTopElement; i++) // Nuskaitom
                FI >> stackElements[i];
        }

        bool isEmpty(){ /// Ar tuðèias
            return currentTopElement == 0;
        }

        void push(int number){ /// Prideda elementà á virðø
            if(isEmpty()) return;
            int i = 0; // Kintamieji

            int *tmp = new int[currentTopElement];
            copy(stackElements, stackElements + currentTopElement, tmp);

            delete[] stackElements;

            stackElements = new int[currentTopElement];
            copy(tmp, tmp + currentTopElement, stackElements);
            stackElements[currentTopElement++] = number;
        }

        void pop(){ /// Nuima virðutiná elementà
            if(isEmpty()) return;
            int i = 0; // Kintamieji

            int *tmp = new int[currentTopElement];
            copy(stackElements, stackElements + currentTopElement, tmp);

            delete[] stackElements;
            currentTopElement--;

            stackElements = new int[currentTopElement];
            copy(tmp, tmp + currentTopElement, stackElements);
        }

        void display(ostream &FO){ /// Iðspausdinam stackà
            int i; // Kintamieji
            FO << "SIZE: " << currentTopElement << endl;
            for(i = 0; i < currentTopElement; i++)
                FO << stackElements[i] << endl;
            FO << endl;
        }

        int peek(){ /// Pasiþiûrim kas yra virðuje
            if(isEmpty()) return -1;
            return stackElements[currentTopElement - 1];
        }

        ~CustomStack(){ /// Dekonstruktori
            delete[] stackElements;
        }

};


#endif // CUSTOMSTACK_H_INCLUDED
