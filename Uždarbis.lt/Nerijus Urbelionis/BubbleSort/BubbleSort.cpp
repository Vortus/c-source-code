#include <fstream>

using namespace std;

#define MAXN 1000
#define FILE "duomenys.txt"
#define FILEOUT "isvestis.txt"

void bubbleSort(int array[], int length, bool ascending);
void displayArray(ostream &FO, int array[], int length);

 int main(){

    int i; // Kintamieji
    int array[MAXN], length; // Masyvas, jo dydis
    ifstream FI(FILE);

    FI >> length;
    for(i = 0; i < length; i++) // Nuskaitom masyvà
        FI >> array[i];

    FI.close();

    ofstream FO(FILEOUT);
    bubbleSort(array, length, true);
    displayArray(FO, array, length);

    bubbleSort(array, length, false);
    displayArray(FO, array, length);

    FO.close();

 return 0; }

/// ------------ Iðvedam masyvà --------------

void displayArray(ostream &FO, int array[], int length){
    int i; // Kintamieji
    for(i = 0; i < length; i++)
        FO << array[i] << endl;
    FO << endl;
}

/// ------------ Bubble Sortas --------------

void bubbleSort(int array[], int length, bool ascending) { /// Bubble sortas
    int i, tmp; // Kintamieji
    bool flag = true; // Flagas
    while (flag) {
        flag = false; // Laukiam tinkamo swapo
        for(i = 0; i < length - 1; i++) {
            if (ascending ? array[i] > array[i + 1] : array[i] < array[i + 1]) {
                tmp = array[i]; // Vyksta swapas
                array[i] = array[i + 1];
                array[i + 1] = tmp;
                flag = true; // Pasiþymim jog swapinom
            }
        }
    }
}
