#ifndef CUSTOMDEQUE_H_INCLUDED
#define CUSTOMDEQUE_H_INCLUDED

#include <string.h> /// Memcpy
#define MAXS 1000000 /// Maksimalus decko dydis
using namespace std;

class CustomDeque{

    private:
        int *dequeElements; // Skaičių masyvas
        int currentTopElement = 0;

    public:
        CustomDeque(istream &FI){ /// Konstruktorius
            int i; // Kintamieji
            cout << "Elementu kiekis: ";
            FI >> currentTopElement;
            dequeElements = new int[currentTopElement];

            cout << "Iveskite elementus: ";
            for(i = 0; i < currentTopElement; i++) // Nuskaitom
                FI >> dequeElements[i];
        }

        bool isEmpty(){ /// Ar tuščias dekas
            return currentTopElement == 0;
        }

    /// ---------------- PUSH_FRONT ---------------------

        void push_front(int number){ /// Pridedam į pradžią
            int i = 0; // Kintamieji

            int *tmp = new int[currentTopElement]; // Laikinas masyvas
            copy(dequeElements, dequeElements + currentTopElement, tmp); // Perkopijuojam elementus

            delete[] dequeElements; // Istrinam elementus

            dequeElements = new int[currentTopElement + 1]; // Susikuriam +1 dydzio masyva
            copy(tmp, tmp + currentTopElement, dequeElements); // Kopijuojam elementus
            dequeElements[currentTopElement++] = number; // I prieki imetam skaiciu
        }

    /// ---------------- PUSH_BACK ---------------------

         void push_back(int number){ /// Pridedam į pabaigą
            int i = 0; // Kintamieji

            int *tmp = new int[currentTopElement];
            copy(dequeElements, dequeElements + currentTopElement, tmp);

            delete[] dequeElements;

            dequeElements = new int[currentTopElement + 1];
            copy(tmp, tmp + currentTopElement, dequeElements + 1); // Kopijuojam nuo antro
            dequeElements[0] = number; // I prieki imetam skaicius
            currentTopElement++;
        }

    /// ---------------- POP_FRONT ---------------------

        void pop_front(){ /// Nuima Viršutinį elementą
            if(isEmpty()) return;
            int i = 0; // Kintamieji

            int *tmp = new int[currentTopElement];
            copy(dequeElements, dequeElements + currentTopElement - 1, tmp); // Nusiskaitom visus tik galinio neemam

            delete[] dequeElements;
            currentTopElement--;

            dequeElements = new int[currentTopElement];
            copy(tmp, tmp + currentTopElement, dequeElements);
        }

    /// ---------------- POP_BACK ---------------------??

         void pop_back(){ /// Apatinio elemento nuimimas
            if(isEmpty()) return;
            int i = 0; // Kintamieji

            int *tmp = new int[currentTopElement - 1];
            copy(dequeElements + 1, dequeElements + currentTopElement, tmp); // Nusiskaitom visus tik priekinio neemam

            delete[] dequeElements;

            dequeElements = new int[--currentTopElement];
            copy(tmp, tmp + currentTopElement, dequeElements);
        }

    /// ---------------- DISPLAYUS ---------------------

        void display(ostream &FO){ /// Išspausdinam decką
            int i; // Kintamieji
            FO << "SIZE: " << currentTopElement << endl;
            for(i = 0; i < currentTopElement; i++)
                FO << dequeElements[i] << endl;
            FO << endl;
        }

    /// ---------------- VIRŠ ELEMENTAS ---------------------

        int front(){ /// Viršutinis elementas
            if(isEmpty()) return -1;
            return dequeElements[currentTopElement - 1];
        }

    /// ------------- APATINIS ELEMENTAS  -------------------

        int back(){ /// Apatinis elementas
            if(isEmpty()) return -1;
            return dequeElements[0];
        }

    /// ------------- DESTRUKTORIUS  -------------------

        ~CustomDeque(){ /// Dekonstruktori
            delete[] dequeElements;
        }

};


#endif // CUSTOMDEQUE_H_INCLUDED
