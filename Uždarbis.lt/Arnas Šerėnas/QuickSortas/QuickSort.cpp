#include <iostream>
#include <fstream>

using namespace std;

#define MAXN 1000
#define FILE "duomenys.txt"
#define FILEOUT "duomenysRez.txt"

void quickSort(int array[], int left, int right, bool ascending);
void displayArray(ostream &FO, int array[], int length);

 int main(){

    int i; // Kintamieji
    int array[MAXN], length; // Masyvas, jo dydis
    ifstream FI(FILE);

    FI >> length;
    for(i = 0; i < length; i++) // Nuskaitom masyvą
        FI >> array[i];

    FI.close();

    ofstream FO(FILEOUT);
    quickSort(array, 0, length - 1, true);
    displayArray(FO, array, length);

    quickSort(array, 0, length - 1, false);
    displayArray(FO, array, length);

    FO.close();

 return 0; }

/// ------------ Išvedam masyvą --------------

void displayArray(ostream &FO, int array[], int length){
    int i; // Kintamieji
    for(i = 0; i < length; i++)
        FO << array[i] << endl;
    FO << endl;
}

/// ------------ Quick Sortas --------------

void quickSort(int *array, int left, int right, bool ascending) {

      int i = left, j = right; // Kintamieji
      int tmp;
      int arrow = array[(left + right) / 2];

      while (i < j) {
            while (ascending ? (array[i] < arrow) : (array[i] > arrow)) i++;
            while (ascending ? (array[j] > arrow) : (array[j] < arrow)) j--;
            if (i <= j) swap(array[i++], array[j--]);
      }

      if (left < j) quickSort(array, left, j, ascending);
      if (i < right) quickSort(array, i, right, ascending);

}
