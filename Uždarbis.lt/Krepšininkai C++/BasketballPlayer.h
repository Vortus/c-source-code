#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED

#include "BasePlayer.h" // Įtraukiam bazinę klasę
#include <iostream> // Įvedimas ranka
#include <fstream> // Įvedimas rinkmena
using namespace std;

class BasketballPlayer : public BasePlayer { /// Paveldim savybes

    /*
        Čia nėra konstruktoriaus kadangi paveldim jį iš bazinės klasės.
    */

    public:

        void loadData(){ /// Nusakom įvedimą klaviatūra
            cout << "1 taskio mesta/pataikyta: "; cin >> thrown[0] >> hit[0]; // Galim pasiekti thrown ir hit kadangi jie yra protected
            cout << "2 taskio mesta/pataikyta: "; cin >> thrown[1] >> hit[1];
            cout << "3 taskio mesta/pataikyta: "; cin >> thrown[2] >> hit[2];
        }

        void loadDataFile(ifstream &FI){ /// Nusakom įvedimą rinkmena
            int i; // Kintamieji
            for(i = 0; i < 3; i++) // Mesti
                FI >> thrown[i];
            for(i = 0; i < 3; i++) // Pataikyti
                FI >> hit[i];
        }

        ~BasketballPlayer(){ /// Nusakom Dekonstruktorių
            delete[] thrown;
            delete[] hit;
        }

        BasketballPlayer operator+(BasketballPlayer &obj) { /// Nusakom perkrovimą sudėties
            BasketballPlayer result = BasketballPlayer();
            int i;

            for(i = 0; i < 3; i++) // Sudedam metimus
                result.thrown[i] = thrown[i] + obj.thrown[i];

            for(i = 0; i < 3; i++) // Sudedam pataikymus
                result.hit[i] = hit[i] + obj.hit[i];

            return result; // Gražinam naują žaidėją, su naujais taškais
        }

};

#endif
