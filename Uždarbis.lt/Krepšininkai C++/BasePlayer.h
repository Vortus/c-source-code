#ifndef BASEPLAYER_H_INCLUDED
#define BASEPLAYER_H_INCLUDED

class BasePlayer{ /// Bazinė klasė

    protected:

        int* thrown; // Metimai kuriuos paveldėsim
        int* hit; // Pataikyti kuriuos paveldėsim

    public:

        void loadData(){} /// Užkraunam duomenis klaviatūra
        void loadDataFile(){} /// Užkraunam duomenis rinkmena

        int getThrown(int i){ /// Gaunam išmestus
            if(!(i >= 0 && i < 3)) return -1;
            return thrown[i];
        }

        int getHit(int i){ /// Gaunam pataikytus
            if(!(i >= 0 && i < 3)) return -1;
            return hit[i];
        }

        ~BasePlayer(){}; /// Destruktorius
        BasePlayer operator+(BasePlayer obj);/// Perkraunam sudėtį

        BasePlayer() { /// Konstruktorius, be argumentų
            thrown = new int[3](); // Atsidarom atmintį
            hit = new int[3](); // Atsidarom atmintį
        };

        BasePlayer(int* thrown, int* hit){ /// Konstruktorius su argumentais
            this->thrown = thrown;
            this->hit = hit;
        }

};

#endif
