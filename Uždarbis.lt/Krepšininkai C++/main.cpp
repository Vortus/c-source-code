#include <iostream>
#include <iomanip>
#include "BasketballPlayer.h"
using namespace std;

    #define FILE "duomenys.txt" // Duomenų failas
    #define MAXN 5 // Žaidėjų kiekis

    BasketballPlayer getAllSum(BasketballPlayer *team); // Sudedam visus metimus ir pataikymus
    void readFile(BasketballPlayer *team); // Nuskaitom žaidėjų duomenis iš failo
    void readKeyboard(BasketballPlayer *team); // Nuskaitom klaviatūra
    void displayResults(BasketballPlayer team); // Procentalus pataikymas

 int main(){

    BasketballPlayer teamPlayers[MAXN]; // Įnicializuojama žaidėjų komanda

    readFile(teamPlayers); // Nuskaitom iš failo
    //readKeyboard(teamPlayers); // Nuskaitom iš klaviatūros

    BasketballPlayer team = getAllSum(teamPlayers); // Susumuojam visus metimus ir pataikymus
    displayResults(team);

 return 0; }

/// --------------- Procentalūs pataikymai  ------------------

    void displayResults(BasketballPlayer team){
        int i;
        for(i = 0; i < 3; i++)
            cout << i + 1 << " taskio mesta: " << team.getThrown(i) << ", pataikyta: " << team.getHit(i) <<
            ", procentalus: " << fixed << setprecision(2) << (double)team.getHit(i) / team.getThrown(i) << "%" << endl;
    }

/// ----------- Susumuojam metimus ir pataikymus -------------

    BasketballPlayer getAllSum(BasketballPlayer* team){
        int i; // Kintamieji
        BasketballPlayer result = BasketballPlayer();
        for(i = 0; i < MAXN; i++)
            result = result + *(team + i);
        return result;
    }

/// --------------- Nuskaitymas iš failo ---------------------

    void readFile(BasketballPlayer* team){
        int i; // Kintamieji
        ifstream FI(FILE);
        for(i = 0; i < MAXN; i++) // Nuskaitom iš rinkmenos
            (team + i)->loadDataFile(FI);
        FI.close();
    }

/// --------------- Nuskaitymas iš klaviatūros ---------------------

    void readKeyboard(BasketballPlayer* team){
        int i; // Kintamieji
        for(i = 0; i < MAXN; i++){ // Nuskaitom iš rinkmenos
            cout << i + 1 << " zaidejas ---" << endl;
            (team + i)->loadData();
        }
    }


