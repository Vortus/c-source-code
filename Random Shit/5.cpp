#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

	ifstream fi("5in.txt");
    int n, c = 0; fi >> n;
    double arr[1000]; for(int i = 0; i < n; i++) arr[i] = 0;

    while(!fi.eof()){
        double sum = 0;
        for(int i = 0; i < n; i++){
            double j; fi >> j;
            sum += j; cout << j << " ";
        }
        arr[c] = sum;
        c++;
        cout << endl;
    }

    for(int i = 0; i < c; i++){
        for(int j = i + 1; j < c; j++){
            if(arr[i] < arr[j]){
                double temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
    }

    for(int i = 0; i < c; i++)
        cout << arr[i] / n << " ";

	fi.close();

 return 0; }
