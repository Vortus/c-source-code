#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <algorithm>
using namespace std;

    #define FILE "in.txt"
    #define MAXN 1000

    int N = 0; // mokiniu sk

    struct Student{
        string name, surname, clas;
        void init(string a, string b, string c){ name = a; surname = b; clas = c;}
        double average;
        bool operator <(const Student &s) const{
            return average >= s.average;
        }

    } students[MAXN];

    void readFile(){
        ifstream FI(FILE);
        string n, s, c; char cc;
        int k;
        while(!FI.eof()){
            char n[11], s[11], c[6];
            FI.get(n, sizeof n); FI.get(s, sizeof s); FI.get(c, sizeof c);
            string line; getline(FI, line);
            students[N].init(n, s, c);
            //suskirstom skaicus
            int kk = 0;
            istringstream linestream(line);
            while (linestream >> k){
                students[N].average += k;
                kk++;
            }
            students[N++].average /= kk;
        }
        FI.close();
        sort(students, students + N);
    }

    void displayTable(){
        ofstream FO("Rez2.txt");
        FO << "--------------------------------------" << endl;
        FO << left << setw(3) << "Nr. "
        << setw(10) << "Pavarde"
        << setw(10) << "Vardas"
        << setw(6) << "Klase"
        << setw(8) << "Vidurkis" << endl;
        FO << "--------------------------------------" << endl;

        for(int i = 0; i < N; i++){
            FO << right << setw(3) << i + 1 << " "
            << left << setw(10) << students[i].surname
            << setw(10) << students[i].name
            << setw(6) << students[i].clas
            << setw(8) << right << fixed << setprecision(2) << students[i].average << endl;
        }
        FO << "--------------------------------------" << endl;
        FO.close();
    }

 int main(){

	readFile();
    displayTable();

 return 0; }
