#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

/// ---------------- Kintamieji -----------------

    int i, j;
    int tmp, result, n;

    string option;
    double buffer[100], di;
    int intBuffer[100] = { 0 };

/// ---------------- Mainas -----------------

 int main(){

/// ---------------- Pirmas-----------------

    while(true){

        tmp = 0;
        result = 0;

        cout << "Pasirinkite sekos tipa(F/A): "; cin >> option;

        if(option == "F"){

            intBuffer[1] = 1;
            cout << "Pasirinkote Fibonnaci seka" << endl;
            cout << "Sekos nariu skaiciu: "; cin >> n;

            // Fibonaci
            for(i = 2; i < n; i++)
                intBuffer[i] = intBuffer[i - 1] + intBuffer[i - 2];

            // Sudejimas
            for(i = 0; i < n; i++){
                cout << intBuffer[i] << endl;
                result += intBuffer[i];
            }

            cout << "Rezultatas: " << result << endl;

        } else {

            cout << "Pasirinkote Aritmetine seka" << endl;
            cout << "Sekos nariu skaiciu: "; cin >> n;

            cout << "Iveskite a1: "; cin >> intBuffer[0]; // Pirmas
            cout << "Iveskite d: "; cin >> tmp;

            // Aritmetine
            for(i = 1; i < n; i++)
               intBuffer[i] = intBuffer[i - 1] + tmp;

            // Sudejimas
            for(i = 0; i < n; i++){
                cout << intBuffer[i] << endl;
                result += intBuffer[i];
            }

            cout << "Rezultatas: " << result << endl;
        }

        // Klausiam
        while(option != "T" || option != "N"){
            cout << "Ar testi? (T/N): "; cin >> option;
            if(option == "T") break;
            else return 0;
        }

/// ---------------- Antras -----------------

        n = 0;

        // Skaičiuojam
        for(di = -10; di <= 10; di += 0.5){
            buffer[n] = -35 * di + 10 * pow(di, 5);
            n++;
        }

        // Ats
        for(i = 0; i < n; i++)
            cout << setw(10) << buffer[i] << endl;

        while(option != "T" || option != "N"){
            cout << endl << "Ar kartoti viska? (T/N): "; cin >> option;
            if(option == "T") break;
            else return 0;
        }

        system("CLS");
    }

 return 0; }
