#include <iostream>

using namespace std;

 int main(){
    setlocale(LC_ALL, "Lithuanian");

    int a, b, c;
    cout << "Įveskite a, b, c: "; cin >> a >> b >> c;

    int mins = min(min(a, b), c);
    int maxs = max(max(a, b), c);

    cout << mins << " " << maxs << endl;

 return 0; }
