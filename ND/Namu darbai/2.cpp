
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

	fstream fi("2in.txt");
	ofstream fo("2out.txt");

    double a, b, c, d;
    fi >> a >> b >> c >> d;

    double res = (b * 0.01 * a * c) / d;

    fo << fixed << setprecision(1) << res;

	fi.close();
    fo.close();

 return 0; }
