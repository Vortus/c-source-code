
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

	fstream fi("3in.txt");
	ofstream fo("3out.txt");

    double ap, rp, bp, ak, rk, bk, kcal;
    fi >> ap >> rp >> bp >> ak >> rk >> bk >> kcal;

    double anglk = (kcal / (ak * 100)) * ap;
    double riebk = (kcal / (rk * 100)) * rp;
    double baltk = (kcal / (bk * 100)) * bp;

    fo << fixed << setprecision(2) << anglk << endl << riebk << endl << baltk;

	fi.close();
    fo.close();

 return 0; }
