
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

	fstream fi("1in.txt");
	ofstream fo("1out.txt");

    int a, b, c, d;
    fi >> a >> b >> c >> d;

    int min = b + d;
    int val = (a + c + (min / 60)) % 24;
    min %= 60;

    fo << val << " " << min;

	fi.close();
    fo.close();
 return 0; }
