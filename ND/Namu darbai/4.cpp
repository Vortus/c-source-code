#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    int n, maxsum = 0, maxp1, maxp2;
    ifstream fi("in.txt");
    fi >> n;

    for(int i = 0; i < n / 2; i++){
        int p1, p2, sum = 0;
        fi >> p1 >> p2;
        sum = p1 + p2;
        if(sum > maxsum) { maxp1 = p1; maxp2 = p2; maxsum = sum; }
    }

    if(maxp1 > maxp2) cout << "Pirmas laimejo";
    else if(maxp2 > maxp1) cout << "Antras laimejo";
    else cout << "lygiosios";

    fi.close();

 return 0; }
