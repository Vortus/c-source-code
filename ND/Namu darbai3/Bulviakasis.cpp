#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    int times(double quantity, int capacity){
        return ceil(quantity / capacity);
    }

 int main(){

	ifstream fi("Duom1.txt");
    int n, m, allTimes = 0, maxTimes = 0; fi >> n >> m;

    for(int i = 0; i < n; i++){
        int j; fi >> j;
        if(j > maxTimes) maxTimes = j;
        allTimes += times(j, m);
    }
    fi.close();

    ofstream fo("Rez1.txt");
    fo << allTimes << endl;
    fo << maxTimes << " " << times(maxTimes, m);
    fo.close();

 return 0; }
