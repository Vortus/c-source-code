#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    void travelTime(double &firstOutH, double &firstOutM, double firstInH, double firstInM,
                    double secondOutH, double secondOutM, double secondInH, double secondInM)
    {
        int firstMinsOut = firstOutH * 60 + firstOutM;
        int firstMinsIn = firstInH * 60 + firstInM;

        int secondMinsOut = secondOutH * 60 + secondOutM;
        int secondMinsIn = secondInH * 60 + secondInM;

        int firstMins = firstMinsIn - firstMinsOut; if (firstMins < 0) firstMins += 24 * 60;
        int secondMins = secondMinsIn - secondMinsOut; if (secondMins < 0) secondMins += 24 * 60;

        int allMins = firstMins + secondMins;
        firstOutH = allMins / 60; allMins -= firstOutH * 60;
        firstOutM = allMins;

    }

 int main(){

	ifstream fi("1in.txt");
    ofstream fo("1out.txt");
    int n; fi >> n;

    for(int i = 0; i < n; i++){
        double a, b, c, d, e, f, g, h; fi >> a >> b >> c >> d >> e >> f >> g >> h;
        travelTime(a, b, c, d, e, f, g, h);
        fo << a << " " << b << endl;
    }

    fo.close();
	fi.close();

 return 0; }
