#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <math.h>

using namespace std;

    int missingWords(int a, int b){
        return a - b;
    }

 int main(){

    int dayCount, wordsPerday, daysMissing = 0, wordsMissing = 0;
    double moreDays;

    ifstream fi("5in.txt");
    ofstream fo("5out.txt");
    fi >> dayCount >> wordsPerday;

    for(int i = 0; i < dayCount; i++){
        int j; fi >> j;
        if(j != wordsPerday) daysMissing++;
        wordsMissing += missingWords(wordsPerday, j);
    }

    moreDays = ceil((double)wordsMissing / wordsPerday);
    fo << daysMissing << endl << wordsMissing << endl << moreDays;

    fi.close();
    fo.close();

 return 0; }
