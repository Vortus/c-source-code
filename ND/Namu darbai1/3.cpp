#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    ifstream fi("in3.txt");
    ofstream fo("out3.txt");
    int n; double rp = 0, bp = 0, ap = 0, allmass = 0;
    fi >> n;

    for(int i = 0; i < n; i++){
        double a, b, c, d;
        fi >> a >> b >> c >> d;
        rp += a; bp += b; ap += c; allmass += d;
    }

    fo << fixed << setprecision(2)
    << rp / (allmass / 100) << endl
    << bp / (allmass / 100) << endl
    << ap / (allmass / 100) << endl;

    fi.close();
    fo.close();

 return 0; }
