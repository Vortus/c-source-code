#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    double travelBusCost(int n, double ticketP){
        return n * ticketP;
    }

 int main(){

    ifstream fi("in2.txt");
    ofstream fo("out2.txt");

    double n, a, b, c, d, carCost;
    fi >> a >> b >> c >> d >> n;
    carCost = a * 0.01 * b * c;

    for(int i = 0; i < n; i++){
        double price;
        fi >> price;
        fo << fixed << setprecision(2)
            << carCost - travelBusCost(d, price) << endl;
    }

    fi.close();
    fo.close();

 return 0; }
