#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    ifstream fi("in1.txt");
    ofstream fo("out1.txt");
    int allTime = 0, deadlineTime = 0;

    int d, h, m, hh, mm, de, he, me;
    fi >> d >> h >> m >> hh >> mm;
    fi >> de >> he >> me;

    allTime = d * 24 * 60 + h * 60 + m + hh * 60 + mm;
    deadlineTime = de * 24 * 60 + he * 60 + me;

    int temp = allTime;
    d = temp / 60 / 24; if(d < 10) fo << "0";
    fo << d << " "; temp -= d * 60 * 24;

    h = temp / 60; if(h < 10) fo << "0";
    fo << h << " "; temp -= h * 60;

    if(temp < 10) fo << "0"; fo << temp << endl;

    if(deadlineTime - allTime < 0) fo << "Ne";
    else fo << "Taip";

    fi.close();
    fo.close();

return 0; }
