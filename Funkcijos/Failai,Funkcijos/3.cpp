
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int multiply(int a, int b){
        return a * b;
    }

 int main(){

	for(int i = 1; i < 10; i++){
        for(int j = 1; j < 10; j++){
            cout << setw(4) << multiply(i, j);
            if(j % 9 == 0) cout << endl;
        }
	}

 return 0; }
