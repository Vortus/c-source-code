
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    double biggestSmallest(double a, double b){ // a >= b
        return a - b;
    }

    void sortArrDesc(int arr[], int l){
        for(int i = 0; i < l; i++){
            for(int j = i + 1; j < l; j++){
                if(arr[i] < arr[j]){
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }

    double average(int arr[], int l){
        double sum = 0;
        for(int i = 0; i < l; i++)
            sum += arr[i];
        return sum / l;
    }

 int main(){

	ifstream fi("duomenys.txt");
    int count = 0;
    int arr[1000];

    while(!fi.eof()){
        fi >> arr[count];
        count++;
    }

    sortArrDesc(arr, count);
    cout << average(arr, count) << endl;
    cout << biggestSmallest(arr[0], arr[count - 1]);

	fi.close();

 return 0; }
