#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    double atstumas(int a, int b, int c, int d){
        return sqrt((c - a) * (c - a) + (d - a) * (d - a));
    }

 int main(){

	ifstream fi("2in.txt");

    while(!fi.eof()){
        int a, b, c, d; fi >> a >> b >> c >> d;
        cout << atstumas(a, b, c, d) << endl;
    }

	fi.close();

 return 0; }
