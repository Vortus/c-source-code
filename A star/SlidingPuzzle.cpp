#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    int board[1000][1000], resultBoard[1000][1000];
    int startx, starty, boardSize;
    string res;

    void displayBoard(int b[][1000], int size){
        for(int i = 0; i < boardSize; i++){
            for(int j = 0; j < boardSize; j++)
                cout << b[i][j];
            cout << endl;
        }
    }

    int difference(int b[1000][1000]){
        int d = 0;
        for(int i = 0; i < boardSize; i++){
            for(int j = 0; j < boardSize; j++){
                if(b[i][j] != resultBoard[i][j]) d++;
            }
        }
        return d;
    }

    void swaping(int board[1000][1000], int x, int y, int xx, int yy){ // swaps tile values
        swap(board[y][x], board[yy][xx]);
    }

    void cpy(int from[1000][1000], int to[1000][1000], int size){
        for(int i = 0; i < boardSize; i++)
            for(int j = 0; j < boardSize; j++)
                to[i][j] = from[i][j];
    }

    bool explore(int tmp[][1000], int x, int y, int last, int lastdiff){

        int diff = difference(tmp);
        if(diff >= lastdiff) return false;

        if(diff == 0){
            return true;
        }

        if(last != 3 && x != 0){ // kaire
            swaping(tmp, x, y, x - 1, y);
            if(explore(tmp, x - 1, y, 4, diff)){ res = "left " + res; return true; }
            swaping(tmp, x, y, x - 1, y); // backswap
        }
        if(last != 2 && y != boardSize - 1){ // apace
            swaping(tmp, x, y, x, y + 1);
            if(explore(tmp, x, y + 1, 1, diff)){ res = "down " + res; return true; }
            swaping(tmp, x, y, x, y + 1); // backswap
        }
        if(last != 4 && x != boardSize - 1){ // desine
            swaping(tmp, x, y, x + 1, y);
            if(explore(tmp, x + 1, y, 3, diff)){ res = "right " + res; return true; }
            swaping(tmp, x, y, x + 1, y); // backswap
        }

        if(last != 1 && y != 0){ // virsu
            swaping(tmp, x, y, x, y - 1);
            if(explore(tmp, x, y - 1, 2, diff)){ res = "up " + res; return true; }
            swaping(tmp, x, y, x, y - 1); // backswap
        }

        return false;
    }

    void readFile(string s){
        ifstream fi(s);
        fi >> boardSize;

        for(int i = 0; i < boardSize; i++)
            for(int j = 0; j < boardSize; j++){
                fi >> board[i][j];
                if(board[i][j] == 0){ startx = j; starty = i; }
            }

        for(int i = 0; i < boardSize * boardSize; i++)
            resultBoard[i / boardSize][i % boardSize] = i + 1;
        resultBoard[boardSize - 1][boardSize - 1] = 0;

        fi.close();
    }

 int main(){

    readFile("slider.txt");

    explore(board, startx, starty, 0, boardSize * boardSize * boardSize);
    cout << res;

 return 0; }
