#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    string decToBin(int n){
        string res;
        while(n > 0){
            int j = n % 2;
            char c = j + '0';
            res = c + res;
            n /= 2;
        }
        return res;
    }

    int power(int x, int n){
        if(n == 0) return 1;
        if(n <= 1) return x;
        return power(x, n - 1) * x;
    }

    int octToDec(string n){
        int numb = 0;
        for(int i = 0; i < n.length(); i++){
            int j = n[n.length() - i - 1] - '0';
            numb += power(8, i) * j; // n numeral system
        }
        return numb;
    }

    int hexToDec(string s){
        int numb = 0;
        for(int i = 0; i < s.length(); i++){
            int j = s[s.length() - i - 1] - '0';
            if(j > 10) j-= 7;
            numb += power(16, i) * j;
        }
        return numb;
    }

    string decToHex(int n){
        string r; char c;
        while(n > 0){
            int j = n % 16;
            if(j < 10) c = j + '0';
            else c = (char)('A' + (j - 10));
            r = c + r;
            n /= 16;
        }
        return r;
    }

 int main(){

    cout << decToBin(2015);

 return 0; }
