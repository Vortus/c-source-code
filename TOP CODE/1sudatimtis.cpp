#include <iostream>
#include <algorithm>
#include <string.h>
#include <cmath>
using namespace std;

    #define MAXSIZE 101
    char X[MAXSIZE], Y[MAXSIZE], K[MAXSIZE];

    void sumChars(){
        int i;
        for(i = 0; i < MAXSIZE; i++){
            K[i] = (X[i] + Y[i] + K[i]) % 10;
            K[i + 1] = (X[i] + Y[i]) / 10;
        }
    }

    //4321
    //1234

    int switchFix(bool &b){
        int i;
        for(i = MAXSIZE - 1; i >= 0; i--){
            if((int)X[i] == 0 && (int)Y[i] != 0) { swap(X, Y); b = true; return 1; }
            if((int)X[i] < (int)Y[i]){ swap(X, Y); b = true; return 0; }
        }
    }

    void subtractChars(bool &b){
        int i, diff, remainder = 0; switchFix(b);
        for(i = 0; i < MAXSIZE; i++) { // pradiniu klasiu atimtis
            K[i] = (int)X[i] - (int)Y[i] - K[i];
            if(K[i] < 0){ K[i] += 10; K[i + 1]++; }
        }
    }

int main(){

    string tmp; int i;
    cout << "Iveskite X: "; cin >> tmp;
    for(i = tmp.length() - 1; i >= 0; i--) X[tmp.length() - i - 1] = tmp[i] % 48;
    cout << "Iveskite Y: "; cin >> tmp;
    for(i = tmp.length() - 1; i >= 0; i--) Y[tmp.length() - i - 1] = tmp[i] % 48;

    //sumChars();
    bool swaping = false;
    subtractChars(swaping);

    if(swaping) cout << "-";
    bool ignore = true;
    for(i = MAXSIZE - 1; i >= 0; i--){
        if(ignore && (int)K[i] != 0) ignore = false;
        if(!ignore) cout << (int)K[i] << "";
    }
    return 0;
}
