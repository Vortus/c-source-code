#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int fibH(int n, int third, int second, int first){
        if(n <= 1) return first;
        return fibH(n - 1, third + second + first, third, second);
    }

    int fib(int n){
        return fibH(n, 1, 1, 1);
    }

 int main(){

    int n; cin >> n;
    cout << "ATS: " << fib(n);

 return 0; }
