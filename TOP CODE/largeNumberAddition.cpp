#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    char intToChar(int i){
        return i + '0';
    }

    int charToInt(char c){
        return c - '0';
    }

    string addup(string a, string b){
        int diff = a.length() - b.length(), remainder = 0;

        for(int i = a.length() - 1; i >= 0; i--){
            int sum = 0;

            if(b[i - diff] - '0' != -49) sum = charToInt(a[i]) + charToInt(b[i - diff]) + remainder; // jei b netuscia
            else sum = charToInt(a[i]) + remainder;

                if(sum / 10 >= 1) remainder = 1;
                else remainder = 0;
                sum %= 10;
                a[i] = intToChar(sum);
        }

        if(remainder > 0) a = intToChar(remainder) + a;
        return a;
    }

 int main(){

	string a, b; cin >> a >> b;
    if(a.length() < b.length()){ string tmp = a; a = b; b = tmp; }
    cout << addup(a, b); // desimtaine sistema

 return 0; }
