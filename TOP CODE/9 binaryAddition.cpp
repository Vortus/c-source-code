#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    char xorCustom(char a, char b){
        if(a == b) return '0';
        else return '1';
    }

    string addupBinary(string a, string b){

        int diff = a.length() - b.length();
        char remainder = '0';

        for(int i = a.length() - 1; i >= 0; i--){
            if(i - diff > b.length() && remainder == '1') a[i] = xorCustom(a[i], a[i]);
            else a[i] = xorCustom(a[i], b[i - diff]);
            remainder = a[i] == '0' ? '1' : '0';
        }

        if(remainder == '1') a = '1' + a;
        return a;
    }

 int main(){

    string a, b;
    cout << "1 - asis: "; getline(cin, a);
    cout << "2 - asis: "; getline(cin, b);

    if(a.length() < b.length()){ string tmp = a; a = b; b = tmp; }

    cout << addupBinary(a, b);

 return 0; }
