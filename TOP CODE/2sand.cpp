#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
using namespace std;

    /* Lattice multiplication ideja
        Ziauriai faini :P lol. */

    #define FILE "duomenys.txt"
    #define MAXSIZE 100000
    string X, Y, K;
    short diagonals[MAXSIZE];

    void fixString(string &s){
        int i;
        for(i = 0; i < s.length(); i++)
            s[i] %= 48;
    }

    void readFile(){
        ifstream FI(FILE);
        FI >> X >> Y; fixString(X); fixString(Y);
        FI.close();
        fill(diagonals, diagonals + MAXSIZE, 0);
        if(X.length() < Y.length()) swap(X, Y);
    }

 int main(){

    readFile();

    int i, j, k, l, mainOffset = 0, tmpOffset, tmpi, tmpj;
    l = X.length() + Y.length();

    //sudauginam
    for(i = 0; i < Y.length(); i++){
        tmpOffset = mainOffset;
        for(j = 0; j < X.length(); j++){
            k = X[j] * Y[i];
            diagonals[tmpOffset] += k / 10;
            diagonals[tmpOffset + 1] += k % 10;
            tmpOffset++;
        }
        mainOffset++;
    }

    //sutvarkom
    for(i = l - 1; i > 0; i--){
        tmpi = diagonals[i] % 10;
        tmpj = diagonals[i] / 10;
        diagonals[i] = tmpi;
        diagonals[i - 1] += tmpj;
    }

    for(i = diagonals[i] == 0 ? 1 : 0; i < l; i++)
        cout << diagonals[i];

 return 0; }
