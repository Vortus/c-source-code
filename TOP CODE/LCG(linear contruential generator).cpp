#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    static unsigned int seed = 1;
    void srand(int newseed){
        seed = (unsigned) newseed & 0x7fffffffU;
    }

    int rand(void){
        seed = (seed * 1103515245U + 12345U) & 0x7fffffffU;
        return (int)seed;
    }

 int main(){

    ifstream fi("seed.tmp");
    int s; fi >> s;
    srand(s);
    cout << rand() << endl;
    fi.close();

    ofstream fo("seed.tmp");
    fo << rand() % 100;
    fo.close();

 return 0; }
