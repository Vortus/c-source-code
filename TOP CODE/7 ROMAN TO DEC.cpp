#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <map>

using namespace std;

int romanToInt(string s) {

    map<char, int> roman;
    roman['M'] = 1000;
    roman['D'] = 500;
    roman['C'] = 100;
    roman['L'] = 50;
    roman['X'] = 10;
    roman['V'] = 5;
    roman['I'] = 1;

    int res = 0;
    for (int i = 0; i < s.size() - 1; i++)
    {
        if (roman[s[i]] < roman[s[i+1]]) // jei dabartinis mazesnis uz kita atimt
            res -= roman[s[i]];
        else // kitu atveju pridedi reiksme
            res += roman[s[i]];
    }
    res += roman[s[s.size()-1]]; // nes tikrinau vienu i prieki

    return res;
}


 int main(){

    string s; cin >> s;
    cout << romanToInt(s);

 return 0; }
