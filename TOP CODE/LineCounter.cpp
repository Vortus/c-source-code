#include <iostream>
#include <fstream>
#include <dirent.h>
#include <vector>
#include <string>

using namespace std;

    ofstream writer;

    int allNewLines = 0;

    bool isFolder(string s){
        if(s.find('.') > 400) return true;
        return false;
    }

    int newLineCount(string s){
        int count = 0;
        ifstream reader(s);
        if(!reader) return 0;
        while(!reader.eof()){
            char c; reader.get(c);
            if(c == '\n') count++;
        }
        reader.close();
        return count;
    }

    int viewFolders(const char *path, vector<string> ends){
        writer << "MASTERPATH: " << path << endl;

        DIR *dir; struct dirent *ent;
        vector <string> paths;
        if ((dir = opendir (path)) != NULL) {

            while ((ent = readdir (dir)) != NULL){
                string s = ent -> d_name;
                for(int i = 0; i < ends.size(); i++)
                    if(s.find("." + ends.at(i)) < 400){//is the file
                            string tmp (path); tmp += s;
                            int n = newLineCount(tmp); writer << "    FILE: " << s << "(" << n << ")" << endl; allNewLines += n; }
                if(isFolder(s)){ writer << "  SUBFOLDER: " << s << endl; paths.push_back(s); }
            }

            closedir (dir);

            for(int i = 0; i < paths.size(); i++){
                string tmp(path);
                tmp += paths.at(i) + "\\";
                viewFolders(tmp.c_str(), ends);
            }

        } else {
            perror ("");
            return EXIT_FAILURE; }
    }

 int main(){

    writer.open("linelog.txt");
    vector<string> ends;
    cout << "Enter file ends like 'cpp' (END to end): ";

    string line;
    do{ cin >> line; if(line != "END") ends.push_back(line);
    }while(line != "END");

    viewFolders(".\\", ends);
    writer << endl << "New line count: " << allNewLines << " with ";
    for(int i = 0; i < ends.size(); i++) writer << ends.at(i) << " ";
    writer << "file endings";

    writer.close();
    cout << "Results at linelog.txt ";
    cin.get();

 return 0; }
