#include <iostream>
using namespace std;

    int power(int n, int x){
        if(x == 1) return n;
        return power(n, x - 1) * n;
    }

int main(){

    int n; cin >> n;
    cout << n << endl << power(n, 3) << endl <<  power(n, 6) << endl;

    return 0;
}
