#include <iostream>
#include <fstream>
#include <algorithm>
using namespace std;

    #define FILE "in.txt" // Duomen� failo pavadinimas
    #define MAXN 1000 // Maksimalus l�k��i� kiekis
    typedef pair<int, int> Plate; // L�k�t�
    Plate plates[MAXN]; // L�k��i� masyvas
    int N; // L�k��i� kiekis

    void readFile(){ /// Failo nuskaitymas
        ifstream FI(FILE);
        FI >> N; // L�k��i� kiekis
        int i, dx, dy;
        for(i = 0; i < N; i++){ // Nuskaitomos l�k�t�s
            FI >> dx >> dy;
            if(dx > dy) swap(dx, dy); // Pavertimas 90 laipsni� kampu
            plates[i] = Plate(dx, dy);
        }
        sort(plates, plates + N); // Surikiuojamos l�k�t�s
        FI.close();
    }

    int longestIncrSub(){ /// Ilgiausia did�janti seka
        int L[MAXN]; // Papildomas masyvas skai�iavimams
        int resultMax, i, j; // Rezultatas

        for(i = 0; i < N; i++){
            resultMax = 0;
            for(j = 0; j < i; j++){
                if(plates[i].second >= plates[j].second && L[j] > resultMax)
                    resultMax = L[j];
            }
            L[i] = resultMax + 1;
        } resultMax = -1;

        for(i = 0; i < N; i++)
            resultMax = max(resultMax, L[i]);

        return resultMax;
    }

    void displayResults(){ /// Rezultat� i�vedimas
        cout << longestIncrSub(); // Atsakymo i�vedimas
    }

 int main(){

    readFile(); /// Apdorojamas duomen� failas
    displayResults(); /// Rezultat� i�vedimas

 return 0; }
