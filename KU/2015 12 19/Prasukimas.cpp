#include <iostream>
using namespace std;

    int monthdays[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    bool leap(int n){
        return n % 400 == 0 || (n % 100 != 0 && n % 4 == 0);
    }

    int distanceMonths(int n, int year){
        int d = 0;
        for(int i = 0; i < n - 1; i++){
            d += monthdays[i];
            if(leap(year) && i == 1) d++;
        }
        return d;
    }

    int distanceYears(int n){
        int d = 0;
        for(int i = 2007; i < n; i++){
            if(leap(i)) d += 366;
            else d += 365;
        }
        return d;
    }


int main(){

    int a; cin >> a; // metai

    int starts = 1, days;
    for(int i = 31; i >= 1; i--){
        days = distanceMonths(3, a) + i + distanceYears(a);
        if(days % 7 == 0) { starts = i; break; }
    }
    cout << "Laikrodis persukamas pirmyn: " << a << ".03." << starts << endl;

    starts = 1;
    for(int i = 31; i >= 1; i--){
        days = distanceMonths(10, a) + i + distanceYears(a);
        if(days % 7 == 0) { starts = i; break; }
    }
    cout << "Laikrodis persukamas atgal: " << a << ".10." << starts << endl;

    return 0;
}
