#include <iostream>
using namespace std;

    int monthdays[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    bool leap(int n){
        return n % 400 == 0 || (n % 100 != 0 && n % 4 == 0);
    }

    int distanceMonths(int n, int year){
        int d = 0;
        for(int i = 0; i < n - 1; i++){
            d += monthdays[i];
            if(leap(year) && i == 1) d++;
        }
        return d;
    }

    int distanceYears(int n){
        int d = 0;
        for(int i = 2007; i < n; i++){
            if(leap(i)) d += 366;
            else d += 365;
        }
        return d;
    }

int main(){

    int a, b, c; cin >> a >> b >> c;
    int days = distanceMonths(b, a) + c + distanceYears(a);

    switch(days % 7){
    case 0:
        cout << "Sekmadienis";
        break;
    case 1:
        cout << "Pirmadienis";
        break;
    case 2:
        cout << "Antradienis";
        break;
    case 3:
        cout << "Treciadienis";
        break;
    case 4:
        cout << "Ketvirtadienis";
        break;
    case 5:
        cout << "Penktadienis";
        break;
    case 6:
        cout << "Sestadienis";
        break;
    }

    return 0;
}
