#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    string decToBin(int n){
        string res;
        while(n > 0){
            int j = n % 2;
            char c = j + '0';
            res = c + res;
            n /= 2;
        }
        return res;
    }

    void check(int a, int b){
        int i = 1;
        while(a > 0 || b > 0){
            int j = a % 2;
            int jj = b % 2;
            if(j == 0 && jj == 1) { cout << "HIPOTEZE KLAIDINGA"; return; }
            if(j != jj) cout << i << " ";
            i++;
            a /= 2;
            b /= 2;
        }
        cout << "HIPOTEZE TEISINGA" << endl;
    }

    void addzeros(string &s, int l){
        while(s.length() < l)
            s = '0' + s;
     }

     void check(string a, string b, int l){
        bool t = 1; int nutr = 0;
        cout << "Nutruke laidai: ";
        for(int i = 0; i < a.length(); i++){
            if(a[i] == '0' && b[i] == '1' && t) t = false;
            if(a[i] != b[i]){ cout << l - i << " "; nutr++; }
        }
        if(nutr == 0) cout << "NERA" << endl;
        if(!t) cout << "HIPOTEZE KLAIDINGA" << endl;
        else cout << "HIPOTEZE TEISINGA" << endl;
     }

 int main(){

    int an = 332, bn = 476;
    string a = decToBin(an); string b = decToBin(bn);
    if(a.length() < b.length()){
        addzeros(a, b.length());
    } else if(a.length() > b.length()){
        addzeros(b, a.length());
    }
    cout << a << endl << b << endl;

    if(an == bn) { cout << "SKAICIUS PERDUOTAS TEISINGAI"; return 0; }
    check(a, b, a.length());

    cout << endl << endl;
    check(an, bn);

 return 0; }
