#include <iostream>
#include <iomanip>
#include <cmath>
#include <fstream>
#include <algorithm>
using namespace std;

    char board[10000][10000]; // lenta
    int N;

    // n kubo dydis, x,y kordinates pradzios kubo
    void cube(int n, int x, int y){ // paima kuba, sudalija i 9 dalis, ir eina i kiekviena dali iskyrus i viduri.
        if(n == 1){ board[y][x] = '#'; return; } // jei vienas kubukas
        n = n / 3; // sumazina kubo dydi
        /////suskirstymas
        for(int i = 0; i < 3; i++)
            for(int j = 0; j < 3; j++)
                if(i != 1 || j != 1) cube(n, j * n + x, i * n + y);
    }

int main(){

    cin >> N; N = pow(3, N); // dimensija
    fill(board[0], board[N], '.'); // uzpildo lenta taskum
    cube(N, 0, 0); // paleidzia kubo daryma

    ofstream FO("rez.txt");
    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++)
            FO << setw(1) << board[i][j];
        FO << endl;
    }
    FO.close();

    return 0;
}
