#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int getSeqLength(int n){
        int result = 1;
        while(n != 1){
            if(n % 2 == 0) n /= 2;
            else n = (n * 3) + 1;
            result++;
        }
        return result;
    }

 int main(){

    ifstream fi("in.txt");
    while(!fi.eof()){
        int a, b; fi >> a >> b;
        int max = 0;
        for(int i = a; i <= b; i++){
            int j = getSeqLength(i);
            if(j > max) max = j;
        }
        cout << a << " " << b << " " << max << endl;
    }
    fi.close();

 return 0; }
