#include <iostream>
#include <iomanip>
#include <fstream>
#include <string.h>

#define MAXN            20
#define MATCH           0
#define INSERT          1
#define DELETE          2

using namespace std;

    struct Cell{
        int cost;
        int parent;
    } m[MAXN][MAXN];

    int match(char c, char d){
        return c == d ? 0 : 1;
    }

    int indel(char c){
        return 1;
    }

    void rowInit(int i){
        m[0][i].cost = i;
        if(i > 0) m[0][i].parent = INSERT;
        else m[0][i].parent = -1;
    }

    void columnInit(int i){
        m[i][0].cost = i;
        if(i > 0) m[i][0].parent = DELETE;
        else m[i][0].parent = -1;
    }

    int stringCmpr(char *s, char *t){
        int i, j;
        int opt[3];

        for(i = 0; i < MAXN; i++){
            rowInit(i);
            columnInit(i);
        }

        for(i = 1; i <= strlen(s) + 1; i++){
            for(j = 1; j <= strlen(t) + 1; j++){
                opt[MATCH] = m[i - 1][j - 1].cost + match(s[i], t[j]);
                opt[INSERT] = m[i][j - 1].cost + indel(t[j]);
                opt[DELETE] = m[i - 1][j].cost + indel(s[i]);

                m[i][j].cost = opt[MATCH];
                m[i][j].parent = MATCH;
                for(int k = INSERT; k <= DELETE; k++){
                    if(opt[k] < m[i][j].cost){
                        m[i][j].cost = opt[k];
                        m[i][j].parent = k;
                    }
                }
            }
        }

        for(int a = 0; a < MAXN; a++){
            for(int b = 0; b < MAXN; b++)
                cout << m[a][b].cost << " ";
                cout << endl;
        }

        return m[strlen(s)][strlen(t)].cost;
    }

 int main(){

    cout << stringCmpr("thou shalt not", "you should not");

 return 0; }
