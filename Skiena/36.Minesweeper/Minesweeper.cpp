#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    const int MAXH = 110, MAXW = 110;
    int buffer[MAXH][MAXW];
    int height, width;

    void calculateAround(int xof, int yof){
        xof--; yof--;
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                int x = j + xof;
                int y = i + yof;
                if(x > -1 && y > -1 && x < width && y < height && // borderiu constrainai
                   buffer[y][x] != -1)
                    buffer[y][x]++;
            }
        }
    }

    void display(){
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                if(buffer[i][j] == -1)  cout << "*";
                else cout << buffer[i][j];
            }
            cout << endl;
        }
    }

    void readFile(){
        ifstream fi("in.txt");
        fi >> height >> width;
        fi.ignore(80, '\n');
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                char c; fi.get(c);
                if(c == '*'){
                    buffer[i][j] = -1;
                    calculateAround(j, i);
                }
            }
            fi.ignore();
        }
        fi.close();
    }

 int main(){

    readFile();
    display();

 return 0; }
