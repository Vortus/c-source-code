#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    double startC, endC, step;

    cout << "Please give in a lower limit, limit >= 0: "; cin >> startC;
    cout << "Please give in a higher limit, " << startC << " > limit <= 50000: "; cin >> endC;
    cout << "Please give in a step, 0 < step <= 10: "; cin >> step;

    cout << endl << left << setw(10) << "Celcius" << setw(15) << "Fahrenheit" << endl;
    cout << left << setw(10) << "-------" << setw(15) << "----------" << endl;

    for(double i = startC; i <= endC; i += step)
        cout << left << fixed << setprecision(6) << setw(10) << i << setw(15) << i * 1.8 + 32 << endl;

 return 0; }
