#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(int argc, char* argv[]){

    ifstream reader(argv[1]);
    int count = 0;
    if(!reader){ cout << "File not found!"; return 1; }

    while(!reader.eof()){
        char c; reader.get(c);
        if(c == '\n') count++;
    }

    reader.close();

    cout << "Newline count: " << count;

 return 0; }
