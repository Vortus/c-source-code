#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(int argc, char *argv[]){

    ifstream reader(argv[1]);

    if(!reader){ cout << "File doesn't exist!"; return 1; }
    long start, end;

    start = reader.tellg();
    reader.seekg(0, ios::end);
    end = reader.tellg();
    reader.close();

    cout << "Size of file is: " << end - start << " bytes";

 return 0; }
