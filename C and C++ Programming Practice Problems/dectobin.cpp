#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

    string reversed(string s){
        if(s.length() <= 1) return s;
        return reversed(s.substr(1)) + s[0];
    }

 int main(int argc, char *argv[]){

    int n = stoi(argv[1]); string bin;
    while(n > 0){
        int j = n % 2;
        n /= 2;
        bin += to_string(j);
    }
    cout << reversed(bin);

 return 0; }
