#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    void swapChars(char &a, char &b){
        char tmp = a;
        a = b;
        b = tmp;
    }

    void permutation(string s, int i, int n){
        if(i == n)
            cout << s << endl;
        else {
            for(int j = i; j < s.length(); j++){
                swap(s[i], s[j]);
                permutation(s, i + 1, n);
                swap(s[i], s[j]);
            }
        }
    }

 int main(int argc, char *argv[]){

    string s = argv[1];

    permutation(s, 0, s.length() - 1);

 return 0; }
