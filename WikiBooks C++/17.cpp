#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    bool prime(long long n){
        if(n == 1) return false;
        for(long long i = 2; i <= sqrt(n); i++)
            if(n % i == 0) return false;
        return true;
    }

 int main(){

    long long n; cin >> n;
    long long i = 2;
    while(n > 0){
        if(prime(i)) n--;
        i++;
    }

    cout << i - 1;

 return 0; }
