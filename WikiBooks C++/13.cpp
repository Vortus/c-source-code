#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    int n, times = 1; cin >> n;

    for(int i = 0; i < 9; i++){
        int j; cin >> j;
        if(j > n){ n = j; times = 1; }
        else if(j == n) times++;
    }

    cout << n << " " << times;

 return 0; }
