#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    double sum = 0, c = 0;

    while(true){
        double j; cin >> j;
        if(j == 0){
            if(c == 0){ cout << "No AVERAGE"; return 0; }
            cout << sum / c; return 0;
        }else if(j < 0) cout << "ERROR: ";
         else c++; sum += j;
    }

 return 0; }
