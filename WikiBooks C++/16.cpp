#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    bool prime(int n){
        if(n == 1) return false;
        for(int i = 2; i <= sqrt(n); i++)
            if(n % i == 0) return false;
        return true;
    }

 int main(){

    int n; cin >> n;
    int counter = 0;
    for(int i = 2; i <= n; i++)
        if(prime(i)) counter++;
    cout << counter;

 return 0; }
