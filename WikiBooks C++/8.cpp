#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int u(int n){
        if(n <= 1) return 1;
        return u(n - 1) + u(n - 2);
    }

    int fib(int n, int next, int current){
        if(n == 0) return current;
        return fib(n - 1, next + current, next);
    }

 int main(){

    int n; cin >> n;
    cout << u(n) << endl;
    cout << fib(n, 1, 1) << endl;

 return 0; }
