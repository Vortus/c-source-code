#include <iostream>
#include <iomanip>
#include <fstream>
#include <windows.h>
#include <time.h>
#include <wtypes.h>

using namespace std;

void GetDesktopResolution(int& horizontal, int& vertical)
{
   RECT desktop;
   const HWND hDesktop = GetDesktopWindow();
   GetWindowRect(hDesktop, &desktop);
   horizontal = desktop.right;
   vertical = desktop.bottom;
}

 int main(){
    srand(time(NULL));
    int xMax, yMax;
    GetDesktopResolution(xMax, yMax);

    while(true){
        int x = rand() % xMax;
        int y = rand() % yMax;
        SetCursorPos(x, y);
    }

 return 0; }
