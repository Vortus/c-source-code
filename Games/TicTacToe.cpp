#include <iostream>
#include <iomanip>
#include <fstream>
#include <windows.h>
#include <stdlib.h>
#include <time.h>

using namespace std;

    char board[9];

    void displayBoard(char b[]){
        cout << "TIC-TAC-TOE" << endl << endl;

        for(int i = 0; i < 9; i++){
            if(i != 0 && i % 3 == 0) cout << "|" << endl;
            cout << "|" << b[i];
        }
        cout << "|" << endl << endl;
    }

    bool goodOption(int i, char enemy){
        return (i >= 1 && i <= 9) && board[i - 1] == '.' && board[i - 1] != enemy ? true : false;
    }

    void setBoard(int i, char c){
        board[i] = c;
    }

    void cpy(char to[], char from[], int l){
        for(int i = 0; i < l; i++)
            to[i] = from[i];
    }

    bool won(char b[], char c){

        if(b[0] == c && b[1] == c && b[2] == c) return true;
        if(b[3] == c && b[4] == c && b[5] == c) return true;
        if(b[6] == c && b[7] == c && b[8] == c) return true;

        if(b[0] == c && b[3] == c && b[6] == c) return true;
        if(b[1] == c && b[4] == c && b[7] == c) return true;
        if(b[2] == c && b[5] == c && b[8] == c) return true;

        if(b[0] == c && b[4] == c && b[8] == c) return true;
        if(b[2] == c && b[4] == c && b[6] == c) return true;

        return false;
    }

    void botAI(char you, char bot, int last, int times){

        if(times == 0 && board[0]&& bot == 'X'){ setBoard(0, bot); return; } // x best
        if(times == 0 && board[4] != you){ setBoard(4, bot); return; } // O best

        char tmpBoard[9];

        for(int i = 0; i < 9; i++){ // not losing
            cpy(tmpBoard, board, 9);
            if(tmpBoard[i] == '.'){

                tmpBoard[i] = bot;
                if(won(tmpBoard, bot)){ setBoard(i, bot); return; }

                tmpBoard[i] = you;
                if(won(tmpBoard, you)){ setBoard(i, bot); return; }

            }
        }

        srand (time(NULL));
        int place = rand() % 9;
        while(board[place] != '.')
            place = rand() % 9;
        setBoard(place, bot);
    }

 int main(){

    for(int i = 0; i < 9; i++) board[i] = '.';
    int times = 0;
    char you = 'X', tmpYou = ' ', bot = 'O';
    cout << "Choose X or O?: "; cin >> tmpYou; if(tmpYou == 'O'){ you = tmpYou; bot = 'X'; }

    while(true){

        system("CLS");

        int option = -1;
        if(times == 10) { cout << "GAME OVER, TIE" << endl; displayBoard(board); cin >> option; return 0; }
        if(you == 'O') botAI(you, bot, option - 1, times);

        if(won(board, you)){ cout << you << ", WON!" << endl; displayBoard(board); cin >> option; return 0;}
        else if(won(board, bot)){ cout << bot << ", WON!" << endl; displayBoard(board); cin >> option; return 0; }

        displayBoard(board);
        cout << "You are " << you << " choose between 1-9: "; cin >> option;

        if(goodOption(option, bot)){
            setBoard(option - 1, you);
            if(times != 8 && you == 'X') botAI(you, bot, option - 1, times);
            times += 2;
        }

    }

  return 0; }
