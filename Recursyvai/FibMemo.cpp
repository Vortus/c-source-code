#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int memo[1000];

    int fib(int n){
        if(memo[n] != -1) return memo[n];
        if(n <= 2) return 1;
        return memo[n] = fib(n - 1) + fib(n - 2);
    }

 int main(){
    int n = 7;
    for(int i = 0; i <= n; i++)
        memo[i] = -1;

    cout << fib(n);
 return 0; }
