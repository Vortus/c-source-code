#include <iostream>
#include <string>

using namespace std;
    
    int fibH(int n, int next, int current){
     if(n <= 1) return current;
     return fibH(n - 1, next + current, next);
    }
    
    int fib(int n){
        return fibH(n, 1, 1);
    }


int main()
{
  cout << fib(7); 
}