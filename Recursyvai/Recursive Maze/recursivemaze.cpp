#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    bool RecursiveMaze(int x, int y);

    int maze[]// 0 siena 1 kelias
    {
    0,1,0,0,0,0,0,0,0,0,
    0,1,0,0,0,1,1,1,0,0,
    0,1,1,1,0,0,1,0,1,0,
    0,1,0,1,1,1,1,1,1,0,
    0,1,0,0,0,0,0,0,1,0,
    0,0,0,0,0,0,0,0,1,0
    };

    int endX = 5, endY = 1, width = 10, height = 6, steps = 1; // 1 nes reikia viso kelio ilgio skaitant ir pradzia

    int main()
    {
        cout << RecursiveMaze(1,0) << endl;
        cout << steps;
        return 0;
    }

    bool RecursiveMaze(int x, int y)
    {
        if(x == endX && y == endY) { return true; } // jei atejo i vieta
        //jei siena 0 arba praeitas 2, praleisti
        if(maze[x + y * width] == 0 || maze[x + y * width] == 2) { return false; }

        maze[x + y * width] = 2;// uzlipom

        //spaudins kelia atvirksciai kadangi recursyvas...
        //ieskos kol atras pabaiga. jei neras tai spausdins false
        if(x != 0) if(RecursiveMaze(x - 1, y)) {  cout << "Kaire "; steps++; return true; }
        if(y != 0) if(RecursiveMaze(x, y - 1)) {  cout << "Virsus "; steps++; return true; }
        if(x != width - 1) if(RecursiveMaze(x + 1, y)) { cout << "Desine "; steps++; return true; }
        if(y != height - 1) if(RecursiveMaze(x, y + 1)) {  cout << "Apacia "; steps++; return true; }

        return false; // jei neber kur eit
    }
