#include <io.h>
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

void Skaiciuokle (int M, double &C, double &P, double &J)
{
    C = M * 100 / 2.54;
    P = M * 100 / 2.54 / 12;
    J = M * 100 / 2.54 /12 / 3;
}

int main ()
{
    int i,n,M;
    double C,P,J;
    n = 5;
    cout << "Metrai " << "Coliai " << "Pedos " << "Jardai" << endl;

    for (i = 1; i <= n; i++){
        M=i;
        Skaiciuokle (M,C,J,P);
        cout << M << setw(11);
        cout << fixed << setprecision(2) << C << "  " << P << "   " << J <<endl;
    }

return 0;
}
