#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int mod(int numb, int factor){
        if(numb == 0) return 0;
        if(numb < 0) return numb + factor;
        return mod(numb - factor, factor);
    }

    int div(int numb, int factor, int times){
        if(numb == 0) return times;
        if(numb < 0) return times - 1;
        return div(numb - factor, factor, times + 1);
    }

 int main(){

	cout << div(12, 12, 0) << endl;

 return 0; }
