#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int arr[1000];

    int fib(int n){
        if(n == 0) return 0;
        if (arr[n] > 0)
            return arr[n];
        return arr[n] = fib(n - 1) + fib(n - 2);
    }

 int main(){

	int n = 7;
	for(int i = 0; i < 1000; i++) arr[i] = 0;
    arr[1] = 1;  arr[2] = 1;

    cout << fib(n);

 return 0; }
