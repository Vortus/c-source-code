#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    char calculate(char a, char b, char option){
        int an = a - '0';
        int bn = b - '0';

        switch(option){
            case '+': return (an + bn) + '0'; break;
            case '-': return (an - bn) + '0'; break;
            case '*': return (an * bn) + '0'; break;
        }
    }

    char rec(string s){
        cout << s << endl;

        if(s.length() <= 1) return s[0] - '0';
        if(s.length() == 3){
            return calculate(s[0], s[2], s[1]);
        }

        if(s[0] == '('){
            int end = s.find_last_of(')');
            return rec(s.substr(1, end - 1)) + s.substr(end - 1);
        }
    }

 int main(){

	cout << rec("((4-2)*6)");

 return 0; }
