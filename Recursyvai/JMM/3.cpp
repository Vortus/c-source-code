#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    double pow(double x, double n){
        if(n == 1) return x;
        if(n < 0) return 1 / pow(x, abs(n));
        if(n > 0) return pow(x, n - 1) * x;
    }

 int main(){

	cout << pow(2, -4) << endl;

 return 0; }
