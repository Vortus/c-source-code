#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int w, h;
    int map[1000][1000];
    bool explored[1000][1000];

    void readFile(string s){
        ifstream fi(s);
        fi >> h >> w;
        for(int y = 0; y < h; y++)
            for(int x = 0; x < w; x++)
                fi >> map[y][x];
        fi.close();
    }

    bool explore(int x, int y){

        if(explored[y][x] || map[y][x] == 0) return false; // jei buvom arba 0
        explored[y][x] = true; // explorinom

        if(x != w - 1) explore(x + 1, y);
        if(y != h - 1) explore(x, y + 1);
        if(x != 0) explore(x - 1, y);
        if(y != 0) explore(x, y - 1);

        return true; // praejom visa
    }

 int main(){

    readFile("in.txt");
    int result = 0;

    for(int i = 0; i < h; i++){
        for(int j = 0; j < w; j++){
           bool b = explore(j, i);
           if(b) result++;
        }
    }

    for(int i = 0; i < h; i++){
        for(int j = 0; j < w; j++)
            cout << explored[i][j];
        cout << endl;
    }

    cout << result;

 return 0; }
