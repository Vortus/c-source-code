#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string.h>

using namespace std;

    string reverse(string s){
        if(s.length() <= 1) return s;
        return reverse(s.substr(1)) + s[0];
    }

 int main(){

	while(true){
        string s; cin >> s;
        cout << reverse(s);
	}

 return 0; }
