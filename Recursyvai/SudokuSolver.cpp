#include <iostream>
#include <fstream>
using namespace std;

    #define UNASSIGNED 0 // Pasižymim nežinomą skaičių
    #define N 9 // Lentelės dydis
    #define SON 3 // Dėžutės dydis

    int board[9][9]; /// Lentelė

    bool findUnassigned(int &x, int &y){ /// Randama nebūta koordinatė
        for(y = 0; y < N; y++)
            for(x = 0; x < N; x++)
                if(board[y][x] == UNASSIGNED) return true;
        return false;
    }

    bool rowCheck(int y, int num){ /// Tikrinama eilutė(horizontaliai)
        for(int x = 0; x < N; x++)
            if(board[y][x] == num) return true;
        return false;
    }

    bool colCheck(int x, int num){ /// Tikrinamas stulpelis(vertikaliai)
        for(int y = 0; y < N; y++)
            if(board[y][x] == num) return true;
        return false;
    }

    bool boxCheck(int bsx, int bsy, int num){ /// Tikrinama dėžutė, šiuo atvėju 3x3 dydžio
        for(int y = 0; y < SON; y++)
            for(int x = 0; x < SON; x++)
                if(board[y + bsy][x + bsx] == num) return true;
        return false;
    }

    bool isSafe(int x, int y, int num){ /// Ar tinka skaičius, x, y koordinatėse
        return !rowCheck(y, num) &&
                !colCheck(x, num) &&
                !boxCheck(x - x % SON, y - y % SON, num);
    }

    void printBoard(){ /// Išspausdinamas sudoku
        for(int i = 0; i < N; i++){
            for(int j = 0; j < N; j++)
                cout << board[i][j];
            cout << endl;
        }
    }

    void readFile(string s){ /// Failo nuskaitymas
        ifstream fi(s);
        for(int i = 0; i < N; i++)
            for(int j = 0; j < N; j++)
                fi >> board[i][j];
        fi.close();
    }

    bool solveSudoku(){ /// Sudoku rekursija
        int x, y; // Langelio pozicija
        if(!findUnassigned(x, y)) // Surandamas nepriskirtas langelis, ir jei nėra kaip priskirti reiškia sudoku išspręstas.
            return true;
        for(int num = 1; num <= N; num++){ // Tikrinami visi skaičiai
            if(isSafe(x, y, num)){ // Tikrinama ar yra galimas variantas
                board[y][x] = num; // Pasižymimas langelis
                if(solveSudoku()) // Kviečiama vėl
                    return true;
                board[y][x] = UNASSIGNED; // Jei skaičius netiko, bandome kitą variantą
            }
        }
        // Backtrackingas, jei nebetiko nei vienas skaičius ir reikia gryžti vienu lygiu atgal, ir tikrinti toliau.
        return false;
    }

 int main(){

    readFile("Sudoku.txt");
    solveSudoku();
    printBoard();

 return 0; }
