
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    string forward(string list[], int current, int i){
        if(current == i - 1) return list[current];
        return list[current] + " " + forward(list, current + 1, i);
    }

    string backward(string list[], int current){
        if(current == 0) return list[current];
        return list[current] + " " + backward(list, current - 1);
    }

 int main(){

    string list[10000];
    int i = 0;

    while(true){
        string line; getline(cin, line);
        if(line == ".") break;
        list[i] = line;
        i++;
    }

    cout << backward(list, i).substr(1);

	return 0; }
