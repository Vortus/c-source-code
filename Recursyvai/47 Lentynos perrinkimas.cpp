#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int arr[1000];
    bool used[1000];

    void coutArr(int m){
        for(int i = 1; i <= m; i++)
            cout << arr[i];
        cout << endl;
    }

    int generate(int m, int n){
        if(m > n){ coutArr(n); return 0; }
        for(int i = 1; i <= n; i++){
            if(!used[i]){
                used[i] = true;
                arr[m] = i;
                generate(m + 1, n);
                used[i] = false;
            }
        }
        return 1;
    }

 int main(){

    generate(1, 2);

 return 0; }
