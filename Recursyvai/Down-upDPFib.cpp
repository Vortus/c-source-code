#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int fibMemo[1000];

    int fib(int n){
        int n1 = 1, n2 = 1, n3 = 0;
        if(n <= 2) return 1;
        for(int k = 3; k <= n; k++){
          n3 = n1 + n2;
          n1 = n2; n2 = n3;
        }
        return n3;
    }
 int main(){

    int n = 15;
    cout << fib(n);

 return 0; }
