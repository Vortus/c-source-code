#include <iostream>
#include <string>
#include <cmath>

using namespace std;

    int binarySearch(int value, int list[], int first, int last){
        if(first <= last){
                int midP = (first + last) / 2;
                if(value == list[midP]) return midP;
                else if(value < list[midP])
                    return binarySearch(value, list, first, midP - 1);
                else
                    return binarySearch(value, list, midP + 1, last);
        }
        return -1;
    }

int main()
{
    int list[] = { 2, 2, 3, 5, 8, 14, 16, 22, 22, 24, 30 };
    cout << binarySearch(2, list, 1, 10);
}
