#include <iostream>
#include <algorithm>
#include <fstream>
using namespace std;

    void insertionSort(int arr[], int l){
        for(int i = 1; i < l; i++){
            int key = i;
            while(arr[key] < arr[key - 1] && key != 0)
                swap(arr[key--], arr[key - 1]);
        }
    }

 int main(){

    int arr[6] { 5, 2, 4, 6, 1, 3 };
    //insertionSort(arr, 6);
    sort(arr, arr + 6);

    for(int i = 0; i < 6; i++)
        cout << arr[i] << " ";

 return 0; }
