#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    void cocSort(int arr[], int l){

        for(int i = 0; i < l; i++){
            int c = 0;
            for(int j = i; j < l - i - 1; j++)
                if(arr[j] > arr[j + 1]){ swap(arr[j], arr[j + 1]); c++; }
            if(c == 0) return; // jei surusiuotas
            for(int j = l - i - 2; j >= i; j--)
                if(arr[j] > arr[j + 1]){ swap(arr[j], arr[j + 1]); c++; }
            if(c == 0) return; // jei surusiuotas
        }
    }

 int main(){

    int arr[10] = { 2, 4, 1, 3, 6, 99, 23, 14, 65, 11 };
    int n = 10;
    cocSort(arr, n);

    for(int i = 0; i < n; i++)
        cout << arr[i] << " ";

 return 0; }
