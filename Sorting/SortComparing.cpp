#include <iostream>
#include <iomanip>
#include <fstream>
#include <ctime>
#include <chrono>

using namespace std;

    int genArray(int arr[], int l){
        srand (time(NULL));// randomseed
        for(int i = 0; i < l; i++)
            arr[i] = rand() % 10000;
    }

    void selectionSort(int arr[], int l){
        for(int i = 0; i < l; i++){
            int minI = i;
            for(int j = i + 1; j < l; j++){
                if(arr[j] < arr[minI]) minI = j;
            }
            if(i != arr[minI]) swap(arr[i], arr[minI]);
        }
    }

    bool sorted(int arr[], int l){
        for(int i = 0; i < l - 1; i++)
            if(arr[i] > arr[i + 1]) return false;
        return true;
    }

 int main(){

    int arr[500], n = 13;
    genArray(arr, n);
    auto start_time = std::chrono::high_resolution_clock::now();
    selectionSort(arr, n);
    auto end_time = std::chrono::high_resolution_clock::now();
    auto time = end_time - start_time;
    cout << "Selection sort: " << std::chrono::duration_cast<std::chrono::microseconds>(time).count() << ", " << sorted(arr, n);

 return 0; }
