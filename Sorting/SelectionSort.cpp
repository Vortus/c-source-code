#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    void selSort(int arr[], int l){
        for(int i = 0; i < l; i++){
            int minI = i; // miIndeks
            for(int j = i + 1; j < l; j++)
                if(arr[j] < arr[minI]) minI = j;
            swap(arr[i], arr[minI]);
        }
    }

 int main(){

    int arr[10] = { 2, 4, 1, 3, 6, 99, 23, 14, 65, 11 };
    int n = 10;
    selSort(arr, n);

    for(int i = 0; i < n; i++)
        cout << arr[i] << " ";

 return 0; }
