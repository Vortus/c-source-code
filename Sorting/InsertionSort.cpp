#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    void insSort(int arr[], int l){

        for(int i = 0; i < l; i++){
            int b = arr[i], j = 0;
            while(b > arr[j]) j++;
            for(int k = i - 1; k > j - 1; k--)
                arr[k + 1] = arr[k];
            arr[j] = b;
        }
    }

 int main(){

    int arr[10] = { 2, 4, 1, 3, 6, 99, 23, 14, 65, 11 };
    int n = 10;
    insSort(arr, n);

    for(int i = 0; i < n; i++)
        cout << arr[i] << " ";

 return 0; }
