#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

 int main(){

    int arr[6] = { 3, 4, 1, 5, 6, 10 };
    int sorted[6];
    int n = 6;

    for(int j = 0; j < n - 1; j++){
        for(int i = 0; i < n - 1; i++){
            if(arr[i] < arr[i + 1]) swap(arr[i], arr[i + 1]);
        }
    }

    for(int i = 0; i < n; i++)
        cout << arr[i];

 return 0; }
