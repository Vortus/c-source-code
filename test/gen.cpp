#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

 int main(){

    cout << endl;
    for(int i = 2; i < 18; i+= 2){
        int k = i < 10 ? i : 18 % i;
        cout << setw(k / 2);
        for(int j = 1; j <= k; j++){
            cout << "#";
            if(j * 2 == k) cout << setw(12 - (j - 1) * 4 + 1);
        }
        cout << endl;
    }

    cout << endl;
    for(int i = 2; i < 18; i+= 2){
        int k = i < 10 ? i : 18 % i;
        cout << setw(abs(4 - k / 2) + 1);
        for(int j = 0; j < k; j++)
            cout << "#";
        cout << endl;
    }

    cout << endl;
    for(int i = 0; i < 4; i++){
        cout << setw(i + 1);
        for(int j = 1 + i; j <= 8 - i; j++)
            cout << "#";
        cout << endl;
    }

    cout << endl;
    int n = 3; n--;

    for(int i = n; i >= -n; i--){
        int j = abs(i);
        for(int k = 0; k <= n - j; k++)
            cout << "#";
        cout << endl;
    }


 return 0; }
