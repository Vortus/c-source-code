#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;


unsigned int *buffer;
void readfile() {
    FILE *f = fopen("house.bmp", "rb");
    buffer = new unsigned int[132*65];
    fseek(f, 54, 0);
    fread(buffer, 132*65*4, 1, f);
    fclose(f);
}

unsigned int getpixel(int x, int y) {
    //assuming your x/y starts from top left, like I usually do
    return buffer[(64 - y) * 132 + x];
}

 int main(){

    readfile();
    cout << getpixel(0, 0);

 return 0; }
