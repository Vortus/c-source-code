#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>

using namespace std;

    long long memo[1000];
    long long fibMemo[1000];

    long long fib(long long n){
        if(fibMemo[n] != -1) return fibMemo[n];
        return fibMemo[n] = fib(n - 1) + fib(n - 2);
    }

 int main(){

    long long n = 50;
    memo[0] = 0;
    memo[1] = 1;

    for(long long i = 2; i <= n; i++)
        memo[i] = memo[i - 1] + memo[i - 2];

    cout << memo[n] << endl;
    ////////
    for(int i = 0; i < 1000; i++)
        fibMemo[i] = -1;
    fibMemo[0] = 0;
    fibMemo[1] = 1;

    cout << fib(n) << endl;

 return 0; }
