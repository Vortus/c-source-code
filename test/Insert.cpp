#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    void insertToArray(int arr[], int &n, int number, int ind){
        for(int i = n - 1; i >= ind; i--)
            arr[i + 1] = arr[i];
        arr[ind] = number;
        n++;
    }

 int main(){

    int arr[10] = { 2, 3, 1, 5, 4 };
    int n = 5;
    insertToArray(arr, n, 99, 3);
    insertToArray(arr, n, 12, 1);

    for(int i = 0; i < n; i++)
        cout << arr[i] << " ";


 return 0; }
