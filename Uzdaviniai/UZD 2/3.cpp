
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    bool available(int a){
        int tempA = a;
        while(tempA > 0){
            int j = tempA % 10;
            if(j == 0 || a % j != 0) return false;
            tempA /= 10;
        }
        return true;
    }

 int main(){

	int a, b; cin >> a >> b;

    for(int i = a; i <= b; i++)
        if(available(i)) cout << i << endl;

 return 0; }
