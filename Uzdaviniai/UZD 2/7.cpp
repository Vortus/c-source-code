#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    string reversed(string s){
        string result;
        for(int i = 0; i < s.length(); i++)
            if(s[i] == '0') result += '1';
            else result += '0';
        return result;
    }

 int main(){

    string s = "0";
    int n; cin >> n;

    while(s.length() < n)
       s += reversed(s);

    cout << "- " << s[n - 1];

 return 0; }
