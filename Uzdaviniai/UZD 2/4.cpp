
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

	for(int i = 1000; i <= 9999; i++){
        int a = i / 100, b = i % 100;
        if((a + b) * (a + b) == i) cout << i << endl;
	}

 return 0; }
