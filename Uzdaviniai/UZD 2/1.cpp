
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

	int n, m; cin >> n >> m;
	int sum = m * m * m;

	for(int i = n; i > m; i--) sum += i * i * i;
	for(int i = n; i < m; i++) sum += i * i * i;

    cout << sum;

 return 0; }
