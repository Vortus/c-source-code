
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

	int n; cin >> n;
	int sum = 0;

	for(int i = 1; i <= n; i += 2)
        sum += i * i;

    cout << sum;

 return 0; }
