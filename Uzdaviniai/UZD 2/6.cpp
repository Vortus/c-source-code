
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    void check(int a, int arr[]){
        int tempA = a;
        while(tempA > 0){
            int j = tempA % 10;
            arr[j]++;
            tempA /= 10;
        }
    }

 int main(){

	int n; cin >> n;
    int arr[10]; for(int i = 0; i < 10; i++) arr[i] = 0;

	for(int i = 1; i <= n; i++)
        check(i, arr);


	for(int i = 0; i < 10; i++)
        cout << i << " = " << arr[i] << endl;

 return 0; }
