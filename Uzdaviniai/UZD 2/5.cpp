#include <sstream>
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    bool isPalindrome(int a){
        ostringstream temp; temp << a;
        string s = temp.str();
        for(int i = s.length() - 1; i >= 0; i--)
            if(s[i] != s[s.length() - 1 - i]) return false;
        return true;
    }

 int main(){

	int n; cin >> n;
	int i = 0, sk = 1;
	while(i < n){
        if(isPalindrome(sk) && isPalindrome(sk*sk)) {
            i++; cout << sk << endl; }
        sk++;
	}

 return 0; }
