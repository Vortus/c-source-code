#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    void check(int a, bool arr[]){
        if(a == 0){ arr[0] == true; return; }
        int tempA = a;

        while(tempA > 0){
            int j = tempA % 10;
            if(arr[j] != true) arr[j] = true;
            tempA /= 10;
        }
    }

 int main(){

    bool arr[10]; for(int i = 0; i < 10; i++) arr[i] = false;
    int n; cin >> n;

    for(int i = 0; i < n; i++){
        int j; cin >> j;
        check(j, arr);
    }

    for(int i = 0; i < 10; i++)
        if(!arr[i]) cout << i << " skaitmuo nebuvo panaudotas nei karto" << endl;

 return 0; }
