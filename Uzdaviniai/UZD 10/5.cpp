#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    int k, s, b, i, n; cin >> k >> s >> b >> i >> n;
    int allMoney = n, debt = 0, allDrinks = 0;
    bool first = true;

    while(true){
        if(i == 1){ allMoney += k; n += k; }
        int drinks = 0; drinks = n / s;
        allDrinks += drinks;
        if(allMoney > 100) break;
        int remainder = drinks * b;
        n %= s; n += remainder;
        debt -= drinks * s;
        if(!first && allMoney + debt >= s) break;
        if(first) first = false;
        allMoney += n;
        i++; i %= 8; if (i == 0) i = 1;
    }

    cout << allDrinks;

 return 0; }
