#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int fibh(int n, int current, int next){
        if(n == 1) return current;
        return fibh(n - 1, next, current + next);
    }

    int fib(int n){
        return fibh(n, 1, 1);
    }

    string conv(int n){
        string res;
        while(n > 0){
            int j = n % 10;
            char c = j + '0';
            res = c + res;
            n /= 10;
        }
        return res;
    }

 int main(){

    int n, i = 1; cin >> n;
    string s;

    while(s.length() < n){
        s += conv(fib(i));
        i++;
    }

    cout << s[n - 1];

 return 0; }
