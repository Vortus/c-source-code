#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int fibh(int i, int current, int next){
        if(i == 0) return current;
        return fibh(i - 1, next, next + current);
    }

    int fib(int i){
        return fibh(i, 1, 1);
    }

 int main(){

	int n; cin >> n;
	for(int i = 0; i <= n; i++){
        for(int j = 1; j <= fib(i); j++) cout << "* ";
        cout << endl;
	}

 return 0; }
