#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int gcd(int a, int b){
        if(b == 0) return a;
        return gcd(b, a % b);
    }

    int primes(int a, int b, int c){
        if(gcd(a, b) == 1 && gcd(b, c) == 1) return true;
        else return false;
    }

 int main(){

	int n; cin >> n;
	for(int a = 1; a <= n; a++){
        for(int b = a + 1; b <= n; b++){
            for(int c = b + 1; c <= n; c++){
                if(a * a + b * b == c * c && primes(a,b,c)) cout << a << ", " << b << ", " << c << endl;
            }
        }
	}

 return 0; }
