#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int reverseInt(int n){
        int s = 0;
        while(n > 0){
            int j = n % 10;
            s = s * 10 + j;
            n /= 10;
        }
        return s;
    }

    int sc(int n){
        int c = 0;
        while(n > 0){
            c++;
            n /= 10;
        }
        return c - 2;
    }

 int main(){

    int n; cin >> n;
    int reversed = reverseInt(n);
    bool first = true;
    for(int i = 0; i < sc(n); i++) cout << "(";

    while(n > 0){
        cout << reversed % 10; if(!first && n > 9) cout << ")";
        if(first) first = !first;
        if(n > 9) cout << " * 10 ";
        if(n / 10 > 0) cout << "+ ";
        reversed /= 10;
        n /= 10;
    }

 return 0; }
