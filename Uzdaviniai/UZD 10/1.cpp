#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    long long numcubesum(long long i){
        long long sum = 0;
        while(i > 0){
            sum += (i % 10) * (i % 10) * (i % 10);
            i /= 10;
        }
        return sum;
    }

 int main(){

	long long i = 1;
    while(i <= 2916){
        if(i == numcubesum(i)) cout << i << endl;
        i++;
    }

 return 0; }
