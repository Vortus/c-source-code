#include <cmath>
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int f(double n){
        return abs(5 * pow(n, 4) - 4 * pow(n, 3)) + abs(n - 3) - 13;
    }

  int main(){

	int m, n; cin >> m >> n;

    for(int i = m; i <= n; i++)
        cout << f(i) << endl;


 return 0; }
