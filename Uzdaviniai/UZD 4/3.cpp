
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int dividersCount(int n){
        int c = 0;
        for(int i = 1; i <= n; i++)
            if(n % i == 0) c++;
        return c;
    }

 int main(){

	int m, n, k, c = 0; cin >> m >> n >> k;

	for(int i = m; i <= n; i++)
        if(dividersCount(i) == k){ c++; cout << i << endl; }

    cout << c;

 return 0; }
