#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    void primeFact(int n){
        int i = 2;
        while(i * i <= n){
            if(n % i == 0){ n /= i; cout << i << "x"; }
            else i++;
        }
        if(n > 1) cout << n;
    }

 int main(){

	while(true){
        int i; cin  >> i;
        if(i == 0) break;
        cout << i << " = "; primeFact(i); cout << endl;
	}

 return 0; }
