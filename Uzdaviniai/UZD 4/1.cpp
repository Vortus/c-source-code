
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int numberSum(int i){
        int sum = 0, tmp = i;
        while(tmp > 0){
            int j = tmp % 10;
            sum += j;
            tmp /= 10;
        }
        return sum;
    }

 int main(){

    int n, c = 0; cout << "Iveskite n ir toliau seka: "; cin >> n;
	while(true){
        int i; cin >> i;
        if(i == 0) break;
        if(numberSum(i) == n) c++;
	}

    cout << c;

 return 0; }
