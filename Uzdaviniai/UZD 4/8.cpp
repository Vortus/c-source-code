
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int rec(int x, int n){
        if(n == 1) return x;
        return x + rec(x, n - 1) * x;
    }

 int main(){

	int x, n; cin >> x >> n;
	cout << rec(x, n);

 return 0; }
