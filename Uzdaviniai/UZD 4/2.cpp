
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    bool twoTimes(int i, int m){
        int tmp = i, c = 0;
        while(tmp > 0){
            int j = tmp % 10;
            if(j == m) c++;
            tmp /= 10;
        }
        if(c == 2) return true;
        return false;
    }

 int main(){

    int m, c = 0; cout << "Iveskite m ir seka: "; cin >> m;

	while(true){
        int i; cin >> i;
        if(i == 0) break;
        if(twoTimes(i, m)) c++;
	}

    cout << c;

 return 0; }
