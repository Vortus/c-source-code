
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int dividerCountSum(int n){
        int sum = 0;
        for(int i = 1; i <= n; i++)
            if(n % i == 0) sum += i;
        return sum;
    }

 int main(){

	int m, n, s, c = 0; cin >> m >> n >> s;

	for(int i = m; i <= n; i++)
        if(dividerCountSum(i) == s){ c++; cout << i << endl; }

    cout << c;

 return 0; }
