#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

    string seka; getline(cin, seka);
    char tarpine, virsus = 'a', apacia = 'b', priekis = 'c', nugara = 'd', kaire = 'e', desine = 'f';

    for(int i = 0; i < seka.length(); i++){
        switch(seka[i]){
            case 'K':
                tarpine = virsus;
                virsus = desine;
                desine = apacia;
                apacia = kaire;
                kaire = tarpine;
                break;
            case 'D':
                tarpine = virsus;
                virsus = kaire;
                kaire = apacia;
                apacia = desine;
                desine = tarpine;
                break;
            case 'A':
                tarpine = virsus;
                virsus = priekis;
                priekis = apacia;
                apacia = nugara;
                nugara = tarpine;
                break;
            case 'P':
                tarpine = virsus;
                virsus = nugara;
                nugara = apacia;
                apacia = priekis;
                priekis = tarpine;
                break;
        }
    }

    cout << virsus;

 return 0; }
