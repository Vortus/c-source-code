#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    bool istwosqr(double n){
        while(n >= 2) n /= 2;
        if(n == 1) return true;
        return false;
    }

 int main(){

	double n; cin >> n;
    cout << istwosqr(n);

 return 0; }
