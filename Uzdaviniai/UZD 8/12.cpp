#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

    bool check(string s){
        string nmb = s.substr(0, s.length() - 3);
        int rightN = s[s.length() - 2] - '0';
        for(int i = 0; i < nmb.length(); i++)
            if(nmb[i] - '0' >= rightN) return false;
        return true;
    }

 int main(){

    string s; getline(cin, s);
    cout << check(s);

 return 0; }
