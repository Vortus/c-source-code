#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    bool notPrime(int n){
        for(int i = 2; i <= sqrt(n); i++)
            if(n % i == 0) return true;
        return false;
    }

 int main(){

    int n; cin >> n;

    for(int i = 1; i <= n; i++)
        for(int j = i; j <= n; j++)
            if(notPrime(i) && notPrime(j) && i * j == n) cout << i << "x" << j << " = " << n << endl;


 return 0; }
