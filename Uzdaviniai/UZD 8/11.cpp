#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

     int i, j; cin >> i >> j;
     int arr[i][j];

     for(int y = 0; y < j; y++){
        for(int x = 0; x < i; x++){
            if((x + y) % 2 == 0) arr[x][y] = 1;
            else arr[x][y] = 0;
            cout << arr[x][y] << " ";
        }
        cout << endl;
     }

 return 0; }
