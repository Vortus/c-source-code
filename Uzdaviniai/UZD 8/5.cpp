#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    double nsub(int n){
        double res = 1; int j = 1;
        for(int i = 0; i < n; i++){
            j += 2;
            res *= j;
        }
        return res;
    }

 int main(){

    int n; cin >> n;
    double sum = 1;

    for(int i = 1; i <= n; i++){
        if(i % 2 != 0){
            sum -= 1 / nsub(i);
        }else sum += 1 / nsub(i);
    }

    cout << sum;

 return 0; }
