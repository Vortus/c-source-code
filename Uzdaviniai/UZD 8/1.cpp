#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    bool isPrime(int n){
        if(n == 1) return false;
        for(int i = 2; i <= sqrt(n); i++)
            if(n % i == 0) return false;
        return true;
    }

 int main(){

	int a; cin >> a;
    while(true){
        a++;
        if(isPrime(a)){ cout << a; break; }
    }

 return 0; }
