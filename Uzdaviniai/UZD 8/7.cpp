#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int spaceCount(string s){
        int result = 0;
        for(int i = 0; i < s.length(); i++)
            if(s[i] == ' ') result++;
        return result;
    }

 int main(){

    string s; getline(cin, s);
    int c = spaceCount(s);
    if(s.length() > 0) c++;

    cout << c;

 return 0; }
