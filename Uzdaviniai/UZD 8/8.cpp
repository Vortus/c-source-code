#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    int positive = 0, negative = 0, all = 5;
    double arr[] = { 1.1 , -1.1, 0, 2, -1 };

    for(int i = 0; i < all; i++)
        if(arr[i] > 0) positive++;
        else if(arr[i] < 0) negative++;

    cout << "+: " << positive << ", -: " << negative << ", 0: " << all - positive - negative;

 return 0; }
