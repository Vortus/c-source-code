#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

    string intton(int number, int n){
        string res;
        while(number > 0){
            int j = number % n;
            res = to_string(j) + res;
            number /= n;
        }
        return res;
    }

 int main(){

    int number, n; cin >> number >> n;
    cout << intton(number, n);

 return 0; }
