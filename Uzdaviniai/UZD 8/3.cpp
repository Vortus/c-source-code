#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

        bool istwosqr(double n, int &x){
        while(n >= 2){ x++; n /= 2; }
        if(n == 1) return true;
        return false;
    }

 int main(){

	int n, x = 0; cin >> n;
    bool c = istwosqr(n, x);
    if(c){ cout << 2 << "^" << x; }
    else cout << c;

 return 0; }
