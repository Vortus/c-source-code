
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

	int arr[10] = { 1, 2, 3, 10, 5, 6, 7, 8, 9, 4 };

	int max = arr[0], maxI = 0, min = arr[0], minI = 0;

	for(int i = 0; i < 10; i++)
        if(arr[i] > max) { max = arr[i]; maxI = i; }
        else if(arr[i] < min) { min = arr[i]; minI = i; }

    arr[maxI] = min;
    arr[minI] = max;

    for(int i = 0; i < 10; i++)
        cout << arr[i] << " ";

 return 0; }
