#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int f(int k, bool show){
        int c = 0;
        for(int i = 1; i <= k; i++)
            if(k / i == k % i){ if(show) cout << "K = " << k << ", X = " << i << endl; c++; }
        return c;
    }

 int main(){

	int n, maxK = 0, maxC = 0; cin >> n;

	for(int k = 1; k <= n; k++){
        int c = f(k, false);
        if(c >= maxC){ maxC = c; maxK = k; }
	}

    f(maxK, true);

 return 0; }
