#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

 int main(){

	double x = 1, n; cin >> n;
	double sum = 0;

	for(int i = 0; i < n; i++){
        sum += 1 / (x * x);
        x += 2;
	}

	cout << sqrt(sum * 8);

 return 0; }
