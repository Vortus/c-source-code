#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

    double sumArr(double arr[], int l){
        double sum = 0;
        for(int i = 0; i < l; i++)
            sum += arr[i];
        return sum;
    }

using namespace std;

 int main(){

	double a[5] = { 1.1, 2.2, 3.3, 4.4, 5.5 };
	double b[5] = { 6.6, 0.1, 0.2, 0.3, 0.4 };
    double bstartSum = sumArr(b, 5);

    for(int i = 0; i < 5; i++){
        for(int j = i + 1; j < 5; j++){
            if(a[i] < b[i]){
                double tmp = a[i];
                a[i] = b[i];
                b[i] = tmp;
            }
        }
    }

    for(int i = 0; i < 5; i++)
        cout << b[i] << " ";

    cout << endl << bstartSum << " - " << sumArr(b, 5) << " = " << bstartSum - sumArr(b, 5);

 return 0; }
