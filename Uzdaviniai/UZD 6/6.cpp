#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    bool xyz(bool x, bool y, bool z){
        if(z && x) return true;
        else if(!x && y && !z) return true;
        return false;
    }

 int main(){

	for(int a = 0; a <= 1; a++)
        for(int b = 0; b <= 1; b++)
            for(int c = 0; c <= 1; c++)
                cout << a << " " << b << " " << c << " " << xyz(a, b, c) << endl;

 return 0; }
