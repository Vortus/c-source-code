
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

	double n, x = 1; cin >> n;
	double sum = 0;

	while(true){
	    double j = 1 / x;
	    if(j < n) break;
        sum += j;
        x *= 2;
	}

    cout << sum;

 return 0; }
