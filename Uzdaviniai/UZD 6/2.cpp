#include <cmath>
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int sumN(int i){
        int a = i / 1000;
        int b = i % 100;
        return a + b;
    }

    bool canRoot(int i){
        double r = sqrt(i);
        if((int)r * (int)r == i) return true;
        return false;
    }

 int main(){

	for(int i = 10000; i <= 99999; i++)
        if(canRoot(i) && canRoot(sumN(i))) cout << i << endl;

 return 0; }
