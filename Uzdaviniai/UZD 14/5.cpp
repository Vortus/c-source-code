#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int toDec(int n, int numeral){
        int res = 0, i = 1;
        while(n > 0){
            int j = n % 10;
            res += i * j;
            i *= numeral;
            n /= 10;
        }
        return res;
    }

    int reverseInt(int n){
        int res = 0;
        while(n > 0){
            int j = n % 10;
            res = res * 10 + j;
            n /= 10;
        }
        return res;
    }

 int main(){

    int n, j = 0; cin >> n;
    int i = 2;

    while(true){
        int a = reverseInt(n);
        int b = toDec(a, i);
        if(n == b) { cout << a << " b " << i << endl; return 0; }
        i++;
    }

 return 0; }
