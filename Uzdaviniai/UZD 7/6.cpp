#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    void sortAsc(int arr[], int l){
        for(int i = 0; i < l; i++){
            for(int j = i + 1; j < l; j++){
                if(arr[i] > arr[j]){
                    int tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
    }

    void sortDesc(int arr[], int l){
        for(int i = 0; i < l; i++){
            for(int j = i + 1; j < l; j++){
                if(arr[i] < arr[j]){
                    int tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
    }

    void coutArr(int arr[], int l){
        for(int i = 0; i < l; i++)
            cout << arr[i];
        cout << endl;
    }


 int main(){

	int arr[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1 };

	sortAsc(arr, 10);
	coutArr(arr, 10);

    sortDesc(arr, 10);
	coutArr(arr, 10);

 return 0; }
