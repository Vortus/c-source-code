#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int power(int n, int x){
        if(x == 0) return 1;
        if(x == 1) return n;
        return power(n, x - 1) * n;
    }

 int main(){

    int n, lp, ls, lv; cout << "SKaitmenu sk, liekanos: "; cin >> n >> lp >> ls >> lv;

    int startValue = power(10, n - 1);
    int endValue = 9; for(int i = 1; i < n; i++){ endValue *= 10; endValue += 9;  }

    int minK = power(10, n), c = 0;
    for(int i = startValue; i <= endValue; i++)
        if(i % 5 == lp && i % 7 == ls && i % 11 == lv){ c++; if(i < minK) minK = i; cout << i << endl; }

    cout << "KODU: " << c << " MAZIAUSIAS: " << minK;

 return 0; }
