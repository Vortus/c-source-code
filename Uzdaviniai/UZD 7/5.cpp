#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <ctime>
#include <sstream>

using namespace std;

    void difference(string arr[]){
        int nd, nm, ny;
        istringstream(arr[1]) >> nd;
        istringstream(arr[0]) >> nm;
        istringstream(arr[2]) >> ny;

        struct std::tm a = {0,0,0,1,0,101};
        struct std::tm b = {0,0,0, nd, nm - 1, 100 + ny % 100}; /* July 5, 2004 */

        time_t x = std::mktime(&a);
        time_t y = std::mktime(&b);

        if ( x != (std::time_t)(-1) && y != (std::time_t)(-1) )
        {
            double difference = std::difftime(y, x) / (60 * 60 * 24);
            cout << "Skirtumas: " << difference << " dienos, " << arr[3] << " valandos, " << arr[4] << " minutes, " <<
                arr[5] << " sekundes";
        }

    }

 int main(){

        time_t rawtime;
        struct tm * timeinfo;
        char buffer[80];
        time (&rawtime);
        timeinfo = localtime(&rawtime);
        strftime(buffer,80,"%d %m %Y %I %M %S",timeinfo);
        string nowDate(buffer);

        string arr[6];
        int i = 0;
        stringstream ssin(nowDate);

        while (ssin.good() && i < 6){
            ssin >> arr[i];
            ++i;
        }

        difference(arr);

 return 0; }
