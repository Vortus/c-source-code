#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>

using namespace std;

    int reverseInt(int n){
        string sInt, reversed;
        ostringstream c; c << n; sInt = c.str();
        for(int i = sInt.length() - 1; i >= 0; i--)
            reversed += sInt[i];
        istringstream(reversed) >> n;
        return n;
    }

    int reversInt(int n){
        int s = 0;
        while(n > 0){
            s = s * 10 + n % 10;
            n /= 10;
        }
        return s;
    }

 int main(){

	int m, n; cin >> m >> n;


	for(int i = m; i <= n; i++){
        int isq = i * i;
        int ireversed = reverseInt(i);
        int irsq = ireversed * ireversed;
        if(isq == reverseInt(irsq)) cout << i << endl;
	}

 return 0; }
