#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    int n; cout << "Iveskite varliu kieki ir po to ju periodus: "; cin >> n;
    int frogPeriod[n], frogTimer[n];
    for(int i = 0; i < n; i++){ cin >> frogPeriod[i]; frogTimer[i] = frogPeriod[i]; };

    while(true){
        int c = 0;

        for(int i = 1; i < n; i++){
            while(frogTimer[0] >= frogTimer[i]){
                if(frogTimer[0] == frogTimer[i]) c++;
                frogTimer[i] += frogPeriod[i];
            }
        }

        if(c == n - 1){ cout << frogTimer[0] << endl; break; }
        frogTimer[0] += frogPeriod[0];
    }

 return 0; }
