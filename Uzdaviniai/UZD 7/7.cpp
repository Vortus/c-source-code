#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string.h>

using namespace std;

    void coutArr(string arr[], int l){
        for(int i = 0; i < l; i++)
            cout << arr[i] << endl;
    }

    void copyValues(string to[], string from[], int l){
        for(int i = 0; i < l; i++)
            to[i] = from[i];
    }

    void sortAlph(string arr[], int l){
        for(int i = 0; i < l; i++){
            for(int j = i + 1; j < l; j++){
                if(arr[i] > arr[j]){
                    string tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
    }

 int main(){

	string a[5] ={
        "Petrauskas", "Jonauskas", "Agurkinis", "Skrajunas", "Pleksne"
	};

    sortAlph(a, 5);
    string b[5];
    copyValues(b, a, 5);
    coutArr(b, 5);

 return 0; }
