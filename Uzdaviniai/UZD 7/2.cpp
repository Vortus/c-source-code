#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

     int n, w = 1; cin >> n;

     while(n > 0){
        int i = 1;

        while(n - i * i >= 0){
            n -= i * i;
            i++;
        }
        cout << w << " piramide - " << (i - 1) * (i - 1) << " plokstes pagrinde" << endl; // -1 nes loopas prideda bereikalingai
        w++;
     }

 return 0; }
