#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

    bool intContains(int n, int what){
        while(n > 0){
            int j = n % 10;
            if(j == what) return true;
            n /= 10;
        }
        return false;
    }

using namespace std;

 int main(){

	int n; cin >> n;
	for(int i = 1; i <= 100; i++)
        if(intContains(i, n) && intContains(i * i, n) && intContains(i * i * i, n))
            cout << i << endl;

 return 0; }
