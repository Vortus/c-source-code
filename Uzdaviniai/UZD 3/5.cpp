
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int bdk(int a, int b){
        if(a < b){ int tmp = a; a = b; b = tmp; }
        int result = 0;
        for(int i = 1; i <= b; i++)
            if(a % i == 0 && b % i == 0) result++;
        return result;
    }

 int main(){

	int a, b;
	cin >> a >> b;

    cout << bdk(a, b);

 return 0; }
