
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int kelti(int a, int n){
        if(n == 1) return a;
        return kelti(a, n - 1) * a;
    }

 int main(){

	int a, n; cin >> a >> n;
	cout << kelti(a, n);

 return 0; }
