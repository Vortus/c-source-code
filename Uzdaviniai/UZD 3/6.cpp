#include <cmath>
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    bool numbersSumEven(int a){
        int tmp = a, c = 0;
        while(tmp > 0){
            int j = tmp % 10;
            c += j;
            tmp /= 10;
        }
        if(c % 2 == 0) return true;
        return false;
    }

    bool isPrime(int a){
        if(a == 1) return false;
        for(int i = 2; i <= sqrt(a); i++)
            if(a % i == 0) return false;
        return true;
    }

 int main(){

    int a, b, c = 0; cin >> a >> b;

    for(int i = a; i <= b; i++)
        if(isPrime(i) && numbersSumEven(i)){ cout << i << endl; c++; }

    cout << c;

 return 0; }
