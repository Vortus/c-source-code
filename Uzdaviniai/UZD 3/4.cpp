
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

	double a = 1, b = 2;
	int i = 0, n; cin >> n;
	bool addup = true;
    double sum = 0;

	while(i < n){
	    if(addup) sum += a / b;
        else sum -= a / b;
  	    if((i + 1) % 2 == 0) addup = !addup;

        cout << sum << endl;
        a++; b++;
        i++;
	}

    cout << sum;

 return 0; }
