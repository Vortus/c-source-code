
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    int numberSumup(int i){
        int sum = 0, tmp = i;

        while(tmp > 0){
            int j = tmp % 10;
            sum += j;
            tmp /= 10;
        }
        return sum;
    }

    int numberMultiply(int i){
        int mult = 1, tmp = i;

        while(tmp > 0){
            int j = tmp % 10;
            mult *= j;
            tmp /= 10;
        }
        return mult;
    }

 int main(){

	int sum, mult; cin >> sum >> mult;
	int i = 1;

	while(true){
        if(numberSumup(i) == sum && numberMultiply(i) == mult) break;
        i++;
	}

	cout << i;

 return 0; }
