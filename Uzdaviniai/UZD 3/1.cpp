
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

	int n; cin >> n;
	int sum = 0, i = 0, sk = 1;

	while(i < n){
        if(sk % 2 != 0){ i++; sum += sk * sk * sk; }
        sk++;
	}

    cout << sum;

 return 0; }
