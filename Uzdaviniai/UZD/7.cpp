#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;


 int main(){

    int n, c = 0; cin >> n;

    for(int i = n; i > 1; i--){
        int a = 2;
        int tempI = i;
        while(a * a <= tempI){
            if(tempI % a == 0){ c++; tempI /= a; }
            else a++;
        }
        if(tempI > 1) c++;
    }

    cout << c;

 return 0; }
