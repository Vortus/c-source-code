
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

    int n; cin >> n;
	int i = 2;
	while(i * i <= n)
        if(n % i == 0){
            cout << i << "x";
            n /= i;
        }else i++;

    if(n > 1) cout << n;

 return 0; }
