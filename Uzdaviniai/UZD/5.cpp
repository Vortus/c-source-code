#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int dbd(int a, int b){
        if(b == 0) return a;
        return dbd(b, a % b);
    }

 int main(){

    int a, b; cin >> a >> b;

    if(dbd(a ,b) == 1) cout << "TAIP";
    else cout << "NE";

 return 0; }
