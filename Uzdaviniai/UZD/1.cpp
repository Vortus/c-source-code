#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
using namespace std;

 int main(){

    double s, n; cin >> s >> n; // jei s [1, 9]
    int nmbr = s, sum = 0;
    for(int i = 1; i <= n; i++){
        sum += nmbr;
        double power = pow(10, i) * n;
        nmbr += power;
    }

    cout << sum;

 return 0; }
