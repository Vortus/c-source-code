#include <cmath>
#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    bool prime(int n){
        if(n == 1) return false;
        for(int i = 2; i <= sqrt(n); i++)
            if(n % i == 0) return false;
        return true;
    }

 int main(){

	int n; cin >> n;
	for(int i = 2; i <= n; i++){
        for(int j = 2; j <= n; j++){
            if(i * j > n) break;
            if(prime(i) && prime(j) && i * j == n){ cout << "TAIP " << i << "x" << j; return 0; }
        }
	}

	cout << "NE";

 return 0; }
