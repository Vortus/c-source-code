#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int dbd(int x, int y){
        if(y == 0) return x;
        return dbd(y, x % y);
    }

    int mbk(int x, int y){
        return x * y / dbd(x, y);
    }

    int didz(int a, int b){
        if(a > b) return a;
        return b;
    }

 int main(){

    int d, k, did; cin >> d >> k; did = didz(d, k);

    for(int x = 1; x <= did; x++){
        for(int y = 1; y <= did; y++){
            if(dbd(x, y) == d && mbk(x, y) == k) cout << x << " " << y << endl;
        }
    }

 return 0; }
