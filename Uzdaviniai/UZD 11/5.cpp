#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    struct romanNumber {
        int v;
        string s;
    };

 int main(){

    int k = 13, n; cin >> n;
	romanNumber arr[] = { { 1, "I"}, { 4, "IV"},  { 5, "V"}, { 9, "IX"}, { 10, "X" }, { 40, "XL"},  { 50, "L"},
                            { 90, "XC"}, {100, "C"}, {400, "CD"}, {500, "D"}, {900, "CM"}, {1000, "M"}};

    while(n > 0){
        int nmb = arr[k - 1].v;
        if(nmb > n) k--;
        else { n -= nmb; cout << arr[k - 1].s; }
    }

 return 0; }
