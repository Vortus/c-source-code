#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

	ifstream fi("4in.txt");
    int n, m; fi >> n >> m;
    int arr[n * m], newArr[n * m];
    for(int i = 0; i < n * m; i++){ fi >> arr[i]; newArr[i] = 0; }
	fi.close();

	for(int y = 0; y < n; y++){
        for(int x = 0; x < m; x++){

            int nmbr = arr[x + y * m];
            int biggerCount = 0; int d = 4;
            if(x == 0 || x == m - 1) d--;
            if(y == 0 || y == n - 1) d--;

            if(x != 0 && nmbr > arr[(x - 1) + y * m]) biggerCount++;
            if(x != m - 1 && nmbr > arr[(x + 1) + y * m]) biggerCount++;
            if(y != 0 && nmbr > arr[x + (y - 1) * m]) biggerCount++;
            if(y != n - 1 && nmbr > arr[x + (y + 1) * m]) biggerCount++;

            newArr[x + y * m] = biggerCount == d ? 1 : 0;
         }
	}

	for(int i = 0; i < n * m; i++){
        if(i != 0 && i % m == 0) cout << endl;
        cout << newArr[i] << " ";
	}

 return 0; }
