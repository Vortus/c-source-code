#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    bool equalcube(int n){
        int sum = 0;
        int tmp = n;
        while(tmp > 0){
            int j = tmp % 10;
            sum += j * j * j;
            tmp /= 10;
        }
        if(sum == n) return true;
        return false;
    }

 int main(){

	int i = 1;
    while(i < 9*9*9*4){
        if(equalcube(i)) cout << i << endl;
        i++;
    }

 return 0; }
