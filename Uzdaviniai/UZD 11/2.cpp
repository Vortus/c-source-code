#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    bool sameNumbers(int n){
        int j = n % 10; n /= 10;
        while(n > 0){
            int a = n % 10;
            if(j != a) return false;
            j = a;
            n /= 10;
        }
        return true;
    }

    int sum(int n){
        int r = 0;
        for(int i = 1; r < n; i++)
            r += i;
        return r;
    }

 int main(){

    for(int i = 1; i < 10000; i++)
        if(sameNumbers(i) && sum(i) == i) cout << i << endl;

 return 0; }
