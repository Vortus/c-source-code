#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

	int n, v, k; cin >> n >> v >> k;
	double indai[n]; indai[0] = v; for(int i = 1; i < n; i++) indai[i] = 0;

	for(int i = 0; i < n; i++){
        double l = indai[i];
        for(int j = i + 1; j < n; j++)
            indai[j] += l / (n - i - 1);
        if(i != n - 1)indai[i] = 0;
 	}

 	for(int i = 0; i < n; i++)
        cout << indai[i] << " ";

 return 0; }
