#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    string newseq(string s){
        string result;
        for(int i = 0; i < s.length(); i++){
            int sum = 0;
            for(int j = 0; j <= i; j++)
                sum += s[j] - '0'; // char to int
            result += ((sum % 2) + '0');
        }
        return result;
    }

 int main(){

	string seq; cin >> seq;
    cout << newseq(seq);

 return 0; }
