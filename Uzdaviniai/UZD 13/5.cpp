#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

    int men[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    int valymai[4]; for(int i = 0; i < 4; i++) valymai[i] = 0;
    int valo = 0, d = 6, m = 0, vd = 1;

    while(m < 12 && vd <= men[11]){ // kol baigias metai
        if(vd / men[m] >= 1){ vd %= men[m]; m++; }
        else vd++;
        valo++; valo %= 4; // keiciasi valytojai
        if(d == 7){ valymai[valo]++; } // jei sekmadienis
        d++; d %= 8; if(d == 0) d++; // savaites dienos
     }

    for(int i = 0; i < 4; i++)
        cout << valymai[i] << " ";

 return 0; }
