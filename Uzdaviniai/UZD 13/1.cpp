#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

 int main(){

	double a, b, c, ka, kb; cin >> a >> b;
    c = sqrt(a * a + b * b);
    ka = atan(a / b) * 180 / M_PI;
    kb = atan(b / a) * 180 / M_PI;

    cout << a << " " << b << " " << c << endl;
    cout << ka << " " << kb;

 return 0; }
