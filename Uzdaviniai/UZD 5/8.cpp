#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    double rec(int a, int n){
        if(a == n + 1) return 1;
        if(a % 2 != 0) return 1 + 1 / rec(a + 1, n);
        return 3 - 1 / rec(a + 1, n);
    }

 int main(){

    int n; cin >> n;
    cout << rec(1, n);

 return 0; }
