#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

using namespace std;

    string reversed(string s){
        if(s.length() <= 1) return s;
        return reversed(s.substr(1)) + s[0];
    }

 int main(){

    string s; getline(cin, s);
    cout << reversed(s);

 return 0; }
