#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    int n; cout << "Iveskite masyvo dydi ir pati masyva: "; cin >> n;
    double posCount = 0;
    int arr[n]; for(int i = 0; i < n; i++){ cin >> arr[i]; if(arr[i] > 0) posCount++; }

    cout << "Teigiamu: " << fixed << setprecision(2) << posCount / n * 100 << "%";

 return 0; }
