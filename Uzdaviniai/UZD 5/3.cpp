#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int findCubeSqrt(int n){
        int i = 1;

        while(i * i * i <= n)
            i++;

        return i - 1;
    }

 int main(){

    int n; cin >> n;
    int cube = findCubeSqrt(n);
    cout << n << " = " << cube << "^3 + " << n - cube * cube * cube;

 return 0; }
