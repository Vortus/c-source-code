#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    int n; cin >> n;
    int res = 1;

    for(int i = n; i <= n * 2; i++)
        res *= i;

    cout << res;

 return 0; }
