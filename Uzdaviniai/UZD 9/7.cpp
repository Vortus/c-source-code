#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

	int m = 5, n = 4;
	int arr[] = {       5, 2, 3, 4, 1,
                        2, 4, 3, 4, 2,
                        1, 7, 2, 6, 2,
                        1, 2, 1, 2, 3 };

    cout << "---------------------------------------------" << endl;
    cout << setw(10);
    for(int i = 1; i <= m; i++)
        cout << i << setw(5); cout << setw(10) << "suma" << endl;

    int sum = 0, allsum = 0;
    for(int y = 0; y < n; y++){
        cout << setw(5) << y + 1;

        for(int x = 0; x < m; x++){
            sum += arr[x + y * m];
            cout << setw(5) << arr[x + y * m];
        }

        cout << setw(8) << sum << endl;
        allsum += sum;
        sum = 0;
    }

    sum = 0;
    cout << setw(5) << "suma";
    for(int x = 0; x < m; x++){
        for(int y = 0; y < n; y++)
            sum += arr[x + y * m];
        cout << setw(5) << sum;
        sum = 0;
    }

    cout << setw(8) << allsum << endl;
    cout << "---------------------------------------------" << endl;

 return 0; }
