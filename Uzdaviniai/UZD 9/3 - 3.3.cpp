#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

	double n, p; cin >> n >> p; p /= 100;
	int i = 0; double s = n;

    cout << "- - - - - - - - - - " << endl;

	while(s / n < 2){
        cout << i << " metai " << s << fixed << setprecision(2) << endl;
        s += s * p;
        i++;
	}

	cout << "- - - - - - - - - - " << endl << "po " << i << " metu bus " << fixed << setprecision(2) << s << " pinigu" << endl;

 return 0; }
