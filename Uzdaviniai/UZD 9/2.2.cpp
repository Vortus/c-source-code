#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

using namespace std;

 int main(){

    int p, n; cin >> p >> n;
    int allsum = 0;
    vector<int> atl;
    atl.push_back(p);
    atl.push_back(p + 2);
    atl.push_back(p + 1);
    if(n >= 1) allsum += atl.at(0);
    if(n >= 2) allsum += atl.at(1);
    if(n >= 3) allsum += atl.at(2);

	for(int i = 4; i <= n; i++){
	    int j = atl.at(i - 3);
	    j += i % 2 == 0 ? 2 : 1;
        atl.push_back(j);
        allsum += j;
	}

    cout << "visa suma " << allsum;

 return 0; }
