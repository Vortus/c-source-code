#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

 int main(){

    double arr[10]; for(int i = 0; i < 10; i++) cin >> arr[i];
    short minI = 0, maxI = 0;
    double minV = arr[0], maxV = arr[0];

    for(int i = 0; i < 10; i++){
        if(arr[i] >= maxV){ maxV = arr[i]; maxI = i; }
        else if(arr[i] <= minV){ minV = arr[i]; minI = i;}
    }

    double average = 0;

    for(int i = 0; i < 10; i++)
        if(i != minI && i != maxI) average += arr[i];

    cout << fixed << setprecision(1) << average / 8;

 return 0; }
