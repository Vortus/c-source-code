#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

using namespace std;

 int main(){

    int p, n; cin >> p >> n;

    vector<int> atl;
    atl.push_back(p);
    atl.push_back(p + 2);
    atl.push_back(p + 1);
    if(n >= 1) cout << "1d " << atl.at(0) << endl;
    if(n >= 2) cout << "2d " << atl.at(1) << endl;
    if(n >= 3) cout << "3d " << atl.at(2) << endl;

	for(int i = 4; i <= n; i++){
	    int j = atl.at(i - 3);
	    j += i % 2 == 0 ? 2 : 1;
        atl.push_back(j);
        cout << i << "d " << j << endl;
	}

 return 0; }
