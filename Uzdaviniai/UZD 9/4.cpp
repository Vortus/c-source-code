#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int charToInt(char c){
        return c - '0';
    }

    char intToChar(int i){
        return i + '0';
    }

    string addup(string a, string b){
        int remainder = 0, diff = a.length() - b.length();
        for(int i = a.length() - 1; i >= 0; i--){
            int sum = 0;
            if(charToInt(b[i - diff]) != -49) sum = charToInt(a[i]) + charToInt(b[i - diff]) + remainder;
            else sum = charToInt(a[i]) + remainder;
            if(sum / 10 >= 1) remainder = 1;
            else remainder = 0;
            sum %= 10;
            a[i] = intToChar(sum);
        }
        return a;
    }

 int main(){

    string a, b; cin >> a >> b;
    if(a.length() < b.length()){ string tmp = a; a = b; b = tmp; }
    cout << endl << left << a
        << endl << b << endl << "------------"  << endl << addup(a, b);

 return 0; }
