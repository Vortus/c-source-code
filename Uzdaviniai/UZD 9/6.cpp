#include <io.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

    string sToLower(string s){
        for(int i = 0; i < s.length(); i++)
            s[i] = tolower(s[i]);
        return s;
    }

 int main(){

    vector<string> words;
	ifstream fi("duomenys.dat");

    while(!fi.eof()){ // nuskaito zodzius
        string s; fi >> s;
        words.push_back(s);
    }
	fi.close();

    for(int i = 0; i < words.size(); i++){
        for(int j = i + 1; j < words.size(); j++){
            if(sToLower(words.at(i)) > sToLower(words.at(j))){
                string tmp = words.at(i);
                words.at(i) = words.at(j);
                words.at(j) = tmp;
            }
        }
    }

    ofstream fo("rezultatai.dat");
    for(int i = 1; i < words.size(); i++)
        fo << words.at(i) << endl;
    fo.close();

 return 0; }
