#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

	double n, p1, p3, p6, p9, d;
	cin >> n >> p1 >> p3 >> p6 >> p9 >> d;

    double k = d / 30;
    if(k <= 3) cout << n * p1 / 100;
    else if(k > 3 && k <= 6) cout << n * p3 / 100;
    else if(k > 6 && k <= 9) cout << n * p6 / 100;
    else cout << n * p9 / 100;

 return 0; }
