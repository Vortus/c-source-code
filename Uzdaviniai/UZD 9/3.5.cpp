#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

 int main(){

	double k, p, m; cin >> k >> p >> m; p /= 100;
    double tmp = k;

    for(int i = 0; i < m; i++)
        k -= k * p;

    cout << "kainavo " << tmp << " dabar kainuoja " << fixed << setprecision(2) << k;

 return 0; }
