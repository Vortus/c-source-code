#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    struct KeywordLetter{
        char c;
        int bigger = 0;
    };

    void coutRow(string s, int x, int h, int w){
         for(int i = 0; i < h; i++)
            if((int)s[x + i * w] > -1) cout << s[x + i * w];
    }

    string sToLower(string s){
        for(int i = 0; i < s.length(); i++)
            s[i] = tolower(s[i]);
        return s;
    }

 int main(){

    //////// nuskaitymas is failo
    ifstream fi("6in.txt");
    string keyword; fi >> keyword; keyword = sToLower(keyword);
    int w = keyword.length();
    KeywordLetter arrK[w]; for(int i = 0; i < w; i++) arrK[i].c = keyword[i];
    string message;

    while(!fi.eof()){
        char c = fi.get();
        if(c != '\n') message += c;
    }
    fi.close();
    int h = message.length() / w;
    ////////

    //sunumeravimas
    for(int i = 0; i < w; i++){
        for(int j = i + 1; j < w; j++){
            if(arrK[i].c > arrK[j].c) arrK[i].bigger++;
            else arrK[j].bigger++;
        }
        arrK[i].bigger++;
    }
    /////////

    for(int i = 0; i < w; i++)
        for(int j = 0; j < w; j++)
            if(arrK[j].bigger == i + 1){ coutRow(message, j, h, w); break; }

 return 0; }
