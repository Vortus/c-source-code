#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    struct Box{
        double a, b, c;
    };

    double maxHeight(Box box){
        if(box.a > box.b && box.a > box.c) return box.a;
        else if(box.b > box.a && box.b > box.c) return box.b;
        return box.c;
    }

 int main(){

    double t, h, n; cin >> t >> h >> n; Box arr[n];
    for(int i = 0; i < n; i++)
        cin >> arr[i].a >> arr[i].b >> arr[i].c;
    t -= h; // trukstams aukstis

    for(int i = 0; i < n; i++)
        if(t <= maxHeight(arr[i])) cout << i + 1 << " deze" << endl;

 return 0; }
