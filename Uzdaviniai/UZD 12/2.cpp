#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    void sortArrAscending(int arr[], int l){
        for(int i = 0; i < l; i++){
            for(int j = i + 1; j < l; j++){
                if(arr[i] > arr[j]){
                    int tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
    }

    bool repeats(int arr[], int l, int index, int ignoreIndex){
        for(int i = 0; i < l; i++)
            if(i != ignoreIndex && arr[i] == arr[index]) return true;
        return false;
    }

 int main(){

    int arr[5]; for(int i = 0; i < 5; i++) cin >> arr[i];
    sortArrAscending(arr, 5);

    if(repeats(arr, 5, 2, 2)) cout << "nera skaiciaus";
    else cout << arr[2];

 return 0; }
