#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    string valToBin(int n){
        string r;
        while(n > 0){
            int j = n % 2; char c = j + '0';
            r = c + r;
            n /= 2;
        }
        return r;
    }

    string octToBin(string n){
        string r;
        for(int i = 0; i < n.length(); i++)
            r += valToBin(n[i] - '0');
        return r;
    }

    string valToBinPadding(int n, int l){
        string r;
        for(int i = l - 1; i >= 0; i--){
            char c = ((n >> i) % 2) + '0';
            r += c;
        }
        return r;
    }

    string hexToBin(string n){
        string r;
        for(int i = 0; i < n.length(); i++){
            int j = n[i] - '0'; if(j > 9) j -= 7;
            r += valToBinPadding(j, 4);
        }
        return r;
    }

 int main(){

    string s; cin >> s; cout << "oct: " << octToBin(s) << endl;
    cin >> s; cout << "hex: " << hexToBin(s);

 return 0; }
