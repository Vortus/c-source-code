#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    void coutA(string arr[], int l){
        int times = 1;
        for(int i = 0; i < l; i++){
            int j = 0;
            while(j < times) { cout << arr[i]; j++; }
            times++;
        }
    }

    void coutB(string arr[], int l){
        int times = 1;
        for(int i = l - 1; i >= 0; i--){
            int j = 0;
            while(j < times) { cout << arr[i]; j++; }
            times++;
        }
    }

    void coutC(string arr[], int l){
        int startIndex = 0;
        while(startIndex < l){
            for(int i = startIndex; i < l; i++)
                cout << arr[i];
            startIndex++;
        }
    }

 int main(){

    string abcArr[26] = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l",
                            "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };

    coutA(abcArr, 26); cout << endl << endl;
    coutB(abcArr, 26); cout << endl << endl;
    coutC(abcArr, 26);

 return 0; }
