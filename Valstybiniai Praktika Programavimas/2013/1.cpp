#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
using namespace std;

    //globalus kintamieji
    string *names; int deliveryCount, limit;
    int *xs, *ys;

    //funkcija keliones atstumui skaiciuot
    int distanceTraveled(int x, int y){
       return (abs(x) + abs(y)) * 2;
    }

    //failo nuskaitymo funkcija
    void readFile(){
        ifstream fi("U1.txt");
        fi >> deliveryCount >> limit;

        names = new string[deliveryCount];
        xs = new int[deliveryCount];
        ys = new int[deliveryCount];

        for(int i = 0; i < deliveryCount; i++)
            fi >> names[i] >> xs[i] >> ys[i];

        fi.close();
    }

    //isvedimo i faila funkcija
    void outputFile(){

        ofstream fo("U1rez.txt");
        int tempSum = 0, index = 0;

        for(int i = 0; i < deliveryCount; i++){
            int traveled = distanceTraveled(xs[i], ys[i]);
            if(tempSum + traveled >= limit) break;
            tempSum += traveled;
            index = i + 1;
        }

        fo << index << " " << tempSum << " " << names[index - 1];
        fo.close();
    }

 int main(){

    readFile();
    outputFile();

 return 0; }
