#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int indexes[100], in[100], out[100];

    //////Ivestis
    int l;
    void readFile(){
        for(int i = 0; i < 100; i++){ indexes[i] = 0; in[i] = 0; out[i] = 0; }
        ifstream fi("U1.txt"); fi >> l;
        for(int i = 0; i < l; i++){
            int a, b; fi >> a >> b;
            indexes[i] = a;
            if(b > 0) in[i] += b;
            else out[i] += b;
        }
        fi.close();
    }
    //////

    void sortAscending(){
        for(int i = 0; i < l; i++)
            for(int j = i + 1; j < l; j++)
            if(indexes[i] > indexes[j]){
                int itmp = indexes[i], intmp = in[i], outtmp = out[i];
                indexes[i] = indexes[j];
                indexes[j] = itmp;
                in[i] = in[j];
                in[j] = intmp;
                out[i] = out[j];
                out[j] = outtmp;
            }
    }

    void fuse(){
        for(int i = 0; i < l; i++){
            for(int j = i + 1; j < l; j++){
                if(indexes[i] == indexes[j]){
                    in[i] += in[j];
                    out[i] += out[j];
                    indexes[j] = -1;
                }
            }
        }
    }

    void returnAscending(){
        sortAscending();
        ofstream fo("U1rez.txt");
        for(int i = 0; i < l; i++)
            if(indexes[i] != -1) fo <<  setw(5) << indexes[i];
        fo << endl;
        fo.close();
    }

    int maximum(){
        int maxs = 0, index = 0;
        for(int i = 0; i < l; i++)
            if(indexes[i] != -1 && in[i] > maxs){ index = i; maxs = in[i]; }
        return indexes[index];
    }

 int main(){

    readFile();
    fuse();
    returnAscending();
    ofstream fo("U1rez.txt", ios::app);
    for(int i = 0; i < l; i++)
        if(indexes[i] != -1) fo <<setw(5) << in[i];
    fo << endl;
    for(int i = 0; i < l; i++)
        if(indexes[i] != -1) fo << setw(5) << out[i];
    fo << endl << setw(5) << maximum();

    fo.close();

 return 0; }
