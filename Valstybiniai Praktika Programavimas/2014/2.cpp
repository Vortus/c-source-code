#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    //globalus kintamieji
    int endX = 0, endY = 0, startX = 0, startY = 0;

    //papildoma funkcija masyvu tvarkymui
    void refreshArr(int arr[], int n){
        for(int i = 0; i < n; i++)
            arr[i] = -1;
    }

    //isvedimas pertvarkyto sekos masyvo i faila
    void writeSequenceToFile(int arr[], int n){
        ofstream fo("U2rez.txt", std::ofstream::app);
        int j = 0;

        for(int i = 0; i < n; i++){
          if(arr[i] == -1) break;
          j++;
          fo << arr[i] << " ";
        }

        fo << j << endl;
        fo.close();
    }

    //aparato judejimo logika
    void moving(int arr[], int n){
        ofstream fo("U2rez.txt", std::ofstream::app);
        int r[n]; refreshArr(r, n);
        int tempX = startX, tempY = startY;

        for(int i = 0; i < n; i++){
            if(tempX == endX && tempY == endY) break;
            if(arr[i] == 1) tempY++;
            else if(arr[i] == 2) tempX++;
            else if(arr[i] == 3) tempY--;
            else if(arr[i] == 4) tempX--;
            r[i] = arr[i];
        }

         if(tempX != endX && tempY != endY)
            fo << "sekos pabaiga ";
            else fo << "pasiektas tikslas ";

        fo.close();
        writeSequenceToFile(r, n);
     }

 int main(){
    //pravalom faila nes appendinu
    ofstream fo("U2rez.txt", std::ofstream::trunc);
    fo.close();

    ifstream fi("U2.txt");
    // nuskaitom pradinius duomenis
    fi >> startX >> startY >> endX >> endY;
    // komandu seku skaiciaus kintamasis
    int seqCount; fi >> seqCount;

    for(int i = 0; i < seqCount; i++){
        //sekos ilgis
        int seqLength; fi >> seqLength;
        //sekos masyvas
        int seq[seqLength];
        //uzpildomas sekos masyvas
        for(int i = 0; i < seqLength; i++)
            fi >> seq[i];
        //judinam aparata
        moving(seq, seqLength);
    }

    fi.close();

 return 0; }
