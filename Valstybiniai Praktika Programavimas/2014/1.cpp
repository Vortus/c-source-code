#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    //grazina maximalu indexa tarp triju sk.
    int maximum(int a, int b, int c){
        if(a >= b && a >= c) return 0;
        if(b >= a && b >= c) return 1;
        if(c >= a && c >= a) return 2;
    }

    //laimetojo apskaiciavimas
    int won(int a, int b, int c){
        return maximum(a, b, c) + 1;
    }

 int main(){

    //lokalus kintamieji
    int firstVotes = 0, secondVotes = 0, thirdVotes = 0, n;
    int firstPoints = 0, secondPoints = 0, thirdPoints = 0;
    int firstDirP = 0, secondDirP = 0, thirdDirP = 0;


	ifstream fi("U1.txt");
    fi >> n;

    for(int i = 0; i < n; i++){
        //tasku apskaiciavimas viename skyriuje
        int arr[3]; fi >> arr[0] >> arr[1] >> arr[2];
        int maxIndex = maximum(arr[0], arr[1], arr[2]);

        //tikrinama ar yra tokiu pat maksimaliu skaiciu ir sudedami taskai kur reikia
        if(maxIndex == 0){
            if(arr[0] == arr[1]){ firstPoints += 2; secondPoints += 2; }
            else if(arr[0] == arr[2]){ firstPoints += 2; thirdPoints += 2; }
            if(arr[2] != arr[1] && arr[1] != arr[0]) firstPoints += 4;

        }else if(maxIndex == 1){
            if(arr[1] == arr[0]){ secondPoints += 2; firstPoints += 2; }
            else if(arr[1] == arr[2]){ secondPoints += 2; thirdPoints += 2; }
            if(arr[2] != arr[1] && arr[1] != arr[0]) secondPoints += 4;
        }else{
            if(arr[2] == arr[1]){ thirdPoints += 2; secondPoints += 2; }
            else if(arr[2] == arr[0]){ thirdPoints += 2; firstPoints += 2; }
            if(arr[2] != arr[1] && arr[1] != arr[0]) thirdPoints += 4;
        }

        //sudedami visi balsai
        firstVotes += arr[0]; secondVotes += arr[1]; thirdVotes += arr[2];
        //uzbaigiamas tasku apskaiciavimas viename skyriuje
    }

    //nuskaitomi dir taskai
    fi >> firstDirP >> secondDirP >> thirdDirP;
	fi.close();

    ofstream fo("U1rez.txt");

        //jeigu reikia direktoriaus pagalbos
        if(firstPoints == secondPoints || secondPoints == thirdPoints){
        int maxIndex = maximum(firstPoints, secondPoints, thirdPoints);
        int arr[3]; arr[0] = firstPoints; arr[1] = secondPoints; arr[2] = thirdPoints;
        if(maxIndex == 0){
            if(arr[0] == arr[1] || arr[0] == arr[2]){
              firstPoints += firstDirP;
              secondPoints += secondDirP;
              thirdPoints += thirdDirP;
            }
        }else if(maxIndex == 1){
              if(arr[1] == arr[0] || arr[1] == arr[2]){
              firstPoints += firstDirP;
              secondPoints += secondDirP;
              thirdPoints += thirdDirP;
            }
        }else{
              if(arr[2] == arr[1] || arr[2] == arr[0]){
              firstPoints += firstDirP;
              secondPoints += secondDirP;
              thirdPoints += thirdDirP;
            }
        }
     }

    //isvedimas atsakymu i faila
    fo << firstVotes << " " << secondVotes << " " << thirdVotes << endl;
    fo << firstPoints << " " << secondPoints << " " << thirdPoints << endl;

    fo << won(firstPoints, secondPoints, thirdPoints);
    fo.close();

return 0; }
