#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    /*
    As visiskai ignoravau fakta tai jog zmones kartojasi
    nes bereikalingi skaiciavimai.
    */

    struct Dancers{
        string name;
        int score;
    } allDancers[100];

    int n, k;

    int countScore(int arr[]){ // balu rinkiniui
        int maxs = 0, maxI = 0, mins = arr[0], minI = 0;
        for(int i = 0; i < k; i++)
            if(arr[i] >= maxs){ maxs = arr[i]; maxI = i; }
            else if(arr[i] <= mins){ mins = arr[i]; minI = i; }
        int result = 0;
        for(int i = 0; i < k; i++)
            if(i != maxI && i != minI) result += arr[i];
        return result;
    }

    void sort(){ // surikiavimas
        for(int i = 0; i < n; i++){
            for(int j = i + 1; j < n; j++){
                if(allDancers[i].score < allDancers[j].score){
                    Dancers tmp = allDancers[i];
                    allDancers[i] = allDancers[j];
                    allDancers[j] = tmp;
                }
            }
        }
    }

    void readFile(string s){ // failo nuskaitimas
        ifstream fi(s); fi >> n >> k;
        for(int i = 0; i < n; i++){
            fi.ignore();
            string s; getline(fi, s);
            int tmpArr[k];
            for(int c = 0; c < k; c++)
                fi >> tmpArr[c];
            int a = countScore(tmpArr); // technika
            for(int c = 0; c < k; c++)
                fi >> tmpArr[c];
            int b = countScore(tmpArr); //artist

            allDancers[i].name = s;
            allDancers[i].score = a + b;
        }
        fi.close();

    }

 int main(){

    readFile("U2.txt");
    sort(); // surikiuoja
    ofstream fo("U2rez.txt");
    for(int i = 0; i < n; i++)
        if (allDancers[i].name.length() > 0) fo << allDancers[i].name << " " << allDancers[i].score << endl;

    fo.close();

 return 0; }
