#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int gender[100], side[100], sizeP[100];
    int n;

    void readFile(string s){ // failo nuskaitymas
        ifstream fi(s);
        fi >> n;
        for(int i = 0; i < n; i++){
           fi >> gender[i] >> side[i] >> sizeP[i]; }

        fi.close();
    }

    void countPairs(int &a, int &b){ // vyru moteru poros
        for(int i = 0; i < n; i++){
            for(int j = i + 1; j < n; j++){
                if(gender[i] == gender[j] &&
                   side[i] != side[j] &&
                   sizeP[i] == sizeP[j]){
                    if(gender[i] == 4) b++;
                    else a++;
                    gender[i] = -1; gender[j] = -1;
                   }
            }
        }
    }

    void countLeftovers(int &a, int &b){ // likusios pirstines
        a = 0; b = 0;
        for(int i = 0; i < n; i++)
        if(gender[i] != -1){
                if(gender[i] == 4) b++; else a++; }
    }

 int main(){

    readFile("U1.txt");
    int a = 0, b = 0;
    countPairs(a, b);
    ofstream fo("U1rez.txt");
    fo << b << endl << a << endl;
    countLeftovers(a, b);
    fo << b << endl << a;
    fo.close();

 return 0; }
