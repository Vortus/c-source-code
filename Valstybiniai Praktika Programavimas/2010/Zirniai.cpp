#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int bqueue[1000];
    int onekgb, twokgb, buyerc;
    int onesold = 0, twosold = 0, lastBought = 0;

    void readFile(){
        ifstream fi("Z1.txt");
        fi >> onekgb >> twokgb >> buyerc;
        for(int i = 0; i < buyerc; i++)
            fi >> bqueue[i];
        fi.close();
    }

    bool buying(int n, int &boughtkg){
        int a = 0, b = 0;
        while(twosold < twokgb && n > 1){ // kiek dvejetu galima
            a++; twosold++;
            n -= 2;
        }
        while(onesold < onekgb && n > 0){ // kiek vienetu
            b++; onesold++;
            n--;
        }
        boughtkg = a * 2 + b;
        return a > 0 || b > 0 ? true : false;
    }

 int main(){

    readFile();

    int buyers = 0;
    for(int i = 0; i < buyerc; i++){
        int j; // grazina kiek nusipirko
        if(buying(bqueue[i], j)){ buyers++; lastBought = j; } // jei nusipirko
    }

    ofstream fo("Z1rez.txt");
    fo << onesold << " " << twosold << endl;
    fo << buyers << endl;
    fo << lastBought;
    fo.close();

 return 0; }
