#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    int figures[6] = { 0, 0, 0, 0, 0, 0 };

    bool noAvailableFigures(){
        for(int i = 0; i < 6; i++)
            if(figures[i] <= 0) return true;
        return false;
    }

    int getResult(){
        int k = 0;
        while(!noAvailableFigures()){
            figures[0] -= 8;
            figures[1] -= 2;
            figures[2] -= 2;
            figures[3] -= 2;
            figures[4] -= 1;
            figures[5] -= 1;
            k++;
        }
        return k;
    }

 int main(){

    ifstream fi("U1.txt");
    int n; fi >> n;
    for(int i = 0; i < n; i++){
        for(int j = 0; j < 6; j++){
            int a; fi >> a;
            figures[j] += a;
        }
    }
    fi.close();

    ofstream fo("U1rez.txt");
    fo << getResult();
    fo.close();

 return 0; }
