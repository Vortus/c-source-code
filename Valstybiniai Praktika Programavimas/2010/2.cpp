#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    struct Dish{
        string name;
        int cost = 0;
    };

    int dishes, parts;
    int partsCost[12];

    int dishCost(int arr[]){ // vieno patiekalo kaina
        int sum = 0;
        for(int i = 0; i < parts; i++)
            sum += arr[i] * partsCost[i];
        return sum;
    }

    void allCost(Dish arr[], ofstream& fo){ // viso kaina
        int sum = 0;
        for(int i = 0; i < dishes; i++)
            sum += arr[i].cost;
        fo << sum / 100 << " " << sum % 100;
    }

 int main(){

    ifstream fi("U2.txt");
    ofstream fo("U2rez.txt");
    fi >> parts >> dishes;
    Dish dishesArr[dishes];
    for(int i = 0; i < parts; i++) fi >> partsCost[i];

    for(int i = 0; i < dishes; i++){
        fi.ignore();
        char s[15]; fi.get(s, 15);
        int sum = 0; int tmpArr[parts];
        for(int j = 0; j < parts; j++)
            fi >> tmpArr[j];
        dishesArr[i].name = s;
        dishesArr[i].cost = dishCost(tmpArr);
        fo << s << dishesArr[i].cost << endl;
    }

    fi.close();
    allCost(dishesArr, fo);
    fo.close();


 return 0; }
