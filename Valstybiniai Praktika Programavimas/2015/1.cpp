#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    //pagalbine funkcija perkelianti duomenis i kita masyva
    void copyValues(int* to, int* from, int s){
        for(int i = 0; i < s; i++)
            to[i] = from[i];
    }

    //skaiciavimas kiek kas suvalgo
    void countEaten(int* arr){
        int temp[10]; copyValues(temp, arr, 10);

        for(int i = 0; i < 10; i++)
            for(int j = 1; j <= 10 - temp[i]; j++)
                arr[i + j]++;
    }

 int main(){

    //pagr kintamieji
     int arr[20];
     ifstream fi("U1.txt");
     ofstream fo("U1rez.txt");

     //nuskaitom faila pradini
     for(int i = 0; i < 20; i++){
        if(i < 10) fi >> arr[i];
        else arr[i] = 0;
     }

    // pertvarkau pagr masyva
     countEaten(arr);
     for(int i = 0; i < 20; i++)
        fo << arr[i] << " ";

    //uzbaigiu viska
    fi.close();
    fo.close();

 return 0; }
