#include <iostream>
#include <iomanip>
#include <fstream>
#include <string.h>

using namespace std;

    //globalus kintamieji
    string *names, *dnr;
    int *dnrDeltas;

    //iraso i faila atsakyma
    void outputFile(string s, int c, int index){
        ofstream fo("U2rez.txt");
        fo << s << endl;

        for(int i = 0; i < c; i++)
            if(i != index)
                fo << names[i] << " " << dnrDeltas[i] << endl;

        fo.close();
    }

    //funkcija nuskaitanti faila
    void readFile(int &scount, int &dnrs, int &m){
        ifstream fi("U2.txt");
        fi >> scount >> dnrs >> m;
        dnrDeltas = new int[scount - 1];
        names = new string[scount]; dnr = new string[scount];

        for(int i = 0; i < scount; i++)
            fi >> names[i] >> dnr[i];

        fi.close();
    }

    //apskaiciuoja koficienta
    int deltaDNR(string dnrA, string dnrB, int n){

        int result = 0;
        char A[1024], B[1024];
        strcpy(A, dnrA.c_str()); strcpy(B, dnrB.c_str());

        for(int i = 0; i < n; i++)
            if(A[i] == B[i]) result++;

        return result;
    }


    //sutvarko dnr masyvus
    void arrangeDNR(int n, int ignoreIndex){
        for(int i = 0; i < n; i++){
            if(i != ignoreIndex){
            for(int j = i + 1; j < n; j++){
                string tempS = names[i];
                int tempI = dnrDeltas[i];
                if(dnrDeltas[i] <= dnrDeltas[j]){
                    dnrDeltas[i] = dnrDeltas[j];
                    dnrDeltas[j] = tempI;
                    names[i] = names[j];
                    names[j] = tempS;
                }
              }
            }
        }
    }


 int main(){

    //lokalus kintamieji
    int sheepCount, dnrSize, mainSheepIndex;

    //nuskaito faila
    readFile(sheepCount, dnrSize, mainSheepIndex);

    //pagr avies vardas
    string mainName = names[mainSheepIndex - 1];

    //suskaiciuoja koficientus
    for(int i = 0; i < sheepCount; i++)
        if(i != mainSheepIndex - 1)
          dnrDeltas[i] = deltaDNR(dnr[mainSheepIndex - 1], dnr[i], dnrSize);

    //pertvarko
    arrangeDNR(sheepCount, mainSheepIndex - 1);

    //isvedimas i faila
    outputFile(mainName, sheepCount, mainSheepIndex - 1);

 return 0; }
