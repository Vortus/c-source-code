#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>

using namespace std;

    int numerics[12], uptime[12], sitTime[12];
    int startTeam[5];
    int n, teamc = 0;

    int findMax(int arr[], int l){ // teigiama ,neigiama didziausia
        int maxs = 0, maxI = 0;
        for(int i = 0; i < l; i++){
        if(maxs < abs(arr[i])){
                maxs = abs(arr[i]);
                maxI = i; }
        }
        return maxI;
    }

    void readFile(string s){ // nuskaito faila
        ifstream fi(s); fi >> n;
        for(int i = 0; i < n; i++){
            int j; fi >> j; numerics[i] = j;//krep. numr.
            int c; fi >> c;//laikai
            int a = 0, b = 0; //zaide, sedejo

            for(int d = 0; d < c; d++){
                int k; fi >> k; // laikas
                if(d == 0 && k > 0){//jei gali i komanda
                    startTeam[teamc] = j;
                    teamc++;
                }
                if(k > 0) a+= k;
                else b += k;
            }
            uptime[i] = a; sitTime[i] = b;
        }
        fi.close();
    }

    void returnValues(int a, int b){ // iraso duomenis i faila
        for(int i = 0; i < 5; i++){
            for(int j = i + 1; j < 5; j++){
                if(startTeam[i] > startTeam[j]){
                    int tmp = startTeam[i];
                    startTeam[i] = startTeam[j];
                    startTeam[j] = tmp;
                }
            }
        }
        ofstream fo("U1rez.txt");
        for(int i = 0; i < 5; i++)
            fo << startTeam[i] << " ";
        fo << endl << numerics[a] << " " << uptime[a] << endl <<
                    numerics[b] << " " << abs(sitTime[b]);
        fo.close();
    }

 int main(){

    readFile("U1.txt");
    int mostUp, mostSit; // daug zaides, sedejes indeksai
    mostUp = findMax(uptime, n);
    mostSit = findMax(sitTime, n);
    returnValues(mostUp, mostSit);

 return 0; }
