#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    struct God { // Dievo structas
        string name;
        int score;
    } gods[50];

    int n, k; // Dievu kiekis, kauliuku kiekis.

    void readFile(string s){ // Duomenu nuskaitymas
        ifstream fi(s); fi >> n >> k;
        for(int i = 0; i < n; i++){
            char name[10]; fi.ignore(); fi.get(name, 10);
            int score = 0;
            for(int j = 0; j < k; j++){
                int c; fi >> c;
                if(c % 2 == 0) score += c;
                else score -= c;
            }
            gods[i].name = name;
            gods[i].score = score;
        }
        fi.close();
    }

    int findBest(){// Surasti geriausia valdovo indeksa
        int index = 0, maxs = 0; // Geriausio valdovo indeksas ir tasku suma
        for(int i = 0; i < n; i++)
            if(maxs < gods[i].score ){ index = i; maxs = gods[i].score; }
        return index;
    }

 int main(){

    readFile("U2.txt");
    ofstream fo("U2rez.txt");
    int i = findBest();
    fo << gods[i].name << gods[i].score;
    fo.close();

 return 0; }
