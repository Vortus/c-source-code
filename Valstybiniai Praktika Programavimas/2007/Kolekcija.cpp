#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>

using namespace std;

    int jmax, lmax, lastl = 0;
    int jtoys[501], ltoys[501];
    int last[501];

    void readfile(){ // failo nuskaityams
        ifstream fi("U1duom.txt");
        fi >> lmax >> jmax;
        for(int i = 0; i < lmax; i++) fi >> ltoys[i];
        for(int i = 0; i < jmax; i++) fi >> jtoys[i];
        fi.close();
    }

    int toyCount(int arr[], int l, int toynr){ // iesko kiek kartu pasikartoja masyve
        int result = 0;
        for(int i = 0; i < l; i++) if(arr[i] == toynr) result++;
        return result;
    }

    void sorter(int arr[], int l){ // suriusoja
        for(int i = 0; i < l; i++)
            for(int j = i + 1; j < l; j++)
                if(arr[i] > arr[j]) swap(arr[i], arr[j]);
    }

 int main(){

    readfile();
    sorter(ltoys, lmax);
    sorter(jtoys, jmax);

    //////////Suranda linos keiciamu
    bool b = false;
    for(int i = 0; i < lmax; i++){ // linos
        if(toyCount(ltoys, lmax, ltoys[i]) > 1 // jei yra dayugiau nei vienas
           && toyCount(jtoys, jmax, ltoys[i]) == 0){ // jei neturi jrugis
            cout << ltoys[i] << " "; ltoys[i] = -1;
            b = true;
           }
    }
    if(!b) cout << 0; b = false;
    cout << endl;

    //////////Suranda jurgio keiciamu
    for(int i = 0; i < jmax; i++){//jurgio
        if(toyCount(jtoys, jmax, jtoys[i]) > 1 // jei yra dayugiau nei vienas
           && toyCount(ltoys, lmax, jtoys[i]) == 0){ // jei neturi lina
            cout << jtoys[i] << " "; jtoys[i] = -1;
            b = true;
           }
    }
    if(!b) cout << 0;

    //////////// sujungia i viena masyva
    for(int i = 0; i < lmax; i++){ // linos
        if(ltoys[i] != -1 && toyCount(last, lastl, ltoys[i]) == 0){
            last[lastl] = ltoys[i]; lastl++; // idedi i masyva
        }
    }

    for(int i = 0; i < jmax; i++){ // jurgio
        if(jtoys[i] != -1 && toyCount(last, lastl, jtoys[i]) == 0){
            last[lastl] = jtoys[i]; lastl++; // idedi i masyva
        }
    }
    /////////////////

    cout << endl;
    sorter(last, lastl);

    for(int i = 0; i < lastl; i++) //
        cout << last[i] << " ";

 return 0; }
