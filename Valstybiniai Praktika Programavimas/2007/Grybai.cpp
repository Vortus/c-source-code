#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    struct day{
        int day, b, r, l;
    } arr[100];

    int n;

    int maximum(int a, int b, int c){
       return a + b + c;
    }

    void sort(){
        for(int i = 0; i < n; i++){
            for(int j = i + 1; j < n; j++){
                if(arr[i].day > arr[j].day){
                    day tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }
            }
        }

    }

    void readFile(){
        ifstream fi("U1.txt");
        fi >> n;
        for(int i = 0; i < n; i++){
            int a, b, c, d; fi >> a >> b >> c >> d;
            arr[i].day = a; arr[i].b = b; arr[i].r = c;
            arr[i].l = d;
            if(maximum(b, c, d) == 0) arr[i].day = -1;
        }
        fi.close();
    }

    void coutArr(){
        for(int i = 0; i < n; i++)
            if(arr[i].day != -1) cout << arr[i].day << " " <<
            arr[i].b << " " << arr[i].r << " " << arr[i].l << endl;
    }

    void fuse(){

        for(int i = 0; i < n; i++){
            for(int j = i + 1; j < n; j++){
                if(arr[i].day == arr[j].day){
                    arr[j].day = -1;
                    arr[i].b += arr[j].b;
                    arr[i].l += arr[j].l;
                    arr[i].r += arr[j].r;
                }
            }
        }

    }

 int main(){

    readFile();
    sort();
    fuse();

    int maxs = 0, maxI = 0, minus = 0;
    for(int i = 0; i < n; i++){
        if(arr[i].day == -1) minus++;
        int j = maximum(arr[i].b, arr[i].l, arr[i].r);
        if(maxs < j && arr[i].day != -1){ maxs = j; maxI = i - minus; }
    }
    sort();
    coutArr();

    cout << maxI + 1 << " " << maxs;

 return 0; }
