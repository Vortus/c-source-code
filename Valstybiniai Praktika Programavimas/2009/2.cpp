#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

    struct Begikas{
        string name;
        int seconds = 0;
    } runners[1000];

    int crunner = 0;

    void sort(){
        crunner++;// buvo paskutinis indeksas reikia ilgio
        for(int i = 0; i < crunner; i++)
            for(int j = i + 1; j < crunner; j++)
            if(runners[i].seconds > runners[j].seconds){
                Begikas tmp = runners[i];
                runners[i] = runners[j];
                runners[j] = tmp;
            }
    }

    void sort(string sArr[], int iArr[], int l){
        for(int i = 0; i < l; i++){
            for(int j = i + 1; j < l; j++){
                if(iArr[i] > iArr[j]){
                    string stmp = sArr[i];
                    int itmp = iArr[i];
                    iArr[i] = iArr[j];
                    iArr[j] = itmp;
                    sArr[i] = sArr[j];
                    sArr[j] = stmp;
                }
            }
        }
    }

    //parenka galimus begikus is grupes ir sudeda i galimu begiku masyva
    void chooseRunners(string names[], int times[], int l){
        sort(names, times, l);

        if(l > 2){
                runners[crunner].name = names[0];
                runners[crunner].seconds = times[0];
                crunner++;
                runners[crunner].name = names[1];
                runners[crunner].seconds = times[1];
                crunner++;
        }else{
                runners[crunner].name = names[0];
                runners[crunner].seconds = times[0];
                crunner++;
        }
    }

    void returnRunners(){
        ofstream fo("U2rez.txt");
        for(int i = 1; i < crunner; i++)
            fo << runners[i].name << runners[i].seconds / 60 << " " << runners[i].seconds % 60 << endl;
        fo.close();
    }

 int main(){

    ifstream fi("U2.txt");
    int n; fi >> n;
    for(int i = 0; i < n; i++){
        int j; fi >> j;
        int rArr[j]; string srArr[j];

        for(int d = 0; d < j; d++){
            int a, b; fi.ignore();
            char s[20]; fi.get(s, 20);
            fi >> a >> b;
            srArr[d] = s; rArr[d] = (a * 60) + b;
        }

        chooseRunners(srArr, rArr, j);
    }
    fi.close();

    sort(); // surusiuoja galimus begikus
    returnRunners();

 return 0; }
