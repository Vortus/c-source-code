#include <io.h>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

    struct Day{
        int day, m, v, e; // ryte vid vakare
    }arr[32];

    int days;

    void readFile(){
        ifstream fi("U1duom.txt");
        fi >> days;
        for(int i = 0; i < days; i++){
            int a, b, c, d; fi >> a >> b >> c >> d;
            arr[i].day = a;
            arr[i].m = b;
            arr[i].v = c;
            arr[i].e = d;
        }
        fi.close();
    }

    int bestTemp(){
        int max = arr[0].m;
        for(int i = 0; i < days; i++){
            if(arr[i].m > max) max = arr[i].m;
            if(arr[i].v > max) max = arr[i].v;
            if(arr[i].e > max) max = arr[i].e;
        }
        return max;
    }

    void bestToFile(){
        ofstream fo("U1rez.txt");
        int max = bestTemp();
        for(int i = 0; i < days; i++){
            if(arr[i].m == max || arr[i].v == max || arr[i].e == max) fo << arr[i].day << " ";
        }
        fo << endl;
        fo.close();
    }

    void notLessToFile(){
        int c = 0;
        ofstream fo; fo.open("U1rez.txt", ios::app);
        for(int i = 0; i < days; i++)
            if(arr[i].m >= 20 && arr[i].v >= 20 && arr[i].e >= 20){ c++; fo << arr[i].day << " "; }
        if (c == 0) fo << "0";
        fo.close();
    }

 int main(){

    readFile();
    bestToFile();
    notLessToFile();

 return 0; }
